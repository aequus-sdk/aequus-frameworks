//
//  AequusAdmanagerInterstitial.swift
//  AequusAdmanagerAdapter
//
//  Created by Aleksandr Poliakov on 20.01.2022.
//

import Foundation
import AequusCore
import GoogleMobileAds

enum AequusAdmobError: Error {
	case adWasNotReady
}

final class AequusAdmanagerInterstitial: NSObject, Interstitial, StatusCheck {
	
	weak var delegate: InterstitialDelegate?
	
	var isReady: Bool {
		return interstitial != nil
	}
	
	var sdkVersion: String {
		GADMobileAds.sharedInstance().sdkVersion
	}
	
	private var interstitial: GAMInterstitialAd?
	private let unitId: String
	
	init(unitId: String, delegate: InterstitialDelegate?) {
		self.delegate = delegate
		self.unitId = unitId
		
		super.init()
	}
	
	func load() {
        let request = GAMRequest()
        
        GAMInterstitialAd.load(withAdManagerAdUnitID:self.unitId,
                               request: request,
                               completionHandler: { [weak self] ad, error in
            guard let `self` = self else { return }
            if let error = error {
                self.delegate?.didFailToLoad(interstitial: self, error: error)
            } else {
                self.interstitial = ad
                self.interstitial?.fullScreenContentDelegate = self
                self.delegate?.didLoad(interstitial: self)
            }
        }
        )
    }
	
	func show(from viewController: UIViewController) {
		if interstitial != nil {
			interstitial?.present(fromRootViewController: viewController)
		} else {
			delegate?.didFailToShow(interstitial: self, error: AequusAdmobError.adWasNotReady)
		}
	}
	
	func destroy() {
		self.interstitial?.fullScreenContentDelegate = nil
	}
	
}

extension AequusAdmanagerInterstitial: GADFullScreenContentDelegate {
	func adDidRecordImpression(_ ad: GADFullScreenPresentingAd) {
		delegate?.impression(interstitial: self)
	}
	
	func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
		delegate?.didShow(interstitial: self)
	}
	
	func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
		delegate?.didFailToShow(interstitial: self, error: error)
	}
	
	func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
		delegate?.didClose(interstitial: self)
	}
	
	func adDidRecordClick(_ ad: GADFullScreenPresentingAd) {
		delegate?.click(interstitial: self)
	}
}
