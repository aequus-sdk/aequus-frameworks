//
//  AequusAdmanagerInterstitialFactory.swift
//  AequusAdmanagerAdapter
//
//  Created by Aleksandr Poliakov on 20.01.2022.
//

import Foundation
import AequusCore

public final class AequusAdmanagerInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusAdmanagerInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusAdmanagerInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusAdmanagerInterstitialFactory {
        return AequusAdmanagerInterstitialFactory()
    }
}
