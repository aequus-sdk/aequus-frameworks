//
//  AequusAdmanagerInitializer.swift
//  AequusAdmanagerAdapter
//
//  Created by Aleksandr Poliakov on 20.01.2022.
//

import Foundation
import AequusCore
import Combine
import GoogleMobileAds

public final class AequusAdmanagerInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        completion(true, nil)
    }
}

extension AequusAdmanagerInitializer: Initializable {
    public static func createInstance() -> AequusAdmanagerInitializer {
        return AequusAdmanagerInitializer()
    }
}
