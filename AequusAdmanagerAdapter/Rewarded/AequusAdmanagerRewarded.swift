//
//  AequusAdmanagerRewarded.swift
//  AequusAdmanagerAdapter
//
//  Created by Aleksandr Poliakov on 20.01.2022.
//
import Foundation
import AequusCore
import GoogleMobileAds

final class AequusAdmanagerRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool {
        return rewarded != nil
    }
    
    var sdkVersion: String {
		GADMobileAds.sharedInstance().sdkVersion
    }
    
	private var rewarded: GADRewardedAd?
    private let unitId: String
    
    init(unitId: String, delegate: RewardedInterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
            
        super.init()
    }
    
    func load() {
		let request = GAMRequest()
		
		GADRewardedAd.load(withAdUnitID: self.unitId, request: request) { [weak self] ad, error in
			guard let `self` = self else { return }
			if let error = error {
				self.delegate?.didFailToLoad(rewarded: self, error: error)
			} else {
				self.rewarded = ad
				self.rewarded?.fullScreenContentDelegate = self
				self.delegate?.didLoad(rewarded: self)
			}
		}
    }
    
    func show(from viewController: UIViewController) {
		if rewarded != nil {
			rewarded?.present(fromRootViewController: viewController) { [weak self] in
				guard let `self` = self else { return }
				self.delegate?.eligibleForReward(rewarded: self)
			}
		} else {
			delegate?.didFailToShow(rewarded: self, error: AequusAdmobError.adWasNotReady)
		}
    }
    
    
    func destroy() {
		self.rewarded?.fullScreenContentDelegate = nil
    }
}

extension AequusAdmanagerRewarded: GADFullScreenContentDelegate {
	func adDidRecordImpression(_ ad: GADFullScreenPresentingAd) {
		delegate?.impression(rewarded: self)
	}
	
	func adWillPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
		delegate?.didShow(rewarded: self)
	}
	
	func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
		delegate?.didFailToShow(rewarded: self, error: error)
	}
	
	func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
		delegate?.didClose(rewarded: self)
	}
	
	func adDidRecordClick(_ ad: GADFullScreenPresentingAd) {
		delegate?.click(rewarded: self)
	}
}
