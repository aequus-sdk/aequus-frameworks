//
//  AequusAdmanagerRewardedFactory.swift
//  AequusAdmanagerAdapter
//
//  Created by Aleksandr Poliakov on 20.01.2022.
//

import Foundation
import AequusCore

public final class AequusAdmanagerRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusAdmanagerRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusAdmanagerRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusAdmanagerRewardedFactory {
        return AequusAdmanagerRewardedFactory()
    }
}
