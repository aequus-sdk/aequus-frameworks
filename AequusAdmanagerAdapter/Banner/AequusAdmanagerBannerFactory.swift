//
//  AequusAdmanagerBannerFactory.swift
//  AequusAdmanagerAdapter
//
//  Created by Aleksandr Poliakov on 20.01.2022.
//

import Foundation
import AequusCore

public final class AequusAdmanagerBannerFactory: BannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        AequusAdmanagerBanner(unitId: id, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusAdmanagerBannerFactory {
        return AequusAdmanagerBannerFactory()
    }
}
