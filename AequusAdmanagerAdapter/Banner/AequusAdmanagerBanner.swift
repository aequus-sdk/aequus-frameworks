//
//  AequusAdmanagerBanner.swift
//  AequusAdmanagerAdapter
//
//  Created by Aleksandr Poliakov on 20.01.2022.
//

import AequusCore
import GoogleMobileAds

final class AequusAdmanagerBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
		GADMobileAds.sharedInstance().sdkVersion
    }
    var isReady: Bool = false
	
    var bannerView: UIView? {
        return banner
    }
	
	var timeout: Bool = false
	
    private let viewController: UIViewController
    private var banner: GAMBannerView? = nil
    private let unitId: String
    private let type: AequusBannerType
    
	init(unitId: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
        self.type = type
		
        super.init()
        
        banner = GAMBannerView(adSize: configureSize(type: type))
		banner?.isAutoloadEnabled = false
    }
    
    func load() {
		banner?.adUnitID = self.unitId
		banner?.rootViewController = viewController
		banner?.delegate = self
		banner?.load(GAMRequest())
    }
    
    func destroy() {
        self.banner?.removeFromSuperview()
        self.banner = nil
    }
    
    private func configureSize(type: AequusBannerType) -> GADAdSize {
        var size: GADAdSize = GADAdSize.init()
        
        switch type {
        case .w320h50:
            size = type.size.width == 320 ? GADAdSizeBanner : GADAdSizeLeaderboard
        case .mrec:
            size = GADAdSizeMediumRectangle
        }
        
        return size
    }
}

extension AequusAdmanagerBanner: GADBannerViewDelegate {
    func bannerViewDidRecordClick(_ bannerView: GADBannerView) {
        delegate?.click(banner: self)
    }
    
	func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
		delegate?.didLoad(banner: self)
		delegate?.didShow(banner: self)
		isReady = true
	}

	func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
		delegate?.failToLoad(banner: self, error: error)
	}

	func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
		delegate?.impression(banner: self)
	}

	func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {}

	func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {}

	func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {}
    
}
