//
//  InterstitialManager.swift
//  AequusUnityAds
//
//  Created by Bohdan Korda on 03.12.2021.
//

import Foundation
import AequusCore
import UnityAds
import Combine

class EventManager: AdapterEventManager, UnityAdsLoadDelegate, UnityAdsShowDelegate {
	
	static var shared: EventManager = EventManager()
	
	private override init() {
		super.init()
	}
	
	func unityAdsAdLoaded(_ placementId: String) {
		adDidLoad(placementId)
	}
	
	func unityAdsAdFailed(toLoad placementId: String, withError error: UnityAdsLoadError, withMessage message: String) {
		UnitySDKProxy.shared.placementFinishShowing(placementID: placementId)
		adDidFailToLoadWithError(UnityAdsLoadErrorBridge(with: error), id: placementId)
	}
	
	func unityAdsShowComplete(_ placementId: String, withFinish state: UnityAdsShowCompletionState) {
		if state == .showCompletionStateCompleted {
			adDidReward(placementId)
		}
		UnitySDKProxy.shared.placementFinishShowing(placementID: placementId)
		adDidClose(placementId)
	}
	
	func unityAdsShowStart(_ placementId: String) {
		adDidShow(placementId)
		adImpression(placementId)
	}
	
	func unityAdsShowClick(_ placementId: String) {
		adDidClick(placementId)
	}
	
	func unityAdsShowFailed(_ placementId: String, withError error: UnityAdsShowError, withMessage message: String) {
		adDidFailToShowWithError(UnityAdsShowErrorBridge(with: error), id: placementId)
	}
	
}
