//
//  UnitySDKProxy.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 15.12.2021.
//

import Foundation
import UnityAds

enum UnitySDKProxyError: Error {
	case alreadyInProgress
}

class UnitySDKProxy {
	
	static let shared = UnitySDKProxy()
	
	private var placementsInLoading: [String] = []
	private init(){}
	
	func loadPlacement(with adUnitId: String) throws {
		guard !placementsInLoading.contains(adUnitId) else {
			throw UnitySDKProxyError.alreadyInProgress
		}
		
		self.placementsInLoading.append(adUnitId)
		UnityAds.load(adUnitId, loadDelegate: EventManager.shared)
	}
	
	func placementFinishShowing(placementID: String) {
		if let index = placementsInLoading.firstIndex(of: placementID) {
			placementsInLoading.remove(at: index)
		}
	}
	
}
