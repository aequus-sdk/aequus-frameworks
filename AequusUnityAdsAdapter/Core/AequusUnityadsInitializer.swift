//
//  AequusUnityadsInitializer.swift
//  AequusUnityadsAdapter
//
//  Created by Bohdan Korda on 13.05.2021.
//

import Foundation
import AequusCore
import UnityAds
import Combine

public enum UnityAdsInitializationErrorBridge: Error {
   /**
    *  Error related to environment or internal services.
    */
    case internalError
   
   /**
    * Error related to invalid arguments
    */
    case invalidArgument
   
   /**
    * Error related to url being blocked
    */
    case adBlockerDetected
    
    case unknown
    
    init(with error: UnityAdsInitializationError) {
        switch error {
        case .initializationErrorAdBlockerDetected:
            self = .adBlockerDetected
        case .initializationErrorInternalError:
            self = .internalError
        case .initializationErrorInvalidArgument:
            self = .invalidArgument
        @unknown default:
            self = .unknown
        }
    }
 
}


public final class AequusUnityadsInitializer: NSObject, AdNetworkInitializer {
    
    private var initCompletion: ((Bool, Error?) -> Void)?
    
    public static var isInitialized: Bool {
        UnityAds.isInitialized()
    }
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
		
        if !AequusUnityadsInitializer.isInitialized {
            self.initCompletion = completion
            
            if let gameId = config?.gameID {
                UnityAds.initialize(gameId,
                                    testMode: false,
                                    initializationDelegate: self)
            } else {
                completion(false, nil)
            }
        }
    }
}

extension AequusUnityadsInitializer: UnityAdsInitializationDelegate {
    public func initializationComplete() {
        initCompletion?(true, nil)
		initCompletion = nil
    }
    
    public func initializationFailed(_ error: UnityAdsInitializationError, withMessage message: String) {
        initCompletion?(false, UnityAdsInitializationErrorBridge(with: error))
		initCompletion = nil
    }
}

extension AequusUnityadsInitializer: Initializable {
    public static func createInstance() -> AequusUnityadsInitializer {
        return AequusUnityadsInitializer()
    }
}
