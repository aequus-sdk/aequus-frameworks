//
//  AequusUnityadsInterstitial.swift
//  AequusUnityadsAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import UnityAds
import Combine

/**
 *  An enumeration for the error category of load errors
 */
public enum UnityAdsLoadErrorBridge: Error {
    /**
     * Error related to SDK not initialized
     */
    case initializeFailed
    
    /**
     * Error related to environment or internal services
     */
    case `internal`
    
    /**
     * Error related to invalid arguments
     */
    case invalidArgument
    
    /**
     * Error related to there being no ads available
     */
    case noFill
    
    /**
     * Error related to there being no ads available
     */
    case timeout
    case unknown
    
    init(with error: UnityAdsLoadError) {
        switch error {
        case .initializeFailed:
            self = .initializeFailed
        case .internal:
            self = .internal
        case .invalidArgument:
            self = .invalidArgument
        case .noFill:
            self = .noFill
        case .timeout:
            self = .timeout
        @unknown default:
            self = .unknown
        }
    }
}

/**
 *  An enumeration for the error category of show errors
 */
public enum UnityAdsShowErrorBridge: Error {
    /**
     * Error related to SDK not initialized
     */
    case notInitialized
    
    /**
     * Error related to placement not being ready
     */
    case notReady
    
    /**
     * Error related to video player
     */
    case videoPlayerError
    
    /**
     * Error related to invalid arguments
     */
    case invalidArgument
     
    /**
     * Error related to internet connection
     */
    case noConnection
       
    /**
     * Error related to ad is already being shown
     */
    case alreadyShowing
    
    /**
     * Error related to environment or internal services
     */
    case internalError
    case unknown
    
    init(with error: UnityAdsShowError) {
        switch error {
        case .showErrorNotInitialized:
            self = .notInitialized
        case .showErrorAlreadyShowing:
            self = .alreadyShowing
        case .showErrorInternalError:
            self = .internalError
        case .showErrorInvalidArgument:
            self = .invalidArgument
        case .showErrorNoConnection:
            self = .noConnection
        case .showErrorNotReady:
            self = .notReady
        case .showErrorVideoPlayerError:
            self = .videoPlayerError
        @unknown default:
            self = .unknown
        }
    }
}

final class AequusUnityadsInterstitial: NSObject, Interstitial {
    
    weak var delegate: InterstitialDelegate?
    
    var isReady: Bool = false
    
    var sdkVersion: String {
        UnityAds.getVersion()
    }
    
    private var adUnitId: String
    private var adm: String?
	private var cancellable: AnyCancellable?
	private var onScreen: Bool = false
	
	var multiInstanceSupport: Bool {
		false
	}
    
    init(adUnitId: String, delegate: InterstitialDelegate) {
        self.adUnitId = adUnitId
        self.delegate = delegate
		
		super.init()
		
		cancellable = EventManager.shared.adEventPublisher.sink { [weak self] event in
			guard let self = self, event.adUnitID == self.adUnitId else { return }
			switch event.type {
			case .didLoad:
				self.isReady = true
				self.delegate?.didLoad(interstitial: self)
			case .failToLoad(let error):
				self.delegate?.didFailToLoad(interstitial: self, error: error)
			case .didShow:
				self.onScreen = true
				self.delegate?.didShow(interstitial: self)
			case .hide:
				self.onScreen = false
				self.isReady = false
				self.delegate?.didClose(interstitial: self)
			case .failToShow(let error):
				self.onScreen = false
				self.delegate?.didFailToShow(interstitial: self, error: error)
			case .click:
				self.delegate?.click(interstitial: self)
			case .expire:
				break
			case .impression:
				self.delegate?.impression(interstitial: self)
			case .reward, .videoStart, .videoFinished:
				break
			}
		}
    }
    
    func load() {
		do {
			try UnitySDKProxy.shared.loadPlacement(with: adUnitId)
		} catch {
			self.delegate?.didFailToLoad(interstitial: self, error: error)
		}
    }
    
    func show(from viewController: UIViewController) {
        UnityAds.show(viewController, placementId: adUnitId, showDelegate: EventManager.shared)
    }
    
    func destroy() {
		if onScreen {
			return
		}
        cancellable = nil
    }
}
