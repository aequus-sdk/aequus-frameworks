//
//  AequusUnityadsInterstitialFactory.swift
//  AequusUnityAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import UnityAds

public final class AequusUnityadsInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusUnityadsInterstitial(adUnitId: id, delegate: delegate)
    }
    
    public static func createInstance() -> AequusUnityadsInterstitialFactory {
        return AequusUnityadsInterstitialFactory()
    }
    
    
}
