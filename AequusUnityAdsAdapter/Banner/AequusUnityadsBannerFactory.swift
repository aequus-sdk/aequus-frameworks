//
//  AequusUnityadsBannerFactory.swift
//  AequusUnityadsAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import UnityAds

public final class AequusUnityadsBannerFactory: BannerFactory {
	public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
		return AequusUnityadsBanner(placementId: id, size: type.size, delegate: delegate)
	}

    public static func createInstance() -> AequusUnityadsBannerFactory {
        return AequusUnityadsBannerFactory()
    }
}

