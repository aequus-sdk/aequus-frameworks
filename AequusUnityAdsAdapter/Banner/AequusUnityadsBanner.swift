//
//  AequusUnityadsBanner.swift
//  AequusUnityadsAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import UnityAds

final class AequusUnityadsBanner: NSObject, Banner, StatusCheck {
    
    weak var delegate: BannerDelegate?
	var timeout: Bool = false
    
    var bannerView: UIView? {
        banner
    }
    
    var sdkVersion: String {
        UnityAds.getVersion()
    }
    
    var isReady: Bool = false
    private let placementId: String
    private let banner: UADSBannerView
    
    init(placementId: String, size: CGSize, delegate: BannerDelegate) {
        self.delegate = delegate
        self.placementId = placementId
        self.banner = UADSBannerView(placementId: placementId, size: size)
        
        super.init()
        self.banner.delegate = self
    }
    
    func load() {
        self.banner.load()
    }
    
    func destroy() {
        self.banner.removeFromSuperview()
    }
    
}

extension AequusUnityadsBanner: UADSBannerViewDelegate {
    func bannerViewDidLoad(_ bannerView: UADSBannerView!) {
		delegate?.didLoad(banner: self)
		delegate?.didShow(banner: self)
		delegate?.impression(banner: self)
    }
    
    func bannerViewDidError(_ bannerView: UADSBannerView!, error: UADSBannerError!) {
        delegate?.failToLoad(banner: self, error: error)
    }
    
    func bannerViewDidClick(_ bannerView: UADSBannerView!) {
        delegate?.click(banner: self)
    }

}
