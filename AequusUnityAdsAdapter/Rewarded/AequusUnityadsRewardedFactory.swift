//
//  AequusUnityadsRewardedFactory.swift
//  AequusUnityadsAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import UnityAds

public final class AequusUnityadsRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusUnityadsRewarded(adUnitId: id, delegate: delegate)
    }
    
    public static func createInstance() -> AequusUnityadsRewardedFactory {
        return AequusUnityadsRewardedFactory()
    }
    
    
}
