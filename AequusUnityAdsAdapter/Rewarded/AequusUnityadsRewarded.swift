//
//  AequusUnityadsRewarded.swift
//  AequusUnityadsAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import UnityAds
import Combine

final class AequusUnityadsRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool = false
    
    var sdkVersion: String {
        UnityAds.getVersion()
    }
	
	var multiInstanceSupport: Bool {
		false
	}
    
    private var adUnitId: String
    private var adm: String?
	private var cancellable: AnyCancellable?
	private var onScreen: Bool = false
    
    init(adUnitId: String, delegate: RewardedInterstitialDelegate) {
        self.adUnitId = adUnitId
        self.delegate = delegate
		
		super.init()
		
		cancellable = EventManager.shared.adEventPublisher.sink { [weak self] event in
			guard let self = self, event.adUnitID == self.adUnitId else { return }
			switch event.type {
			case .didLoad:
				self.isReady = true
				self.delegate?.didLoad(rewarded: self)
			case .failToLoad(let error):
				self.delegate?.didFailToLoad(rewarded: self, error: error)
			case .didShow:
				self.onScreen = true
				self.delegate?.didShow(rewarded: self)
			case .hide:
				self.onScreen = false
				self.isReady = false
				self.delegate?.didClose(rewarded: self)
			case .failToShow(let error):
				self.onScreen = false
				self.delegate?.didFailToShow(rewarded: self, error: error)
			case .reward:
				self.delegate?.eligibleForReward(rewarded: self)
			case .click:
				self.delegate?.click(rewarded: self)
			case .expire:
				break
			case .impression:
				self.delegate?.impression(rewarded: self)
			case .videoStart:
				break
			case .videoFinished:
				break
			}
		}
    }
    
    func load() {
		do {
			try UnitySDKProxy.shared.loadPlacement(with: adUnitId)
		} catch {
			self.delegate?.didFailToLoad(rewarded: self, error: error)
		}
    }
    
    func show(from viewController: UIViewController) {
        UnityAds.show(viewController, placementId: adUnitId, showDelegate: EventManager.shared)
    }
    
    func destroy() {
		if onScreen {
			return
		}
		cancellable = nil
    }
}
