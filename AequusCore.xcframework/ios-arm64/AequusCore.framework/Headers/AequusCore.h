//
//  AequusCore.h
//  AequusCore
//
//  Created by Bohdan Korda on 06.08.2021.
//

#import <Foundation/Foundation.h>

//! Project version number for AequusCore.
FOUNDATION_EXPORT double AequusCoreVersionNumber;

//! Project version string for AequusCore.
FOUNDATION_EXPORT const unsigned char AequusCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AequusCore/PublicHeader.h>


