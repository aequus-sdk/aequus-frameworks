// Generated by Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
#ifndef AEQUUSCORE_SWIFT_H
#define AEQUUSCORE_SWIFT_H
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgcc-compat"

#if !defined(__has_include)
# define __has_include(x) 0
#endif
#if !defined(__has_attribute)
# define __has_attribute(x) 0
#endif
#if !defined(__has_feature)
# define __has_feature(x) 0
#endif
#if !defined(__has_warning)
# define __has_warning(x) 0
#endif

#if __has_include(<swift/objc-prologue.h>)
# include <swift/objc-prologue.h>
#endif

#pragma clang diagnostic ignored "-Wauto-import"
#include <Foundation/Foundation.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#if !defined(SWIFT_TYPEDEFS)
# define SWIFT_TYPEDEFS 1
# if __has_include(<uchar.h>)
#  include <uchar.h>
# elif !defined(__cplusplus)
typedef uint_least16_t char16_t;
typedef uint_least32_t char32_t;
# endif
typedef float swift_float2  __attribute__((__ext_vector_type__(2)));
typedef float swift_float3  __attribute__((__ext_vector_type__(3)));
typedef float swift_float4  __attribute__((__ext_vector_type__(4)));
typedef double swift_double2  __attribute__((__ext_vector_type__(2)));
typedef double swift_double3  __attribute__((__ext_vector_type__(3)));
typedef double swift_double4  __attribute__((__ext_vector_type__(4)));
typedef int swift_int2  __attribute__((__ext_vector_type__(2)));
typedef int swift_int3  __attribute__((__ext_vector_type__(3)));
typedef int swift_int4  __attribute__((__ext_vector_type__(4)));
typedef unsigned int swift_uint2  __attribute__((__ext_vector_type__(2)));
typedef unsigned int swift_uint3  __attribute__((__ext_vector_type__(3)));
typedef unsigned int swift_uint4  __attribute__((__ext_vector_type__(4)));
#endif

#if !defined(SWIFT_PASTE)
# define SWIFT_PASTE_HELPER(x, y) x##y
# define SWIFT_PASTE(x, y) SWIFT_PASTE_HELPER(x, y)
#endif
#if !defined(SWIFT_METATYPE)
# define SWIFT_METATYPE(X) Class
#endif
#if !defined(SWIFT_CLASS_PROPERTY)
# if __has_feature(objc_class_property)
#  define SWIFT_CLASS_PROPERTY(...) __VA_ARGS__
# else
#  define SWIFT_CLASS_PROPERTY(...)
# endif
#endif

#if __has_attribute(objc_runtime_name)
# define SWIFT_RUNTIME_NAME(X) __attribute__((objc_runtime_name(X)))
#else
# define SWIFT_RUNTIME_NAME(X)
#endif
#if __has_attribute(swift_name)
# define SWIFT_COMPILE_NAME(X) __attribute__((swift_name(X)))
#else
# define SWIFT_COMPILE_NAME(X)
#endif
#if __has_attribute(objc_method_family)
# define SWIFT_METHOD_FAMILY(X) __attribute__((objc_method_family(X)))
#else
# define SWIFT_METHOD_FAMILY(X)
#endif
#if __has_attribute(noescape)
# define SWIFT_NOESCAPE __attribute__((noescape))
#else
# define SWIFT_NOESCAPE
#endif
#if __has_attribute(ns_consumed)
# define SWIFT_RELEASES_ARGUMENT __attribute__((ns_consumed))
#else
# define SWIFT_RELEASES_ARGUMENT
#endif
#if __has_attribute(warn_unused_result)
# define SWIFT_WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#else
# define SWIFT_WARN_UNUSED_RESULT
#endif
#if __has_attribute(noreturn)
# define SWIFT_NORETURN __attribute__((noreturn))
#else
# define SWIFT_NORETURN
#endif
#if !defined(SWIFT_CLASS_EXTRA)
# define SWIFT_CLASS_EXTRA
#endif
#if !defined(SWIFT_PROTOCOL_EXTRA)
# define SWIFT_PROTOCOL_EXTRA
#endif
#if !defined(SWIFT_ENUM_EXTRA)
# define SWIFT_ENUM_EXTRA
#endif
#if !defined(SWIFT_CLASS)
# if __has_attribute(objc_subclassing_restricted)
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# else
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# endif
#endif
#if !defined(SWIFT_RESILIENT_CLASS)
# if __has_attribute(objc_class_stub)
#  define SWIFT_RESILIENT_CLASS(SWIFT_NAME) SWIFT_CLASS(SWIFT_NAME) __attribute__((objc_class_stub))
#  define SWIFT_RESILIENT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_class_stub)) SWIFT_CLASS_NAMED(SWIFT_NAME)
# else
#  define SWIFT_RESILIENT_CLASS(SWIFT_NAME) SWIFT_CLASS(SWIFT_NAME)
#  define SWIFT_RESILIENT_CLASS_NAMED(SWIFT_NAME) SWIFT_CLASS_NAMED(SWIFT_NAME)
# endif
#endif

#if !defined(SWIFT_PROTOCOL)
# define SWIFT_PROTOCOL(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
# define SWIFT_PROTOCOL_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
#endif

#if !defined(SWIFT_EXTENSION)
# define SWIFT_EXTENSION(M) SWIFT_PASTE(M##_Swift_, __LINE__)
#endif

#if !defined(OBJC_DESIGNATED_INITIALIZER)
# if __has_attribute(objc_designated_initializer)
#  define OBJC_DESIGNATED_INITIALIZER __attribute__((objc_designated_initializer))
# else
#  define OBJC_DESIGNATED_INITIALIZER
# endif
#endif
#if !defined(SWIFT_ENUM_ATTR)
# if defined(__has_attribute) && __has_attribute(enum_extensibility)
#  define SWIFT_ENUM_ATTR(_extensibility) __attribute__((enum_extensibility(_extensibility)))
# else
#  define SWIFT_ENUM_ATTR(_extensibility)
# endif
#endif
#if !defined(SWIFT_ENUM)
# define SWIFT_ENUM(_type, _name, _extensibility) enum _name : _type _name; enum SWIFT_ENUM_ATTR(_extensibility) SWIFT_ENUM_EXTRA _name : _type
# if __has_feature(generalized_swift_name)
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME, _extensibility) enum _name : _type _name SWIFT_COMPILE_NAME(SWIFT_NAME); enum SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_ENUM_ATTR(_extensibility) SWIFT_ENUM_EXTRA _name : _type
# else
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME, _extensibility) SWIFT_ENUM(_type, _name, _extensibility)
# endif
#endif
#if !defined(SWIFT_UNAVAILABLE)
# define SWIFT_UNAVAILABLE __attribute__((unavailable))
#endif
#if !defined(SWIFT_UNAVAILABLE_MSG)
# define SWIFT_UNAVAILABLE_MSG(msg) __attribute__((unavailable(msg)))
#endif
#if !defined(SWIFT_AVAILABILITY)
# define SWIFT_AVAILABILITY(plat, ...) __attribute__((availability(plat, __VA_ARGS__)))
#endif
#if !defined(SWIFT_WEAK_IMPORT)
# define SWIFT_WEAK_IMPORT __attribute__((weak_import))
#endif
#if !defined(SWIFT_DEPRECATED)
# define SWIFT_DEPRECATED __attribute__((deprecated))
#endif
#if !defined(SWIFT_DEPRECATED_MSG)
# define SWIFT_DEPRECATED_MSG(...) __attribute__((deprecated(__VA_ARGS__)))
#endif
#if __has_feature(attribute_diagnose_if_objc)
# define SWIFT_DEPRECATED_OBJC(Msg) __attribute__((diagnose_if(1, Msg, "warning")))
#else
# define SWIFT_DEPRECATED_OBJC(Msg) SWIFT_DEPRECATED_MSG(Msg)
#endif
#if !defined(IBSegueAction)
# define IBSegueAction
#endif
#if __has_feature(modules)
#if __has_warning("-Watimport-in-framework-header")
#pragma clang diagnostic ignored "-Watimport-in-framework-header"
#endif
@import CoreData;
@import CoreGraphics;
@import ObjectiveC;
@import UIKit;
#endif

#pragma clang diagnostic ignored "-Wproperty-attribute-mismatch"
#pragma clang diagnostic ignored "-Wduplicate-method-arg"
#if __has_warning("-Wpragma-clang-attribute")
# pragma clang diagnostic ignored "-Wpragma-clang-attribute"
#endif
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wnullability"

#if __has_attribute(external_source_symbol)
# pragma push_macro("any")
# undef any
# pragma clang attribute push(__attribute__((external_source_symbol(language="Swift", defined_in="AequusCore",generated_declaration))), apply_to=any(function,enum,objc_interface,objc_category,objc_protocol))
# pragma pop_macro("any")
#endif



@class NSEntityDescription;
@class NSManagedObjectContext;

SWIFT_CLASS_NAMED("AdCapping")
@interface AdCapping : NSManagedObject
- (nonnull instancetype)initWithEntity:(NSEntityDescription * _Nonnull)entity insertIntoManagedObjectContext:(NSManagedObjectContext * _Nullable)context OBJC_DESIGNATED_INITIALIZER;
@end


@class NSNumber;
@class NSDate;

@interface AdCapping (SWIFT_EXTENSION(AequusCore))
@property (nonatomic) int64_t dayAdsShown;
@property (nonatomic, copy) NSDate * _Nullable dayStartUTC;
@property (nonatomic) int64_t hourAdsShown;
@property (nonatomic, copy) NSDate * _Nullable hourStartUTC;
@property (nonatomic) int64_t placementID;
@end

@class NSString;

SWIFT_PROTOCOL("_TtP10AequusCore22AdReadyDelegateBridged_")
@protocol AdReadyDelegateBridged
- (void)adReady:(BOOL)ready impressionDataString:(NSString * _Nullable)impressionDataString;
@end

typedef SWIFT_ENUM_NAMED(NSInteger, AequusAdType, "AdType", open) {
  AequusAdTypeBanner = 0,
  AequusAdTypeInterstitial = 1,
  AequusAdTypeRewarded = 2,
};


SWIFT_CLASS("_TtC10AequusCore19AdapterEventManager")
@interface AdapterEventManager : NSObject
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end

@protocol ILRDDelegate;
@class AequusPrivacy;

SWIFT_CLASS("_TtC10AequusCore6Aequus")
@interface Aequus : NSObject
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, strong) Aequus * _Nonnull shared;)
+ (Aequus * _Nonnull)shared SWIFT_WARN_UNUSED_RESULT;
@property (nonatomic, copy) NSString * _Nullable userID;
@property (nonatomic, readonly) BOOL isInitialised;
@property (nonatomic, weak) id <ILRDDelegate> _Nullable ilrdDelegate;
@property (nonatomic, strong) AequusPrivacy * _Nonnull privacy;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end


@interface Aequus (SWIFT_EXTENSION(AequusCore))
- (void)initialiseWithApiKey:(NSString * _Nonnull)apiKey acsKey:(NSString * _Nonnull)acsKey completion:(void (^ _Nullable)(BOOL, NSError * _Nullable))completion;
@end

@class UIViewController;
@protocol AequusBannerDelegate;
@class AequusBannerAdView;
@protocol AequusInterstitialDelegate;
@protocol AequusInterstitial;
@protocol AequusRewardedDelegate;
@protocol AequusRewardedInterstitial;

@interface Aequus (SWIFT_EXTENSION(AequusCore))
- (AequusBannerAdView * _Nonnull)createMRECFor:(NSString * _Nonnull)placementName viewController:(UIViewController * _Nonnull)viewController delegate:(id <AequusBannerDelegate> _Nullable)delegate SWIFT_WARN_UNUSED_RESULT;
- (id <AequusInterstitial> _Nonnull)createInterstitialFor:(NSString * _Nonnull)placementName viewController:(UIViewController * _Nonnull)viewController delegate:(id <AequusInterstitialDelegate> _Nullable)delegate SWIFT_WARN_UNUSED_RESULT;
- (id <AequusRewardedInterstitial> _Nonnull)createRewardedFor:(NSString * _Nonnull)placementName viewController:(UIViewController * _Nonnull)viewController delegate:(id <AequusRewardedDelegate> _Nullable)delegate SWIFT_WARN_UNUSED_RESULT;
- (AequusBannerAdView * _Nonnull)createBannerFor:(NSString * _Nonnull)placementName viewController:(UIViewController * _Nonnull)viewController delegate:(id <AequusBannerDelegate> _Nullable)delegate SWIFT_WARN_UNUSED_RESULT;
@end


SWIFT_PROTOCOL("_TtP10AequusCore11StatusCheck_")
@protocol StatusCheck
@property (nonatomic, readonly) BOOL isReady;
@end


SWIFT_PROTOCOL("_TtP10AequusCore11Destroyable_")
@protocol Destroyable
- (void)destroy;
@end


SWIFT_PROTOCOL("_TtP10AequusCore8AequusAd_")
@protocol AequusAd <Destroyable, StatusCheck>
@property (nonatomic, readonly) BOOL isAdLoaded;
@end

typedef SWIFT_ENUM(NSInteger, AequusAdErrorBridged, open) {
  AequusAdErrorBridgedGeneral = 0,
  AequusAdErrorBridgedTimeout = 1,
};


SWIFT_PROTOCOL("_TtP10AequusCore20AequusExtraParameter_")
@protocol AequusExtraParameter
- (void)setExtraParameterWithKey:(NSString * _Nonnull)key extraParam:(id _Nullable)extraParam;
@end

@class NSCoder;

SWIFT_CLASS("_TtC10AequusCore18AequusBannerAdView")
@interface AequusBannerAdView : UIView <AequusAd, AequusExtraParameter>
@property (nonatomic, weak) id <AequusBannerDelegate> _Nullable delegate;
@property (nonatomic, readonly) BOOL isAdLoaded;
@property (nonatomic) BOOL isReady;
@property (nonatomic) BOOL suspendPreloadWhenInvisible;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)coder SWIFT_UNAVAILABLE;
- (void)didMoveToSuperview;
- (void)load;
- (void)destroy;
- (void)show;
- (void)hide;
- (void)setExtraParameterWithKey:(NSString * _Nonnull)key extraParam:(id _Nullable)extraParam;
- (nonnull instancetype)initWithFrame:(CGRect)frame SWIFT_UNAVAILABLE;
@end




SWIFT_PROTOCOL("_TtP10AequusCore14BaseAdDelegate_")
@protocol BaseAdDelegate
- (void)didLoadWithAd:(id <AequusAd> _Nonnull)ad;
- (void)failToLoadWithAd:(id <AequusAd> _Nonnull)ad with:(NSError * _Nonnull)error;
- (void)didShowWithAd:(id <AequusAd> _Nonnull)ad;
- (void)failToShowWithAd:(id <AequusAd> _Nonnull)ad with:(NSError * _Nonnull)error;
- (void)didHideWithAd:(id <AequusAd> _Nonnull)ad;
- (void)didClickOn:(id <AequusAd> _Nonnull)ad;
- (void)impressionOn:(id <AequusAd> _Nonnull)ad;
@end


SWIFT_PROTOCOL("_TtP10AequusCore20AequusBannerDelegate_")
@protocol AequusBannerDelegate <BaseAdDelegate>
@end

typedef SWIFT_ENUM(NSInteger, AequusBannerType, open) {
  AequusBannerTypeW320h50 = 0,
  AequusBannerTypeMrec = 1,
};


@protocol AQImpressionDataProtocol;

SWIFT_PROTOCOL("_TtP10AequusCore30AequusFirstAdToDisplayDelegate_")
@protocol AequusFirstAdToDisplayDelegate
- (void)firstAdToDisplayChangedWithImpressionData:(id <AQImpressionDataProtocol> _Nonnull)impressionData adType:(enum AequusAdType)adType;
@end


SWIFT_PROTOCOL("_TtP10AequusCore37AequusFirstAdToDisplayDelegateBridged_")
@protocol AequusFirstAdToDisplayDelegateBridged
- (void)firstAdToDisplayChangedWithImpressionDataString:(NSString * _Nonnull)impressionDataString;
@end


SWIFT_PROTOCOL("_TtP10AequusCore18AequusInterstitial_")
@protocol AequusInterstitial <AequusAd, AequusExtraParameter>
@property (nonatomic, strong) id <AequusInterstitialDelegate> _Nullable interstitialDelegate;
@property (nonatomic, strong) id <AequusFirstAdToDisplayDelegate> _Nullable firstAdToDisplayDelegate;
@property (nonatomic, readonly, strong) id <AQImpressionDataProtocol> _Nullable firstAdToDisplay;
- (void)load;
- (void)showFrom:(UIViewController * _Nonnull)viewController;
@end


SWIFT_PROTOCOL("_TtP10AequusCore26AequusInterstitialDelegate_")
@protocol AequusInterstitialDelegate <BaseAdDelegate>
@end


SWIFT_CLASS("_TtC10AequusCore13AequusPrivacy")
@interface AequusPrivacy : NSObject
@property (nonatomic, readonly) BOOL GDPRConsent;
@property (nonatomic, readonly) BOOL CCPADoNotSell;
@property (nonatomic, readonly) BOOL underAgeOfConsent;
- (nonnull instancetype)init SWIFT_UNAVAILABLE;
+ (nonnull instancetype)new SWIFT_UNAVAILABLE_MSG("-init is unavailable");
@end


SWIFT_CLASS_NAMED("Builder")
@interface AequusPrivacyBuilder : NSObject
- (AequusPrivacyBuilder * _Nonnull)GDPRConsent:(BOOL)consent SWIFT_WARN_UNUSED_RESULT;
- (AequusPrivacyBuilder * _Nonnull)CCPADoNotSell:(BOOL)doNotSell SWIFT_WARN_UNUSED_RESULT;
- (AequusPrivacyBuilder * _Nonnull)underAgeOfConsent:(BOOL)underAgeOfConsent SWIFT_WARN_UNUSED_RESULT;
- (AequusPrivacy * _Nonnull)build SWIFT_WARN_UNUSED_RESULT;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end


SWIFT_PROTOCOL("_TtP10AequusCore22AequusRewardedDelegate_")
@protocol AequusRewardedDelegate <BaseAdDelegate>
- (void)userRewardedWithAd:(id <AequusAd> _Nonnull)ad;
- (void)rewardedVideoStartedWithAd:(id <AequusAd> _Nonnull)ad;
- (void)rewardedVideoCompletedWithAd:(id <AequusAd> _Nonnull)ad;
@end


SWIFT_PROTOCOL("_TtP10AequusCore26AequusRewardedInterstitial_")
@protocol AequusRewardedInterstitial <AequusAd, AequusExtraParameter>
@property (nonatomic, strong) id <AequusRewardedDelegate> _Nullable rewardedDelegate;
@property (nonatomic, strong) id <AequusFirstAdToDisplayDelegate> _Nullable firstAdToDisplayDelegate;
@property (nonatomic, readonly, strong) id <AQImpressionDataProtocol> _Nullable firstAdToDisplay;
- (void)load;
- (void)showFrom:(UIViewController * _Nonnull)viewController;
@end


SWIFT_CLASS("_TtC10AequusCore18AequusUnityBridged")
@interface AequusUnityBridged : NSObject
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, strong) AequusUnityBridged * _Nonnull shared;)
+ (AequusUnityBridged * _Nonnull)shared SWIFT_WARN_UNUSED_RESULT;
@property (nonatomic, copy) NSString * _Nullable userID;
@property (nonatomic, strong) AequusPrivacy * _Nonnull privacy;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end


@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore)) <AequusBannerDelegate>
@end


@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore))
- (void)setStringSegment:(NSString * _Nonnull)json;
@end


@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore)) <AequusFirstAdToDisplayDelegate>
- (void)firstAdToDisplayChangedWithImpressionData:(id <AQImpressionDataProtocol> _Nonnull)impressionData adType:(enum AequusAdType)adType;
@end


@protocol RewardedAdDelegateBridged;

@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore))
- (void)requestRewardedWith:(NSString * _Nonnull)placementName viewController:(UIViewController * _Nonnull)viewController delegate:(id <RewardedAdDelegateBridged> _Nullable)delegate firstAdToDisplayDelegate:(id <AequusFirstAdToDisplayDelegateBridged> _Nullable)firstAdToDisplayDelegate adReadyDelegate:(id <AdReadyDelegateBridged> _Nullable)adReadyDelegate;
- (void)loadRewarded;
- (void)showRewardedWithViewController:(UIViewController * _Nonnull)viewController;
@end

@protocol InterstitialAdDelegateBridged;

@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore))
- (void)requestInterstitialWith:(NSString * _Nonnull)placementName viewController:(UIViewController * _Nonnull)viewController delegate:(id <InterstitialAdDelegateBridged> _Nullable)delegate firstAdToDisplayDelegate:(id <AequusFirstAdToDisplayDelegateBridged> _Nullable)firstAdToDisplayDelegate adReadyDelegate:(id <AdReadyDelegateBridged> _Nullable)adReadyDelegate;
- (void)loadInterstitial;
- (void)showInterstitialWithViewController:(UIViewController * _Nonnull)viewController;
@end


SWIFT_PROTOCOL("_TtP10AequusCore12ILRDDelegate_")
@protocol ILRDDelegate
- (void)adDidLoadWithImpressionData:(id <AQImpressionDataProtocol> _Nonnull)impressionData;
- (void)adDidShowWithImpressionData:(id <AQImpressionDataProtocol> _Nonnull)impressionData;
@end

@protocol ILRDDelegateBridged;

@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore)) <ILRDDelegate>
- (void)setILRDDelegate:(id <ILRDDelegateBridged> _Nullable)delegate;
- (void)adDidLoadWithImpressionData:(id <AQImpressionDataProtocol> _Nonnull)impressionData;
- (void)adDidShowWithImpressionData:(id <AQImpressionDataProtocol> _Nonnull)impressionData;
@end


@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore)) <AequusRewardedDelegate>
- (void)userRewardedWithAd:(id <AequusAd> _Nonnull)ad;
- (void)rewardedVideoStartedWithAd:(id <AequusAd> _Nonnull)ad;
- (void)rewardedVideoCompletedWithAd:(id <AequusAd> _Nonnull)ad;
@end

@protocol BannerAdDelegateBridged;

@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore))
- (void)requestBannerWith:(NSString * _Nonnull)placementName suspendPreloadWhenInvisible:(BOOL)suspendPreloadWhenInvisible viewController:(UIViewController * _Nonnull)viewController delegate:(id <BannerAdDelegateBridged> _Nullable)delegate;
- (void)bannerShow;
- (void)bannerHide;
@end


@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore))
- (void)requestMRECBannerWith:(NSString * _Nonnull)placementName suspendPreloadWhenInvisible:(BOOL)suspendPreloadWhenInvisible viewController:(UIViewController * _Nonnull)viewController delegate:(id <BannerAdDelegateBridged> _Nullable)delegate;
- (void)bannerMrecShow;
- (void)bannerMrecHide;
@end


@interface AequusUnityBridged (SWIFT_EXTENSION(AequusCore)) <AequusInterstitialDelegate>
- (void)failToLoadWithAd:(id <AequusAd> _Nonnull)ad with:(NSError * _Nonnull)error;
- (void)didShowWithAd:(id <AequusAd> _Nonnull)ad;
- (void)failToShowWithAd:(id <AequusAd> _Nonnull)ad with:(NSError * _Nonnull)error;
- (void)didHideWithAd:(id <AequusAd> _Nonnull)ad;
- (void)didClickOn:(id <AequusAd> _Nonnull)ad;
- (void)didLoadWithAd:(id <AequusAd> _Nonnull)ad;
- (void)impressionOn:(id <AequusAd> _Nonnull)ad;
@end


SWIFT_PROTOCOL("_TtP10AequusCore13BaseAdBridged_")
@protocol BaseAdBridged
- (void)didLoad;
- (void)failToLoadWith:(NSError * _Nonnull)error;
- (void)didShow;
- (void)failToShow;
- (void)didHide;
- (void)didClick;
- (void)impression;
@end


SWIFT_PROTOCOL("_TtP10AequusCore23BannerAdDelegateBridged_")
@protocol BannerAdDelegateBridged <BaseAdBridged>
@end






SWIFT_CLASS_NAMED("DCREvent")
@interface DCREvent : NSManagedObject
- (nonnull instancetype)initWithEntity:(NSEntityDescription * _Nonnull)entity insertIntoManagedObjectContext:(NSManagedObjectContext * _Nullable)context OBJC_DESIGNATED_INITIALIZER;
@end


@class NSUUID;

@interface DCREvent (SWIFT_EXTENSION(AequusCore))
@property (nonatomic, copy) NSString * _Nullable abTestGroup;
@property (nonatomic) int32_t abTestID;
@property (nonatomic, copy) NSString * _Nullable adapter;
@property (nonatomic, copy) NSString * _Nullable adapterVersion;
@property (nonatomic, copy) NSString * _Nullable adUnitID;
@property (nonatomic, copy) NSString * _Nullable appVersion;
@property (nonatomic, copy) NSString * _Nullable event;
@property (nonatomic, copy) NSString * _Nullable format;
@property (nonatomic) BOOL isBid;
@property (nonatomic) int64_t latency;
@property (nonatomic) int64_t placementID;
@property (nonatomic, copy) NSString * _Nullable publisherID;
@property (nonatomic, copy) NSUUID * _Nullable sessionID;
@property (nonatomic, copy) NSDate * _Nullable sessionStartDate;
@property (nonatomic) int64_t value;
@end




SWIFT_PROTOCOL("_TtP10AequusCore19ILRDDelegateBridged_")
@protocol ILRDDelegateBridged
- (void)adDidLoadWithImpressionDataString:(NSString * _Nonnull)impressionDataString;
- (void)adDidShowWithImpressionDataString:(NSString * _Nonnull)impressionDataString;
@end

enum ILRDNetworkType : NSInteger;

SWIFT_CLASS("_TtC10AequusCore15ILRDNetworkInfo")
@interface ILRDNetworkInfo : NSObject
@property (nonatomic, readonly) enum ILRDNetworkType type;
@property (nonatomic, readonly, copy) NSString * _Nonnull network;
@property (nonatomic, readonly, copy) NSString * _Nonnull version;
- (nonnull instancetype)init SWIFT_UNAVAILABLE;
+ (nonnull instancetype)new SWIFT_UNAVAILABLE_MSG("-init is unavailable");
@end

typedef SWIFT_ENUM(NSInteger, ILRDNetworkType, open) {
  ILRDNetworkTypeBid = 0,
  ILRDNetworkTypeWaterfall = 1,
};


SWIFT_PROTOCOL_NAMED("ImpressionData")
@protocol AQImpressionDataProtocol
@property (nonatomic, readonly, copy) NSString * _Nonnull impressionId;
@property (nonatomic, readonly) NSInteger placementId;
@property (nonatomic, readonly, copy) NSString * _Nonnull placementName;
@property (nonatomic, readonly, copy) NSString * _Nonnull adFormatId;
@property (nonatomic, readonly, copy) NSString * _Nonnull adFormatName;
@property (nonatomic, readonly, copy) NSString * _Nonnull currency;
@property (nonatomic, readonly) double publisherRevenue;
@property (nonatomic, readonly, copy) NSString * _Nonnull networkName;
@property (nonatomic, readonly, copy) NSString * _Nonnull networkAdUnitId;
@property (nonatomic, readonly, copy) NSString * _Nonnull networkAdUnitName;
@property (nonatomic, readonly, copy) NSString * _Nonnull appVersion;
@property (nonatomic, readonly) NSInteger adUnitPriority;
@property (nonatomic, readonly, copy) NSString * _Nonnull country;
@property (nonatomic, readonly, copy) NSString * _Nonnull networkType;
@property (nonatomic, readonly, copy) NSString * _Nonnull networkSdkVersion;
@property (nonatomic, readonly, copy) NSString * _Nonnull aequusSdkVersion;
@property (nonatomic, readonly, copy) NSString * _Nonnull timestamp;
@property (nonatomic, readonly, copy) NSString * _Nonnull precision;
@property (nonatomic, readonly) BOOL lat;
@property (nonatomic, readonly, copy) NSString * _Nullable abTestId;
@property (nonatomic, readonly, copy) NSString * _Nullable abTestGroup;
@property (nonatomic, readonly, copy) NSString * _Nullable deviceType;
@property (nonatomic, readonly, copy) NSString * _Nullable jsonRepresentation;
@end


SWIFT_PROTOCOL("_TtP10AequusCore14Impressionable_")
@protocol Impressionable
@end


SWIFT_PROTOCOL("_TtP10AequusCore29InterstitialAdDelegateBridged_")
@protocol InterstitialAdDelegateBridged <BaseAdBridged>
@end


SWIFT_PROTOCOL("_TtP10AequusCore25RewardedAdDelegateBridged_")
@protocol RewardedAdDelegateBridged <BaseAdBridged>
- (void)reward;
@end







#if __has_attribute(external_source_symbol)
# pragma clang attribute pop
#endif
#pragma clang diagnostic pop
#endif
