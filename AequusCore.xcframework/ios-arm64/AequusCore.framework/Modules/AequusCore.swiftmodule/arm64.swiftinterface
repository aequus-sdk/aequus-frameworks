// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name AequusCore
import AVFoundation
import AVKit
import AdSupport
@_exported import AequusCore
import AppTrackingTransparency
import Combine
import CoreData
import CoreGraphics
import CoreLocation
import CoreMedia
import CoreTelephony
import CryptoKit
import Foundation
import SafariServices
import StoreKit
import Swift
import SystemConfiguration
import UIKit
import WebKit
import _Concurrency
@_hasMissingDesignatedInitializers @objc public class AequusPrivacy : ObjectiveC.NSObject {
  @objc final public let GDPRConsent: Swift.Bool
  @objc final public let CCPADoNotSell: Swift.Bool
  @objc final public let underAgeOfConsent: Swift.Bool
  @_inheritsConvenienceInitializers @objc(AequusPrivacyBuilder) public class Builder : ObjectiveC.NSObject {
    @objc public func GDPRConsent(_ consent: Swift.Bool) -> AequusCore.AequusPrivacy.Builder
    @objc public func CCPADoNotSell(_ doNotSell: Swift.Bool) -> AequusCore.AequusPrivacy.Builder
    @objc public func underAgeOfConsent(_ underAgeOfConsent: Swift.Bool) -> AequusCore.AequusPrivacy.Builder
    @objc public func build() -> AequusCore.AequusPrivacy
    @objc override dynamic public init()
    @objc deinit
  }
  @objc deinit
}
@objc public enum AequusAdErrorBridged : Swift.Int {
  case general
  case timeout
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc public protocol BaseAdBridged {
  @objc func didLoad()
  @objc func failToLoad(with error: Swift.Error)
  @objc func didShow()
  @objc func failToShow()
  @objc func didHide()
  @objc func didClick()
  @objc func impression()
}
@objc public protocol InterstitialAdDelegateBridged : AequusCore.BaseAdBridged {
}
@objc public protocol BannerAdDelegateBridged : AequusCore.BaseAdBridged {
}
@objc public protocol RewardedAdDelegateBridged : AequusCore.BaseAdBridged {
  @objc func reward()
}
@objc public protocol AdReadyDelegateBridged {
  @objc func adReady(_ ready: Swift.Bool, impressionDataString: Swift.String?)
}
@objc public protocol ILRDDelegateBridged {
  @objc func adDidLoad(impressionDataString: Swift.String)
  @objc func adDidShow(impressionDataString: Swift.String)
}
@objc public protocol AequusFirstAdToDisplayDelegateBridged {
  @objc func firstAdToDisplayChanged(impressionDataString: Swift.String)
}
public struct Impression : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol AdNetworkInitializer : AnyObject, AequusCore.Initializable {
  static var isInitialized: Swift.Bool { get }
  func initialize(config: AequusCore.SDKConfig.AdapterNetworkConfig?, privacyPublisher: Combine.AnyPublisher<AequusCore.AequusPrivacy, Swift.Never>, completion: @escaping (Swift.Bool, Swift.Error?) -> Swift.Void)
}
public protocol Initializable {
  static func createInstance() -> Self
}
extension Swift.Array {
  public func associateBy<Key>(_ selectKey: (Element) -> Key) -> [Key : Element] where Key : Swift.Hashable
}
public protocol BidTokenSource : AequusCore.Initializable {
  func invoke(completion: @escaping (Swift.String?) -> Swift.Void)
}
public protocol ExternalBidAdUnit : AequusCore.Destroyable {
  var price: Swift.Int64 { get }
  var adUnit: Swift.String { get }
  func requestBid(completion: @escaping (Swift.Bool) -> ())
}
public enum TrackingError : Swift.Error {
  case unableToUpdateProgressTrackingComplete
  case unableToUpdateProgress(msg: Swift.String)
  case unableToProvideCreativeClickThroughUrls
  case unableToProvideCreativeImpressions
  case unableToProvideCreativeView
  case internalError(msg: Swift.String)
  case unableToSkipAdAtThisTime
  case noLinearAdFound
}
extension Swift.Bool {
  public var numericStringValue: Swift.String {
    get
  }
  public var intValue: Swift.Int {
    get
  }
}
public typealias HTTPParameters = [Swift.String : Any]
public typealias HTTPHeaders = [Swift.String : Swift.String]
public struct VastParserOptions {
  public let wrapperLimit: Swift.Int
  public let singleWrapperTimeLimit: Foundation.TimeInterval
  public let timeLimit: Foundation.TimeInterval
  public init(wrapperLimit: Swift.Int = 5, singleWrapperTimeLimit: Foundation.TimeInterval = 5, timeLimit: Foundation.TimeInterval = 10)
  public static let `default`: AequusCore.VastParserOptions
}
public protocol MultiInstanciable {
  var multiInstanceSupport: Swift.Bool { get }
}
extension AequusCore.MultiInstanciable {
  public var multiInstanceSupport: Swift.Bool {
    get
  }
}
public protocol Banner : AequusCore.Destroyable, AequusCore.StatusCheck {
  var delegate: AequusCore.BannerDelegate? { get set }
  var timeout: Swift.Bool { get set }
  var bannerView: UIKit.UIView? { get }
  var sdkVersion: Swift.String { get }
  func load()
}
public protocol BannerDelegate : AnyObject {
  func didLoad(banner: AequusCore.Banner)
  func failToLoad(banner: AequusCore.Banner?, error: Swift.Error?)
  func didShow(banner: AequusCore.Banner)
  func impression(banner: AequusCore.Banner)
  func click(banner: AequusCore.Banner)
}
public protocol BannerFactory : AequusCore.AdFactory, AequusCore.Initializable {
  func create(viewController: UIKit.UIViewController, type: AequusCore.AequusBannerType, id: Swift.String, adUnitConfig: AequusCore.SDKConfig.AdUnitConfig, delegate: AequusCore.BannerDelegate) -> AequusCore.Banner?
}
public protocol BidBannerFactory : AequusCore.Initializable {
  func create(viewController: UIKit.UIViewController, type: AequusCore.AequusBannerType, adId: Swift.String, bidId: Swift.String, adm: Swift.String, params: [Swift.String : Swift.String]?, hashableParams: [Swift.AnyHashable : Any]?, delegate: AequusCore.BannerDelegate) -> AequusCore.Banner?
}
public protocol ExternalBidBannerFactory : AequusCore.Initializable {
  func create(viewController: UIKit.UIViewController, type: AequusCore.AequusBannerType, delegate: AequusCore.BannerDelegate?) -> AequusCore.Banner?
}
@_hasMissingDesignatedInitializers final public class DCREventsStorage {
  @objc deinit
}
@objc public protocol ILRDDelegate {
  @objc func adDidLoad(impressionData: AequusCore.ImpressionData)
  @objc func adDidShow(impressionData: AequusCore.ImpressionData)
}
@objc(AQImpressionDataProtocol) public protocol ImpressionData {
  @objc var impressionId: Swift.String { get }
  @objc var placementId: Swift.Int { get }
  @objc var placementName: Swift.String { get }
  @objc var adFormatId: Swift.String { get }
  @objc var adFormatName: Swift.String { get }
  @objc var currency: Swift.String { get }
  @objc var publisherRevenue: Swift.Double { get }
  @objc var networkName: Swift.String { get }
  @objc var networkAdUnitId: Swift.String { get }
  @objc var networkAdUnitName: Swift.String { get }
  @objc var appVersion: Swift.String { get }
  @objc var adUnitPriority: Swift.Int { get }
  @objc var country: Swift.String { get }
  @objc var networkType: Swift.String { get }
  @objc var networkSdkVersion: Swift.String { get }
  @objc var aequusSdkVersion: Swift.String { get }
  @objc var timestamp: Swift.String { get }
  @objc var precision: Swift.String { get }
  @objc var lat: Swift.Bool { get }
  @objc var abTestId: Swift.String? { get }
  @objc var abTestGroup: Swift.String? { get }
  @objc var deviceType: Swift.String? { get }
  @objc var jsonRepresentation: Swift.String? { get }
}
@_hasMissingDesignatedInitializers public class AequusImpressionData : AequusCore.ImpressionData, Swift.Encodable {
  @objc public var impressionId: Swift.String
  @objc public var placementId: Swift.Int
  @objc public var placementName: Swift.String
  @objc public var adFormatId: Swift.String
  @objc public var adFormatName: Swift.String
  @objc public var currency: Swift.String
  @objc public var publisherRevenue: Swift.Double
  @objc public var networkName: Swift.String
  @objc public var networkAdUnitId: Swift.String
  @objc public var networkAdUnitName: Swift.String
  @objc public var appVersion: Swift.String
  @objc public var adUnitPriority: Swift.Int
  @objc public var country: Swift.String
  @objc public var networkType: Swift.String
  @objc public var networkSdkVersion: Swift.String
  @objc public var aequusSdkVersion: Swift.String
  @objc public var timestamp: Swift.String
  @objc public var precision: Swift.String
  @objc public var lat: Swift.Bool
  @objc public var abTestId: Swift.String?
  @objc public var abTestGroup: Swift.String?
  @objc public var deviceType: Swift.String?
  @objc public var jsonRepresentation: Swift.String? {
    @objc get
  }
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
}
@objc public class ILRDNetworkInfo : ObjectiveC.NSObject, Swift.Encodable {
  @objc public enum ILRDNetworkType : Swift.Int, Swift.Encodable {
    case bid
    case waterfall
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  @objc final public let type: AequusCore.ILRDNetworkInfo.ILRDNetworkType
  @objc final public let network: Swift.String
  @objc final public let version: Swift.String
  public init(type: AequusCore.ILRDNetworkInfo.ILRDNetworkType, network: Swift.String, version: Swift.String)
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
}
extension Swift.Dictionary {
  public func mapKeys<T>(_ transform: (Key) throws -> T) rethrows -> [T : Value] where T : Swift.Hashable
  public func compactMapKeys<Transformed>(_ transform: (Key) throws -> Transformed?) rethrows -> [Transformed : Value] where Transformed : Swift.Hashable
}
public enum VastPlayerOptionKey : Swift.String {
  case closeButtonDelay
  case closeButtonSize
  case ctaButtonSize
  case ctaTitle
  case buttonTintColor
  case progressTintColor
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum VastButtonSize {
  case regular
  case big
  public static func == (a: AequusCore.VastButtonSize, b: AequusCore.VastButtonSize) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum VastPlayerAdType {
  case banner
  case fullscreen
  public static func == (a: AequusCore.VastPlayerAdType, b: AequusCore.VastPlayerAdType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum PlayerUIViewEvents : Swift.Equatable {
  case close
  case skip
  case videoClick(url: Foundation.URL?)
  case companionClick(url: Foundation.URL?)
  case iconClick(url: Foundation.URL?)
  case mute
  case unmute
  case replay
  case unknown
  case companionWasShown
  public static func == (a: AequusCore.PlayerUIViewEvents, b: AequusCore.PlayerUIViewEvents) -> Swift.Bool
}
public class VastPlayer {
  public var readyToPlayPublisher: Combine.AnyPublisher<Swift.Bool, Swift.Never>? {
    get
  }
  public var videDidReachEndPublisher: Combine.AnyPublisher<Swift.Bool, Swift.Never>? {
    get
  }
  public var playerUIEventPublisher: Combine.AnyPublisher<AequusCore.PlayerUIViewEvents, Swift.Never> {
    get
  }
  public var videoView: UIKit.UIView {
    get
  }
  public var disableSkip: Swift.Bool
  public var forceSkip: Swift.Bool
  convenience public init?(vastXMLString: Swift.String, adType: AequusCore.VastPlayerAdType = .fullscreen, options: [AequusCore.VastPlayerOptionKey : Any])
  public init?(vastXMLData: Foundation.Data, adType: AequusCore.VastPlayerAdType = .fullscreen, options: [AequusCore.VastPlayerOptionKey : Any])
  public func start()
  public func impression()
  public func pause()
  public func play()
  public func destroy()
  @objc deinit
}
public protocol Network {
  var adapterName: Swift.String { get }
  var adapterAppId: Swift.String? { get }
  var adapterNetworkConfig: AequusCore.SDKConfig.AdapterNetworkConfig? { get }
  var id: Swift.Int64 { get }
}
public struct SDKConfig {
  public struct CustomData : Swift.Encodable {
    public func encode(to encoder: Swift.Encoder) throws
  }
  public struct WaterfallNetwork : AequusCore.Network, Swift.Decodable {
    public let adapterName: Swift.String
    public let id: Swift.Int64
    public let adapterAppId: Swift.String?
    public let adapterNetworkConfig: AequusCore.SDKConfig.AdapterNetworkConfig?
    public init(from decoder: Swift.Decoder) throws
  }
  public struct AdapterNetworkConfig : Swift.Decodable {
    public let accessKey: Swift.String?, accessToken: Swift.String?, accountID: Swift.String?, assetKey: Swift.String?, adNetworkKey: Swift.String?, appSignature: Swift.String?, appKey: Swift.String?, appID: Swift.String?, apiKey: Swift.String?, apiSecret: Swift.String?, buisnessID: Swift.String?, clientID: Swift.String?, clientSecret: Swift.String?, gameID: Swift.String?, monetisationStatsAPIKey: Swift.String?, organizationCoreID: Swift.String?, refreshToken: Swift.String?, reportingApiID: Swift.String?, reportingKey: Swift.String?, userID: Swift.String?, username: Swift.String?, userSignature: Swift.String?, sdkKey: Swift.String?, sKey: Swift.String?, secretKey: Swift.String?, siteId: Swift.String?, secret: Swift.String?, zoneIds: Swift.String?, storeId: Swift.String?, storeUrl: Swift.String?, sellerId: Swift.String?, publisherID: Swift.String?, bannerUnitId: Swift.String?, interstitialUnitId: Swift.String?, rewardedUnitId: Swift.String?, mrecUnitId: Swift.String?
    public init(from decoder: Swift.Decoder) throws
  }
  public struct Bidder : AequusCore.Network, Swift.Decodable {
    public let adapterName: Swift.String
    public let id: Swift.Int64
    public let adapterAppId: Swift.String?
    public let adapterNetworkConfig: AequusCore.SDKConfig.AdapterNetworkConfig?
    public var isNonSdkDemandPartner: Swift.Bool
    public var isExternalBidder: Swift.Bool
    public init(from decoder: Swift.Decoder) throws
  }
  public struct AdUnit : Swift.Decodable {
    public let adUnitID: Swift.String
    public let adUnitName: Swift.String
    public let network: Swift.String
    public let priority: Swift.Int
    public let cpmFloor: Swift.Int64
    public let maxAdUnitResponseTimeOutMs: Swift.Int64
    public let adUnitConfig: AequusCore.SDKConfig.AdUnitConfig
    public let precision: Swift.String
    public init(from decoder: Swift.Decoder) throws
  }
  public struct AdUnitConfig : Swift.Decodable {
    public let slotId: Swift.String?, adNetworkKey: Swift.String?, zoneId: Swift.String?, unitId: Swift.String?, placementId: Swift.String?
    public let maxAdUnitLoadsBeforeNoAds: Swift.Int?
    public let loadDurationSeconds: Swift.Int?
    public let delayErrorAfterLoadSeconds: Swift.Int?
    public init(from decoder: Swift.Decoder) throws
  }
}
public enum CreativeType : Swift.CaseIterable {
  case vast
  case mraid
  case `static`
  case unknown
  public static func == (a: AequusCore.CreativeType, b: AequusCore.CreativeType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public typealias AllCases = [AequusCore.CreativeType]
  public static var allCases: [AequusCore.CreativeType] {
    get
  }
  public var hashValue: Swift.Int {
    get
  }
}
public protocol Interstitial : AequusCore.Destroyable, AequusCore.MultiInstanciable, AequusCore.StatusCheck {
  var delegate: AequusCore.InterstitialDelegate? { get set }
  var sdkVersion: Swift.String { get }
  func load()
  func show(from viewController: UIKit.UIViewController)
}
public protocol InterstitialDelegate : AnyObject {
  func didLoad(interstitial: AequusCore.Interstitial)
  func didShow(interstitial: AequusCore.Interstitial)
  func impression(interstitial: AequusCore.Interstitial)
  func didClose(interstitial: AequusCore.Interstitial)
  func didFailToLoad(interstitial: AequusCore.Interstitial, error: Swift.Error)
  func didFailToShow(interstitial: AequusCore.Interstitial, error: Swift.Error)
  func click(interstitial: AequusCore.Interstitial)
  func expired(interstitial: AequusCore.Interstitial)
}
public protocol InterstitialFactory : AequusCore.AdFactory, AequusCore.Initializable {
  func create(viewController: UIKit.UIViewController, id: Swift.String, adUnitConfig: AequusCore.SDKConfig.AdUnitConfig, delegate: AequusCore.InterstitialDelegate) -> AequusCore.Interstitial?
}
public protocol BidInterstitialFactory {
  func create(viewController: UIKit.UIViewController, adId: Swift.String, bidId: Swift.String, adm: Swift.String, params: [Swift.String : Swift.String]?, hashableParams: [Swift.AnyHashable : Any]?, delegate: AequusCore.InterstitialDelegate) -> AequusCore.Interstitial?
}
public protocol ExternalBidInterstitialFactory {
  func create(viewController: UIKit.UIViewController, delegate: AequusCore.InterstitialDelegate?) -> AequusCore.Interstitial?
}
public enum VastParserError : Swift.Error {
  case invalidXMLDocument
  case invalidVASTDocument
  case unableToParseDocument
  case unableToCreateXMLParser
  case wrapperLimitReached
  case singleRequestTimeLimitReached
  case wrapperTimeLimitReached
  case internalError
  public static func == (a: AequusCore.VastParserError, b: AequusCore.VastParserError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum VastViewableImpressionType : Swift.String {
  case viewable
  case notViewable
  case viewUndetermined
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
@objc public enum AequusBannerType : Swift.Int {
  case w320h50 = 0
  case mrec = 1
  public var size: CoreGraphics.CGSize {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) public class AequusBannerAdView : UIKit.UIView, AequusCore.AequusAd, AequusCore.AequusExtraParameter {
  @objc @_Concurrency.MainActor(unsafe) weak public var delegate: AequusCore.AequusBannerDelegate?
  @objc @_Concurrency.MainActor(unsafe) public var isAdLoaded: Swift.Bool {
    @objc get
  }
  @objc @_Concurrency.MainActor(unsafe) public var isReady: Swift.Bool
  @objc @_Concurrency.MainActor(unsafe) public var suspendPreloadWhenInvisible: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func didMoveToSuperview()
  @objc @_Concurrency.MainActor(unsafe) public func load()
  @objc @_Concurrency.MainActor(unsafe) public func destroy()
  @objc @_Concurrency.MainActor(unsafe) public func show()
  @objc @_Concurrency.MainActor(unsafe) public func hide()
  @objc @_Concurrency.MainActor(unsafe) public func setExtraParameter(key: Swift.String, extraParam: Any?)
  @objc deinit
}
extension AequusCore.AequusBannerAdView : AequusCore.BannerDelegate {
  @_Concurrency.MainActor(unsafe) public func didLoad(banner: AequusCore.Banner)
  @_Concurrency.MainActor(unsafe) public func failToLoad(banner: AequusCore.Banner?, error: Swift.Error?)
  @_Concurrency.MainActor(unsafe) public func didShow(banner: AequusCore.Banner)
  @_Concurrency.MainActor(unsafe) public func impression(banner: AequusCore.Banner)
  @_Concurrency.MainActor(unsafe) public func click(banner: AequusCore.Banner)
}
infix operator === : DefaultPrecedence
@_hasMissingDesignatedInitializers final public class AdCapStorage {
  @objc deinit
}
public enum AdEventType : Swift.Equatable {
  case didLoad
  case failToLoad(error: Swift.Error)
  case didShow
  case failToShow(error: Swift.Error)
  case reward
  case hide
  case click
  case expire
  case impression
  case videoStart
  case videoFinished
  public static func == (lhs: AequusCore.AdEventType, rhs: AequusCore.AdEventType) -> Swift.Bool
}
public struct AdEvent {
  public let adUnitID: Swift.String
  public let type: AequusCore.AdEventType
}
@objc @_inheritsConvenienceInitializers open class AdapterEventManager : ObjectiveC.NSObject {
  public var adEventPublisher: Combine.AnyPublisher<AequusCore.AdEvent, Swift.Never> {
    get
  }
  public func adDidLoad(_ id: Swift.String)
  public func adDidFailToLoadWithError(_ error: Swift.Error, id: Swift.String)
  public func adDidShow(_ id: Swift.String)
  public func adDidClose(_ id: Swift.String)
  public func adDidFailToShowWithError(_ error: Swift.Error, id: Swift.String)
  public func adDidClick(_ id: Swift.String)
  public func adDidExpire(_ id: Swift.String)
  public func adDidReward(_ id: Swift.String)
  public func adImpression(_ id: Swift.String)
  public func adVideoStart(_ id: Swift.String)
  public func adVideoFinished(_ id: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
public enum Gender : Swift.String, Swift.Encodable {
  case male
  case female
  case unknown
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct AequusSegment {
  public class Builder {
    public init()
    public func gender(_ gender: AequusCore.Gender?) -> AequusCore.AequusSegment.Builder
    public func age(_ age: Swift.Int?) -> AequusCore.AequusSegment.Builder
    public func gameLevel(_ gameLevel: Swift.Int?) -> AequusCore.AequusSegment.Builder
    public func totalInAppPurchaseValue(_ totalInAppPurchaseValue: Swift.Int?) -> AequusCore.AequusSegment.Builder
    public func isPayingUser(_ isPayingUser: Swift.Bool?) -> AequusCore.AequusSegment.Builder
    public func custom(key: Swift.String, value: Swift.String?) -> AequusCore.AequusSegment.Builder
    public func build() -> AequusCore.AequusSegment
    @objc deinit
  }
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol AdFactory {
}
@objc public protocol Destroyable {
  @objc func destroy()
}
@objc public protocol StatusCheck {
  @objc var isReady: Swift.Bool { get }
}
@objc public protocol Impressionable {
}
public protocol AdInfo {
  var adUnitID: Swift.String { get }
  var adUnitName: Swift.String { get }
  var networkName: Swift.String { get }
}
public enum AdUnitType {
  case waterfall
  case bidding
  public static func == (a: AequusCore.AdUnitType, b: AequusCore.AdUnitType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol BidExternalParameterHandler : AequusCore.Destroyable {
  func consume(externalParameter: Any?)
  func produce() -> AequusCore.BidExternalParameterProducedData?
}
public protocol BidExternalParameterHandlerFactory {
  func create() -> AequusCore.BidExternalParameterHandler?
}
public struct BidExternalParameterProducedData {
  public init(hashableParams: [Swift.AnyHashable : Any]?, bidParameters: [Swift.AnyHashable : Any]?)
}
@objc @_inheritsConvenienceInitializers public class Aequus : ObjectiveC.NSObject {
  @objc public static let shared: AequusCore.Aequus
  @objc public var userID: Swift.String?
  @objc public var isInitialised: Swift.Bool {
    @objc get
  }
  @objc weak public var ilrdDelegate: AequusCore.ILRDDelegate?
  @objc public var privacy: AequusCore.AequusPrivacy {
    @objc get
    @objc set
  }
  public var setSegment: AequusCore.AequusSegment {
    get
    set
  }
  public var initialisationPublisher: Combine.AnyPublisher<Swift.Bool, Swift.Never> {
    get
  }
  public var sdkVersion: Swift.String {
    get
  }
  public var publisherSegmentBuilder: AequusCore.AequusSegment?
  @objc override dynamic public init()
  @objc deinit
}
extension AequusCore.Aequus {
  @objc dynamic public func initialise(apiKey: Swift.String, acsKey: Swift.String, completion: ((Swift.Bool, Swift.Error?) -> ())?)
}
extension AequusCore.Aequus {
  @objc dynamic public func createMREC(for placementName: Swift.String, viewController: UIKit.UIViewController, delegate: AequusCore.AequusBannerDelegate? = nil) -> AequusCore.AequusBannerAdView
  @objc dynamic public func createInterstitial(for placementName: Swift.String, viewController: UIKit.UIViewController, delegate: AequusCore.AequusInterstitialDelegate? = nil) -> AequusCore.AequusInterstitial
  @objc dynamic public func createRewarded(for placementName: Swift.String, viewController: UIKit.UIViewController, delegate: AequusCore.AequusRewardedDelegate? = nil) -> AequusCore.AequusRewardedInterstitial
  @objc dynamic public func createBanner(for placementName: Swift.String, viewController: UIKit.UIViewController, delegate: AequusCore.AequusBannerDelegate? = nil) -> AequusCore.AequusBannerAdView
}
@_inheritsConvenienceInitializers @objc final public class AequusUnityBridged : ObjectiveC.NSObject {
  @objc public static let shared: AequusCore.AequusUnityBridged
  @objc final public var userID: Swift.String? {
    @objc get
    @objc set
  }
  @objc final public var privacy: AequusCore.AequusPrivacy {
    @objc get
    @objc set
  }
  @objc override dynamic public init()
  @objc deinit
}
extension AequusCore.AequusUnityBridged : AequusCore.ILRDDelegate {
  @objc final public func setILRDDelegate(_ delegate: AequusCore.ILRDDelegateBridged?)
  @objc final public func adDidLoad(impressionData: AequusCore.ImpressionData)
  @objc final public func adDidShow(impressionData: AequusCore.ImpressionData)
}
extension AequusCore.AequusUnityBridged {
  @objc final public func setStringSegment(_ json: Swift.String)
}
extension AequusCore.AequusUnityBridged {
  @objc final public func requestInterstitial(with placementName: Swift.String, viewController: UIKit.UIViewController, delegate: AequusCore.InterstitialAdDelegateBridged?, firstAdToDisplayDelegate: AequusCore.AequusFirstAdToDisplayDelegateBridged?, adReadyDelegate: AequusCore.AdReadyDelegateBridged?)
  @objc final public func loadInterstitial()
  @objc final public func showInterstitial(viewController: UIKit.UIViewController)
}
extension AequusCore.AequusUnityBridged {
  @objc final public func requestRewarded(with placementName: Swift.String, viewController: UIKit.UIViewController, delegate: AequusCore.RewardedAdDelegateBridged?, firstAdToDisplayDelegate: AequusCore.AequusFirstAdToDisplayDelegateBridged?, adReadyDelegate: AequusCore.AdReadyDelegateBridged?)
  @objc final public func loadRewarded()
  @objc final public func showRewarded(viewController: UIKit.UIViewController)
}
extension AequusCore.AequusUnityBridged {
  @objc final public func requestBanner(with placementName: Swift.String, suspendPreloadWhenInvisible: Swift.Bool, viewController: UIKit.UIViewController, delegate: AequusCore.BannerAdDelegateBridged?)
  @objc final public func bannerShow()
  @objc final public func bannerHide()
}
extension AequusCore.AequusUnityBridged {
  @objc final public func requestMRECBanner(with placementName: Swift.String, suspendPreloadWhenInvisible: Swift.Bool, viewController: UIKit.UIViewController, delegate: AequusCore.BannerAdDelegateBridged?)
  @objc final public func bannerMrecShow()
  @objc final public func bannerMrecHide()
}
extension AequusCore.AequusUnityBridged : AequusCore.AequusFirstAdToDisplayDelegate {
  @objc final public func firstAdToDisplayChanged(impressionData: AequusCore.ImpressionData, adType: AequusCore.AdType)
}
extension AequusCore.AequusUnityBridged : AequusCore.AequusInterstitialDelegate {
  @objc final public func failToLoad(ad: AequusCore.AequusAd, with error: Swift.Error)
  @objc final public func didShow(ad: AequusCore.AequusAd)
  @objc final public func failToShow(ad: AequusCore.AequusAd, with error: Swift.Error)
  @objc final public func didHide(ad: AequusCore.AequusAd)
  @objc final public func didClick(on ad: AequusCore.AequusAd)
  @objc final public func didLoad(ad: AequusCore.AequusAd)
  @objc final public func impression(on ad: AequusCore.AequusAd)
}
extension AequusCore.AequusUnityBridged : AequusCore.AequusRewardedDelegate {
  @objc final public func userRewarded(ad: AequusCore.AequusAd)
  @objc final public func rewardedVideoStarted(ad: AequusCore.AequusAd)
  @objc final public func rewardedVideoCompleted(ad: AequusCore.AequusAd)
}
extension AequusCore.AequusUnityBridged : AequusCore.AequusBannerDelegate {
}
@_hasMissingDesignatedInitializers final public class AequusBidBannerFactory : AequusCore.BidBannerFactory {
  final public func create(viewController: UIKit.UIViewController, type: AequusCore.AequusBannerType, adId: Swift.String, bidId: Swift.String, adm: Swift.String, params: [Swift.String : Swift.String]?, hashableParams: [Swift.AnyHashable : Any]?, delegate: AequusCore.BannerDelegate) -> AequusCore.Banner?
  public static func createInstance() -> AequusCore.AequusBidBannerFactory
  @objc deinit
}
public struct QueueDispatcher {
  public static func background(_ task: @escaping () -> ())
  public static func backgroundWithDelay(_ delaySeconds: Swift.Int, task: @escaping () -> ())
  public static func main(_ task: @escaping () -> ())
}
public enum CompanionsRequirement : Swift.String, Swift.Codable {
  case all
  case any
  case none
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum ExtraParamsKeys {
  public static let amazon: Swift.String
}
@objc public protocol AequusExtraParameter {
  @objc func setExtraParameter(key: Swift.String, extraParam: Any?)
}
infix operator <<< : BitwiseShiftPrecedence
public struct SHA1 {
  public static func hexString(fromFile filename: Swift.String) -> Swift.String?
  public static func hash(fromFile filename: Swift.String) -> [Swift.Int]?
  public static func hexString(from data: inout Foundation.Data) -> Swift.String?
  public static func hash(from data: inout Foundation.Data) -> [Swift.Int]?
  public static func hexString(from str: Swift.String) -> Swift.String?
  public static func hash(from str: Swift.String) -> [Swift.Int]?
}
@objc(AequusAdType) public enum AdType : Swift.Int {
  case banner
  case interstitial
  case rewarded
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum AequusAdError : Swift.Error {
  case timeout
  case general
  case bannerViewError
  case invalidPlacement
  case notInitialized
  public static func == (a: AequusCore.AequusAdError, b: AequusCore.AequusAdError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension AequusCore.AequusAdError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
@objc public protocol AequusAd : AequusCore.Destroyable, AequusCore.StatusCheck {
  @objc var isAdLoaded: Swift.Bool { get }
}
@objc public protocol BaseAdDelegate {
  @objc func didLoad(ad: AequusCore.AequusAd)
  @objc func failToLoad(ad: AequusCore.AequusAd, with error: Swift.Error)
  @objc func didShow(ad: AequusCore.AequusAd)
  @objc func failToShow(ad: AequusCore.AequusAd, with error: Swift.Error)
  @objc func didHide(ad: AequusCore.AequusAd)
  @objc func didClick(on ad: AequusCore.AequusAd)
  @objc func impression(on ad: AequusCore.AequusAd)
}
@objc public protocol AequusInterstitialDelegate : AequusCore.BaseAdDelegate {
}
@objc public protocol AequusRewardedDelegate : AequusCore.BaseAdDelegate {
  @objc func userRewarded(ad: AequusCore.AequusAd)
  @objc func rewardedVideoStarted(ad: AequusCore.AequusAd)
  @objc func rewardedVideoCompleted(ad: AequusCore.AequusAd)
}
@objc public protocol AequusBannerDelegate : AequusCore.BaseAdDelegate {
}
@objc public protocol AequusFirstAdToDisplayDelegate {
  @objc func firstAdToDisplayChanged(impressionData: AequusCore.ImpressionData, adType: AequusCore.AdType)
}
extension Swift.Collection {
  public subscript(safe index: Self.Index) -> Self.Iterator.Element? {
    get
  }
}
@objc public protocol AequusInterstitial : AequusCore.AequusAd, AequusCore.AequusExtraParameter {
  @objc var interstitialDelegate: AequusCore.AequusInterstitialDelegate? { get set }
  @objc var firstAdToDisplayDelegate: AequusCore.AequusFirstAdToDisplayDelegate? { get set }
  @objc var firstAdToDisplay: AequusCore.ImpressionData? { get }
  @objc func load()
  @objc func show(from viewController: UIKit.UIViewController)
}
@objc public protocol AequusRewardedInterstitial : AequusCore.AequusAd, AequusCore.AequusExtraParameter {
  @objc var rewardedDelegate: AequusCore.AequusRewardedDelegate? { get set }
  @objc var firstAdToDisplayDelegate: AequusCore.AequusFirstAdToDisplayDelegate? { get set }
  @objc var firstAdToDisplay: AequusCore.ImpressionData? { get }
  @objc func load()
  @objc func show(from viewController: UIKit.UIViewController)
}
extension UIKit.UIDevice {
  @_Concurrency.MainActor(unsafe) public static let modelName: (Swift.String, Swift.String)
  @_Concurrency.MainActor(unsafe) public static let ppi: Swift.Int
  @_Concurrency.MainActor(unsafe) public static func mapToDevice(identifier: Swift.String) -> (Swift.String, Swift.String, Swift.Int)
}
public protocol RewardedInterstitial : AequusCore.Destroyable, AequusCore.MultiInstanciable, AequusCore.StatusCheck {
  var delegate: AequusCore.RewardedInterstitialDelegate? { get set }
  var sdkVersion: Swift.String { get }
  func load()
  func show(from viewController: UIKit.UIViewController)
}
public protocol RewardedInterstitialDelegate : AnyObject {
  func didLoad(rewarded: AequusCore.RewardedInterstitial)
  func didShow(rewarded: AequusCore.RewardedInterstitial)
  func impression(rewarded: AequusCore.RewardedInterstitial)
  func didClose(rewarded: AequusCore.RewardedInterstitial)
  func didFailToLoad(rewarded: AequusCore.RewardedInterstitial, error: Swift.Error)
  func didFailToShow(rewarded: AequusCore.RewardedInterstitial, error: Swift.Error)
  func click(rewarded: AequusCore.RewardedInterstitial)
  func eligibleForReward(rewarded: AequusCore.RewardedInterstitial)
  func rewardedVideoDidStart(rewarded: AequusCore.RewardedInterstitial)
  func rewardedVideoDidComplete(rewarded: AequusCore.RewardedInterstitial)
}
public protocol RewardedInterstitialFactory : AequusCore.AdFactory, AequusCore.Initializable {
  func create(viewController: UIKit.UIViewController, id: Swift.String, adUnitConfig: AequusCore.SDKConfig.AdUnitConfig, delegate: AequusCore.RewardedInterstitialDelegate) -> AequusCore.RewardedInterstitial?
}
public protocol BidRewardedInterstitialFactory {
  func create(viewController: UIKit.UIViewController, adId: Swift.String, bidId: Swift.String, adm: Swift.String, params: [Swift.String : Swift.String]?, hashableParams: [Swift.AnyHashable : Any]?, delegate: AequusCore.RewardedInterstitialDelegate) -> AequusCore.RewardedInterstitial?
}
public protocol ExternalBidRewardedInterstitialFactory {
  func create(viewController: UIKit.UIViewController, delegate: AequusCore.RewardedInterstitialDelegate?) -> AequusCore.RewardedInterstitial?
}
@_inheritsConvenienceInitializers @objc(DCREvent) public class DCREvent : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension AequusCore.DCREvent {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<AequusCore.DCREvent>
  @objc @NSManaged dynamic public var abTestGroup: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var abTestID: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var adapter: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var adapterVersion: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var adUnitID: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var appVersion: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var event: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var format: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var isBid: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var latency: Swift.Int64 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var placementID: Swift.Int64 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var publisherID: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var sessionID: Foundation.UUID? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var sessionStartDate: Foundation.Date? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var value: Swift.Int64 {
    @objc get
    @objc set
  }
}
extension AequusCore.DCREvent : Swift.Identifiable {
  public typealias ID = Swift.ObjectIdentifier
}
@_inheritsConvenienceInitializers @objc(AdCapping) public class AdCapping : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
extension AequusCore.AdCapping {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<AequusCore.AdCapping>
  @objc @NSManaged dynamic public var dayAdsShown: Swift.Int64 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var dayStartUTC: Foundation.Date? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var hourAdsShown: Swift.Int64 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var hourStartUTC: Foundation.Date? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var placementID: Swift.Int64 {
    @objc get
    @objc set
  }
}
extension AequusCore.AdCapping : Swift.Identifiable {
  public typealias ID = Swift.ObjectIdentifier
}
extension AequusCore.AequusAdErrorBridged : Swift.Equatable {}
extension AequusCore.AequusAdErrorBridged : Swift.Hashable {}
extension AequusCore.AequusAdErrorBridged : Swift.RawRepresentable {}
extension AequusCore.ILRDNetworkInfo.ILRDNetworkType : Swift.Equatable {}
extension AequusCore.ILRDNetworkInfo.ILRDNetworkType : Swift.Hashable {}
extension AequusCore.ILRDNetworkInfo.ILRDNetworkType : Swift.RawRepresentable {}
extension AequusCore.VastPlayerOptionKey : Swift.Equatable {}
extension AequusCore.VastPlayerOptionKey : Swift.Hashable {}
extension AequusCore.VastPlayerOptionKey : Swift.RawRepresentable {}
extension AequusCore.VastButtonSize : Swift.Equatable {}
extension AequusCore.VastButtonSize : Swift.Hashable {}
extension AequusCore.VastPlayerAdType : Swift.Equatable {}
extension AequusCore.VastPlayerAdType : Swift.Hashable {}
extension AequusCore.CreativeType : Swift.Equatable {}
extension AequusCore.CreativeType : Swift.Hashable {}
extension AequusCore.VastParserError : Swift.Equatable {}
extension AequusCore.VastParserError : Swift.Hashable {}
extension AequusCore.VastViewableImpressionType : Swift.Equatable {}
extension AequusCore.VastViewableImpressionType : Swift.Hashable {}
extension AequusCore.VastViewableImpressionType : Swift.RawRepresentable {}
extension AequusCore.AequusBannerType : Swift.Equatable {}
extension AequusCore.AequusBannerType : Swift.Hashable {}
extension AequusCore.AequusBannerType : Swift.RawRepresentable {}
extension AequusCore.Gender : Swift.Equatable {}
extension AequusCore.Gender : Swift.Hashable {}
extension AequusCore.Gender : Swift.RawRepresentable {}
extension AequusCore.AequusSegment : Swift.Encodable {}
extension AequusCore.AdUnitType : Swift.Equatable {}
extension AequusCore.AdUnitType : Swift.Hashable {}
extension AequusCore.CompanionsRequirement : Swift.Equatable {}
extension AequusCore.CompanionsRequirement : Swift.Hashable {}
extension AequusCore.CompanionsRequirement : Swift.RawRepresentable {}
extension AequusCore.AdType : Swift.Equatable {}
extension AequusCore.AdType : Swift.Hashable {}
extension AequusCore.AdType : Swift.RawRepresentable {}
extension AequusCore.AequusAdError : Swift.Equatable {}
extension AequusCore.AequusAdError : Swift.Hashable {}
