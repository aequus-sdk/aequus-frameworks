//
//  AequusAdColonyBanner.swift
//  AequusAdColonyAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore
import AdColony

final class AequusAdColonyBanner: NSObject, Banner {
    
	weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
        return AdColony.getSDKVersion()
    }
    
    var bannerView: UIView? {
        return banner
    }
    
	var timeout: Bool = false
    var isReady: Bool = false
    private let zoneId: String
    private var viewController: UIViewController
    private var insideView: UIView?
    private var banner: AdColonyAdView?
    private let type: AequusBannerType

    init(zoneId: String, adm: String?, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate) {
        self.delegate = delegate
        self.viewController = viewController
        self.zoneId = zoneId
        self.type = type
    }
    
    func destroy() {
        banner?.destroy()
        banner?.removeFromSuperview()
        banner = nil
    }
    
    func load() {
        AdColony.requestAdView(
            inZone: self.zoneId,
            with: configureSize(type: type),
            viewController: self.viewController,
            andDelegate: self)
    }
    
    private func configureSize(type: AequusBannerType) -> AdColonyAdSize {
        var size: AdColonyAdSize = AdColonyAdSize.init()
        
        switch type {
        case .w320h50:
            size = type.size.width == 320 ? kAdColonyAdSizeBanner : kAdColonyAdSizeLeaderboard
        case .mrec:
            size = kAdColonyAdSizeMediumRectangle
        }
        
        return size
    }
}

extension AequusAdColonyBanner: AdColonyAdViewDelegate {
    
    func adColonyAdViewDidLoad(_ adView: AdColonyAdView) {
        banner = adView
        isReady = true
        delegate?.didLoad(banner: self)
        delegate?.didShow(banner: self)
        delegate?.impression(banner: self)
    }
    
    func adColonyAdViewDidFail(toLoad error: AdColonyAdRequestError) {
        delegate?.failToLoad(banner: self, error: error)
    }
    
    func adColonyAdViewDidReceiveClick(_ adView: AdColonyAdView) {
        delegate?.click(banner: self)
    }
    
}
