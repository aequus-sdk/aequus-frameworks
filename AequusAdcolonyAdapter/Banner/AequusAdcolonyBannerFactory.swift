//
//  AequusAdcolonyBannerFactory.swift
//  AequusAdcolonyAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore
import AdColony

public final class AequusAdcolonyBannerFactory: BannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusAdColonyBanner(zoneId: id, adm: nil, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusAdcolonyBannerFactory {
        return AequusAdcolonyBannerFactory()
    }
}
