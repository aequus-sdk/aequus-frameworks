//
//  AequusAdcolonyBidBannerFactory.swift
//  AequusAdcolonyAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore
import AdColony

public final class AequusAdcolonyBidBannerFactory: BidBannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: BannerDelegate) -> Banner? {
        return AequusAdColonyBanner(zoneId: adId, adm: adm, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusAdcolonyBidBannerFactory {
        return AequusAdcolonyBidBannerFactory()
    }
}

