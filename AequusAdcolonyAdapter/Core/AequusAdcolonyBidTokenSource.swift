//
//  AequusAdcolonyBidTokenSource.swift
//  AequusAdcolonyAdapter
//
//  Created by Bohdan Korda on 25.04.2021.
//

import Foundation
import AequusCore
import AdColony

final class AequusAdcolonyBidTokenSource: BidTokenSource  {
    func invoke(completion: @escaping (String?) -> Void) {
        if !AequusAdcolonyInitializer.isInitialized {
            completion(nil)
            return
        }

        AdColony.collectSignals { (signals, error) in
            completion(signals)
        }
    }
}

extension AequusAdcolonyBidTokenSource: Initializable {
    static func createInstance() -> AequusAdcolonyBidTokenSource {
        return AequusAdcolonyBidTokenSource()
    }
}
