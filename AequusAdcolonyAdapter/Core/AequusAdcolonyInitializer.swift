//
//  AequusAdcolonyInitializer.swift
//  AequusCore
//
//  Created by Bohdan Korda on 24.04.2021.
//

import Foundation
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>4pfyvq9l8r.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>yclnxrl5pm.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>v72qych5uu.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>tl55sbb4fm.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>t38b2kh725.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>prcb7njmu6.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>ppxm28t8ap.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>mlmmfzh3r3.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>klf5c3l5u5.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>hs6bdukanm.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>c6k4g5qg8m.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>9t245vhmpl.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>9rd848q2bz.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>8s468mfl3y.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>7ug5zh24hu.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>4fzdc2evr5.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>4468km3ulz.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>3rd42ekr43.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>2u9pt9hc89.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>m8dbw4sv7c.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>7rz58n8ntl.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>ejvt5qm6ak.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>5lm9lj6jb7.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>44jx6755aq.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>mtkv5xtk9e.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>6g9af3uyq4.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>275upjj5gd.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>9nlqeag3gk.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>cg4yq2srnc.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>g28c52eehv.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>rx5hdcabgc.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>u679fj5vs4.skadnetwork</string>
//</dict>
//<dict>
//<key>SKAdNetworkIdentifier</key>
//<string>uw77j35x4d.skadnetwork</string>
//</dict>
import AequusCore
import AdColony
import Combine

/*
 
 https://github.com/AdColony/AdColony-iOS-SDK/wiki/Project-Setup
 
 With release of iOS 11.3 Apple introduced a new way for install attribution handling. In order to enable us to use it add network IDs presented below to your Info.plist (Ad Network IDs are case-sensitive. A full list of ad network IDs supported by AdColony can be found here)
 
 <key>SKAdNetworkItems</key>
 <array>
     
 </array>
 
 AdColony [*** ERROR ***] : Controller : [error] [App]  SKAdNetworks missing from Info.plist. Expected networks: ["4pfyvq9l8r.skadnetwork","yclnxrl5pm.skadnetwork","v72qych5uu.skadnetwork","tl55sbb4fm.skadnetwork","t38b2kh725.skadnetwork","prcb7njmu6.skadnetwork","ppxm28t8ap.skadnetwork","mlmmfzh3r3.skadnetwork","klf5c3l5u5.skadnetwork","hs6bdukanm.skadnetwork","c6k4g5qg8m.skadnetwork","9t245vhmpl.skadnetwork","9rd848q2bz.skadnetwork","8s468mfl3y.skadnetwork","7ug5zh24hu.skadnetwork","4fzdc2evr5.skadnetwork","4468km3ulz.skadnetwork","3rd42ekr43.skadnetwork","2u9pt9hc89.skadnetwork","m8dbw4sv7c.skadnetwork","7rz58n8ntl.skadnetwork","ejvt5qm6ak.skadnetwork","5lm9lj6jb7.skadnetwork","44jx6755aq.skadnetwork","mtkv5xtk9e.skadnetwork","6g9af3uyq4.skadnetwork","uw77j35x4d.skadnetwork","u679fj5vs4.skadnetwork","rx5hdcabgc.skadnetwork","g28c52eehv.skadnetwork","cg4yq2srnc.skadnetwork","9nlqeag3gk.skadnetwork","275upjj5gd.skadnetwork"]
 
 */

public final class AequusAdcolonyInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
	public static var attempts: Int = 0
    private var cancelable: AnyCancellable?
    
    var appId = ""
    var zoneIds = [String]()
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        if !AequusAdcolonyInitializer.isInitialized {
            if let appId = config?.appID,
               let zoneIds = config?.zoneIds?.components(separatedBy: ",") {
                
                self.zoneIds = zoneIds
                
                cancelable = privacyPublisher.sink { privacy in
                    let options = AdColonyAppOptions()
                    
                    options.setPrivacyFrameworkOfType(ADC_GDPR, isRequired: true)
                    options.setPrivacyConsentString(privacy.GDPRConsent.numericStringValue, forType: ADC_GDPR)
                    
                    options.setPrivacyFrameworkOfType(ADC_CCPA, isRequired: true)
                    options.setPrivacyConsentString(privacy.CCPADoNotSell.numericStringValue, forType: ADC_CCPA)
                    
                    options.setPrivacyFrameworkOfType(ADC_COPPA, isRequired: true)
                    options.setPrivacyConsentString(privacy.CCPADoNotSell.numericStringValue, forType: ADC_COPPA)
                    
                    AdColony.setAppOptions(options)
                }
                
				AequusAdcolonyInitializer.attempts += 1
				let delay = AequusAdcolonyInitializer.attempts > 0 ? 5 : 0
				
				DispatchQueue.global().asyncAfter(deadline: .now() + .seconds(delay)) {
					AdColony.configure(withAppID: appId, options: nil) { zones in
						if zoneIds.count > 0, zones.count > 0 {
							AequusAdcolonyInitializer.isInitialized = true
							completion(true, nil)
						} else {
							completion(false, nil)
						}
					}
				}
				
            } else {
                completion(false, nil)
            }
        }
    }
    
}

extension AequusAdcolonyInitializer: Initializable {
    public static func createInstance() -> AequusAdcolonyInitializer {
        return AequusAdcolonyInitializer()
    }
}
