//
//  AequusAdcolonyRewarded.swift
//  AequusAdcolonyAdapter
//
//  Created by Bohdan Korda on 25.04.2021.
//

import Foundation
import AequusCore
import AdColony

final class AequusAdcolonyRewarded: NSObject, RewardedInterstitial {
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool {
        return ad != nil && !ad!.expired
    }
    
    var sdkVersion: String {
        return AdColony.getSDKVersion()
    }
    
    private var zoneId: String
    private var adm: String?
    
    private var ad: AdColonyInterstitial?
    
    init(zoneId: String, adm: String?, delegate: RewardedInterstitialDelegate) {
        self.zoneId = zoneId
        self.adm = adm
        self.delegate = delegate
        
        super.init()
        
        if let zone = AdColony.zone(forID: zoneId) {
            zone.setReward { [weak self] (success, name, amount) in
                guard let `self` = self else { return }
                if success {
                    self.delegate?.eligibleForReward(rewarded: self)
                }
            }
        }
        
    }
    
    func load() {
        //TODO check options
        let options = AdColonyAdOptions()
        if let adm = adm {
            options.setOption("adm", withStringValue: adm)
        }
        ///
        AdColony.requestInterstitial(inZone: zoneId, options: options, andDelegate: self)
    }
    
    func show(from viewController: UIViewController) {
        self.ad?.show(withPresenting: viewController)
    }
    
    func destroy() {
        self.ad?.cancel()
    }
}

extension AequusAdcolonyRewarded: AdColonyInterstitialDelegate {
    
    func adColonyInterstitialDidLoad(_ interstitial: AdColonyInterstitial) {
        self.ad = interstitial
        self.delegate?.didLoad(rewarded: self)
    }
    
    func adColonyInterstitialDidFail(toLoad error: AdColonyAdRequestError) {
        self.delegate?.didFailToLoad(rewarded: self, error: error)
    }
    
    func adColonyInterstitialDidClose(_ rewarded: AdColonyInterstitial) {
        self.delegate?.didClose(rewarded: self)
    }
    
    func adColonyInterstitialDidReceiveClick(_ rewarded: AdColonyInterstitial) {
        self.delegate?.click(rewarded: self)
    }
    
	func adColonyInterstitialWillOpen(_ interstitial: AdColonyInterstitial) {
		self.delegate?.didShow(rewarded: self)
		self.delegate?.impression(rewarded: self)
	}
}
