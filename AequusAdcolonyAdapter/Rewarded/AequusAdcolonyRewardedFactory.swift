//
//  AequusAdcolonyRewardedFactory.swift
//  AequusAdcolonyAdapter
//
//  Created by Bohdan Korda on 25.04.2021.
//

import Foundation
import AequusCore
import AdColony

public final class AequusAdcolonyRewardedFactory: RewardedInterstitialFactory {
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusAdcolonyRewarded(zoneId: id, adm: nil, delegate: delegate)
    }
    
    public static func createInstance() -> AequusAdcolonyRewardedFactory {
        return AequusAdcolonyRewardedFactory()
    }
    
}

