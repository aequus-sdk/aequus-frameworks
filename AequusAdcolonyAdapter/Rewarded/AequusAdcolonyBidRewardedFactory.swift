//
//  AequusAdcolonyBidRewardedFactory.swift
//  AequusAdcolonyAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import AdColony

final class AequusAdcolonyBidRewardedFactory: BidRewardedInterstitialFactory {
    
    func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusAdcolonyRewarded(zoneId: adId, adm: adm, delegate: delegate)
    }
    
}

extension AequusAdcolonyBidRewardedFactory: Initializable {
    static func createInstance() -> AequusAdcolonyBidRewardedFactory {
        return AequusAdcolonyBidRewardedFactory()
    }
}
