//
//  AequusAdcolonyInterstitial.swift
//  AequusAdColonyAdapter
//
//  Created by Bohdan Korda on 24.04.2021.
//

import Foundation
import AequusCore
import AdColony

class AequusAdcolonyInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
    
    var isReady: Bool {
        return ad != nil && !ad!.expired
    }
    
    var sdkVersion: String {
        return AdColony.getSDKVersion()
    }
    
    private var zoneId: String
    private var adm: String?
    
    private var ad: AdColonyInterstitial?
    
    init(zoneId: String, adm: String?, delegate: InterstitialDelegate) {
        self.adm = adm
        self.zoneId = zoneId
        self.delegate = delegate
    }
    
    func load() {
        //TODO check options
        let options = AdColonyAdOptions()
        if let adm = adm {
            options.setOption("adm", withStringValue: adm)
        }
        ///
        AdColony.requestInterstitial(inZone: zoneId, options: options, andDelegate: self)
    }
    
    func show(from viewController: UIViewController) {
        self.ad?.show(withPresenting: viewController)
    }
    
    func destroy() {
        self.ad?.cancel()
    }
}

extension AequusAdcolonyInterstitial: AdColonyInterstitialDelegate {
    
    func adColonyInterstitialDidLoad(_ interstitial: AdColonyInterstitial) {
        self.ad = interstitial
        self.delegate?.didLoad(interstitial: self)
    }
    
    func adColonyInterstitialDidFail(toLoad error: AdColonyAdRequestError) {
        self.delegate?.didFailToLoad(interstitial: self, error: error)
    }
    
    func adColonyInterstitialDidClose(_ interstitial: AdColonyInterstitial) {
        self.delegate?.didClose(interstitial: self)
    }
    
    func adColonyInterstitialDidReceiveClick(_ interstitial: AdColonyInterstitial) {
        self.delegate?.click(interstitial: self)
    }
	
	func adColonyInterstitialWillOpen(_ interstitial: AdColonyInterstitial) {
		self.delegate?.didShow(interstitial: self)
		self.delegate?.impression(interstitial: self)
	}
    
}
