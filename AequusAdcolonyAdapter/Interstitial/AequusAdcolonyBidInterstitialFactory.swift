//
//  AequusAdcolonyBidInterstitialFactory.swift
//  AequusCore
//
//  Created by Bohdan Korda on 27.04.2021.
//

import Foundation
import AequusCore
import AdColony

final class AequusAdcolonyBidInterstitialFactory: BidInterstitialFactory {
    
    func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusAdcolonyInterstitial(zoneId: adId, adm: adm, delegate: delegate)
    }
    
}

extension AequusAdcolonyBidInterstitialFactory: Initializable {
    static func createInstance() -> AequusAdcolonyBidInterstitialFactory {
        return AequusAdcolonyBidInterstitialFactory()
    }
}
