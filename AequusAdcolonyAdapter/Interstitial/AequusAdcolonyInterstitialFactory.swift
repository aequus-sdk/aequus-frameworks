//
//  AequusAdcolonyInterstitialFactory.swift
//  AequusAdColonyAdapter
//
//  Created by Bohdan Korda on 24.04.2021.
//

import Foundation
import AequusCore
import AdColony

public final class AequusAdcolonyInterstitialFactory: InterstitialFactory {

    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusAdcolonyInterstitial(zoneId: id, adm: nil, delegate: delegate)
    }
    
    public static func createInstance() -> AequusAdcolonyInterstitialFactory {
        return AequusAdcolonyInterstitialFactory()
    }
}

