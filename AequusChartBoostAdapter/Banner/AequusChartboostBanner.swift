//
//  AequusChartboostBanner.swift
//  AequusChartboostAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore

class AequusChartboostBanner: NSObject, Banner, StatusCheck {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
		AequusChartBoostObjcBridge.getSDKVersion()
    }
	
    var isReady: Bool = false
	var timeout: Bool = false

    var bannerView: UIView? {
		return banner?.bannerView()
    }
    
    private let viewController: UIViewController
	private var banner: AequusChartBoostBannerBridged?
    private let unitId: String
    
    init(unitId: String, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
		
		super.init()
		
		banner = AequusChartBoostBannerBridged(location: unitId, delegate: self)
    }
    
    func load() {
		banner?.cache()
    }
    
    func destroy() {
		
    }
}

extension AequusChartboostBanner: AequusChartBoostAdDelegate {
	func didClickAd() {
        delegate?.click(banner: self)
	}
	
	@objc func didDismissAd() {
	}
	
	@objc func didCacheAd(_ ad: AequusChartBoostBaseAd, error: Error?) {
		if let error = error {
			self.delegate?.failToLoad(banner: self, error: error)
		} else {
			self.delegate?.didLoad(banner: self)
			self.banner?.show(from: viewController)
		}
	}
	
	@objc func didShow(_ ad: AequusChartBoostBaseAd, error: Error?) {
		if error == nil {
			self.delegate?.didShow(banner: self)
			self.delegate?.impression(banner: self)
		}
	}
}
