//
//  AequusChartboostBannerFactory.swift
//  AequusChartboostAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusChartboostBannerFactory: BannerFactory {
	public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
		return AequusChartboostBanner(unitId: id, viewController: viewController, delegate: delegate)
	}

    public static func createInstance() -> AequusChartboostBannerFactory {
        return AequusChartboostBannerFactory()
    }
}
