//
//  AequusChartBoostAdDelegate.h
//  Pods
//
//  Created by Bohdan Korda on 09.04.2021.
//

#ifndef AequusChartBoostAdDelegate_h
#define AequusChartBoostAdDelegate_h

@class AequusChartBoostBaseAd;

@protocol AequusChartBoostAdDelegate

- (void)didCacheAd:(AequusChartBoostBaseAd * _Nonnull )ad error:(nullable NSError *)error;
- (void)didShowAd:(AequusChartBoostBaseAd * _Nonnull)ad error:(nullable NSError *)error;
- (void)didDismissAd;
- (void)didClickAd;

@optional
- (void)didEarnReward:(AequusChartBoostBaseAd * _Nonnull)ad;
- (void)didCompleteRewardVideo:(AequusChartBoostBaseAd * _Nonnull)ad;

@end

#endif /* AequusChartBoostAdDelegate_h */
