//
//  AequusChartBoostBaseAd.h
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import <Foundation/Foundation.h>
#import "AequusChartBoostAdDelegate.h"

@class CHBCacheError;
@class CHBShowError;

@interface AequusChartBoostBaseAd : NSObject

/*!
 @brief Chartboost location for the ad.
 @discussion Used to obtain ads with increased performance.
 */
@property (nonatomic, readonly) NSString * _Nonnull location;

/*!
 @brief The delegate instance to receive interstitial callbacks.
 */
@property (nonatomic, weak, nullable) id<AequusChartBoostAdDelegate> delegate;


/*!
 @brief Determines if a cached ad exists.
 @return YES if there is a cached ad, and NO if not.
 @discussion A return value of YES here indicates that it is safe to call the showFromViewController: method.
 Calling this method when this value is NO will cause the show request to fail with a CHBShowErrorCodeNoCachedAd error.
 */
@property (nonatomic, readonly) BOOL isCached;


/*!
 @brief The initializer for the interstitial object.
 @param location Location for the interstitial. See the location property documentation.
 @param delegate Delegate for the interstitial. See the delegate property documentation.
 */
- (instancetype _Nullable )initWithLocation:(nonnull NSString *)location delegate:(nullable id<AequusChartBoostAdDelegate>)delegate;

/*!
 @brief Caches an ad.
 @discussion This method will first check if there is a cached ad and, if found, will do nothing.
 If no cached ad exists the method will attempt to fetch it from the Chartboost server.
 Implement didCacheAd:error: in your ad delegate to be notified of a cache request result.
 */
- (void)cache;

/*!
 @brief Shows an ad.
 @param viewController The view controller to present the ad on.
 @discussion This method will first check if there is a cached ad, if found it will present it.
 If no cached ad exists the request will fail with a CHBShowErrorCodeNoCachedAd error.
 It is highly recommended that a non-nil view controller is passed, as it is required for enhanced ad presentation and some features like opening links in an in-app web browser.
 Implement didShowAd:error: in your ad delegate to be notified of a show request result.
 */
- (void)showFromViewController:(nullable UIViewController *)viewController;


- (NSError *_Nullable)mapCHBCacheError:(CHBCacheError *_Nullable)error;
- (NSError *_Nullable)mapCHBShowError:(CHBShowError *_Nullable)error;

@end
