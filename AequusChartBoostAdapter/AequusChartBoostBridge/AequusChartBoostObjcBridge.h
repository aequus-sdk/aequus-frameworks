//
//  AequusChartBoostObjcBridge.h
//  AequusChartBoostAdapter
//
//  Created by Oleh Veheria on 24.03.2021.
//

#import <Foundation/Foundation.h>

@interface AequusChartBoostObjcBridge : NSObject

+(AequusChartBoostObjcBridge *) shared;

- (void)initSDK:(NSString *)appId appSignature:(NSString *)appSignature completion:(void (^)(BOOL))completion;
+ (NSString *)getSDKVersion;
+ (void)setCCPAConsent:(BOOL)consent;
+ (void)setGDPRConsent:(BOOL)consent;

@end
