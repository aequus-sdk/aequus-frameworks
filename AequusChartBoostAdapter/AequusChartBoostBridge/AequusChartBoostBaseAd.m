//
//  AequusChartBoostBaseAd.m
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import "AequusChartBoostBaseAd.h"
#import "ChartboostSDK/ChartboostSDK.h"

@implementation AequusChartBoostBaseAd

- (NSError *)mapCHBCacheError:(CHBCacheError *)error {
    if (error.code == CHBCacheErrorCodeNoAdFound) {
		return [self makeError:error.code with:@"No Ad Found"];
    } else if (error.code == CHBCacheErrorCodeAssetDownloadFailure) {
		return [self makeError:error.code with:@"Asset Download Failure"];
    } else if (error.code == CHBCacheErrorCodePublisherDisabled) {
		return [self makeError:error.code with:@"Publisher Disabled"];
    } else if (error.code == CHBCacheErrorCodeSessionNotStarted) {
		return [self makeError:error.code with:@"Session Not Started"];
    } else if (error.code == CHBCacheErrorCodeNetworkFailure) {
		return [self makeError:error.code with:@"Network Failure"];
    } else if (error.code == CHBCacheErrorCodeInternetUnavailable) {
		return [self makeError:error.code with:@"Internet Unavailable"];
    }
	return [self makeError:error.code with:@"unknown error"];
}

- (NSError *)mapCHBShowError:(CHBShowError *)error {
    if (error.code == CHBShowErrorCodeNoCachedAd) {
		return [self makeError:error.code with:@"No Cached Ad"];
    } else if (error.code == CHBShowErrorCodePresentationFailure) {
		return [self makeError:error.code with:@"Presentation Failure"];
    } else if (error.code == CHBShowErrorCodeSessionNotStarted) {
		return [self makeError:error.code with:@"Session Not Started"];
    } else if (error.code == CHBShowErrorCodeInternetUnavailable) {
		return [self makeError:error.code with:@"Internet Unavailable"];
    }
	return [self makeError:error.code with:@"unknown error"];
}

- (NSError *)makeError:(NSInteger)code with:(NSString *)message {
	NSMutableDictionary* details = [NSMutableDictionary dictionary];
	[details setValue:message forKey:NSLocalizedDescriptionKey];
	return [[NSError alloc] initWithDomain:@"com.aequus.ads" code:code userInfo:details];
}

- (void)cache {
	@throw [NSException exceptionWithName:NSInternalInconsistencyException
								   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
								 userInfo:nil];
}

- (void)showFromViewController:(UIViewController *)viewController {
	@throw [NSException exceptionWithName:NSInternalInconsistencyException
								   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
								 userInfo:nil];
}

- (instancetype)initWithLocation:(NSString *)location delegate:(id<AequusChartBoostAdDelegate>)delegate {
	@throw [NSException exceptionWithName:NSInternalInconsistencyException
								   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
								 userInfo:nil];
}

@end
