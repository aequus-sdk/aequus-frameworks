//
//  AequusChartBoostBannerBridged.h
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import <Foundation/Foundation.h>
#import "AequusChartBoostBaseAd.h"

NS_ASSUME_NONNULL_BEGIN

@interface AequusChartBoostBannerBridged : AequusChartBoostBaseAd

/*!
 @brief Please use initWithLocation:delegate: instead.
*/
- (UIView *)bannerView;
- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
