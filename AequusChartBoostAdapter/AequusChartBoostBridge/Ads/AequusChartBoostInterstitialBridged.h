//
//  AequusChartBoostInterstitial.h
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import <Foundation/Foundation.h>
#import "AequusChartBoostBaseAd.h"
#import "AequusChartBoostAdDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface AequusChartBoostInterstitialBridged : AequusChartBoostBaseAd

/*!
 @brief Please use initWithLocation:delegate: instead.
*/
- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
