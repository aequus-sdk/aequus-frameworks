//
//  AequusChartBoostRewardedBridged.m
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import "AequusChartBoostRewardedBridged.h"
#import "ChartboostSDK/ChartboostSDK.h"


@interface AequusChartBoostRewardedBridged () <CHBRewardedDelegate>

@property (nonatomic, strong) CHBRewarded *rewarded;

@end


@implementation AequusChartBoostRewardedBridged

@dynamic delegate;

- (BOOL)isCached {
    return self.rewarded.isCached;
}

- (instancetype)initWithLocation:(NSString *)location delegate:(nullable id<AequusChartBoostAdDelegate>)delegate {
    if (self = [[AequusChartBoostRewardedBridged alloc] init]) {
        self.delegate = delegate;
        self.rewarded = [[CHBRewarded alloc] initWithLocation:location delegate:self];
    }
    return self;
}

- (void)cache {
    [[self rewarded] cache];
}

- (void)showFromViewController:(nullable UIViewController *)viewController {
    [[self rewarded] showFromViewController:viewController];
}

#pragma mark delegate

- (void)didCacheAd:(CHBCacheEvent *)event error:(CHBCacheError *)error {
    NSError *errorMessage;
    
    if (error != nil) {
        errorMessage = [self mapCHBCacheError:error];
    }
        
    [self.delegate didCacheAd:self error:errorMessage];
}

- (void)didShowAd:(CHBShowEvent *)event error:(CHBShowError *)error {
	NSError *errorMessage;
    
    if (error != nil) {
        errorMessage = [self mapCHBShowError:error];
    }
    
    [self.delegate didShowAd:self error:errorMessage];
}

- (void)didEarnReward:(CHBRewardEvent *)event {
    [self.delegate didEarnReward:self];
}

- (void)didDismissAd:(CHBDismissEvent *)event {
    [self.delegate didDismissAd];
}

- (void)didClickAd:(CHBClickEvent *)event error:(CHBClickError *)error {
	if (error == nil) {
		[self.delegate didClickAd];
	}
}

@end
