//
//  AequusChartBoostInterstitial.m
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import "AequusChartBoostInterstitialBridged.h"
#import "ChartboostSDK/ChartboostSDK.h"

@interface AequusChartBoostInterstitialBridged () <CHBInterstitialDelegate>

@property (nonatomic, strong) CHBInterstitial *interstitial;

@end


@implementation AequusChartBoostInterstitialBridged

@dynamic delegate;

- (BOOL)isCached {
    return self.interstitial.isCached;
}

- (instancetype)initWithLocation:(NSString *)location delegate:(nullable id<AequusChartBoostAdDelegate>)delegate {
    
    if (self = [[AequusChartBoostInterstitialBridged alloc] init]) {
        self.delegate = delegate;
        self.interstitial = [[CHBInterstitial alloc] initWithLocation:location delegate:self];
    }
    return self;
}

- (void)cache {
    [[self interstitial] cache];
}

- (void)showFromViewController:(nullable UIViewController *)viewController {
    [[self interstitial] showFromViewController:viewController];
}

#pragma mark delegate

- (void)didCacheAd:(CHBCacheEvent *)event error:(CHBCacheError *)error {
    NSError *mappedError;
    
    if (error != nil) {
		mappedError = [self mapCHBCacheError:error];
    }
        
    [self.delegate didCacheAd:self error:mappedError];
}

- (void)didShowAd:(CHBShowEvent *)event error:(CHBShowError *)error {
    NSError *mappedError;
    
    if (error != nil) {
		mappedError = [self mapCHBShowError:error];
    }
    
    [self.delegate didShowAd:self error:mappedError];
}

- (void)didDismissAd:(CHBDismissEvent *)event {
	[self.delegate didDismissAd];
}

- (void)didClickAd:(CHBClickEvent *)event error:(CHBClickError *)error {
	if (error == nil) {
		[self.delegate didClickAd];
	}
}

@end
