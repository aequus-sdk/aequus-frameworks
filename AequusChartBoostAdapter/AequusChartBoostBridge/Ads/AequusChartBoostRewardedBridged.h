//
//  AequusChartBoostRewarded.h
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import <Foundation/Foundation.h>
#import "AequusChartBoostBaseAd.h"


NS_ASSUME_NONNULL_BEGIN

@interface AequusChartBoostRewardedBridged : AequusChartBoostBaseAd

/*!
 @brief Please use initWithLocation:delegate: instead.
*/
- (instancetype)init NS_UNAVAILABLE;

/*!
 @brief Shows an ad.
 @param viewController The view controller to present the ad on.
 @discussion This method will first check if there is a cached ad, if found it will present it.
 If no cached ad exists the request will fail with a CHBShowErrorCodeNoCachedAd error.
 It is highly recommended that a non-nil view controller is passed, as it is required for enhanced ad presentation and some features like opening links in an in-app web browser.
 Implement didShowAd:error: in your ad delegate to be notified of a show request result.
 */
- (void)showFromViewController:(nullable UIViewController *)viewController;

@end

NS_ASSUME_NONNULL_END
