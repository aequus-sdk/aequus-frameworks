//
//  AequusChartBoostBannerAdBridged.m
//  AequusChartBoostAdapter
//
//  Created by Bohdan Korda on 09.04.2021.
//

#import "AequusChartBoostBannerBridged.h"
#import "ChartboostSDK/ChartboostSDK.h"

@interface AequusChartBoostBannerBridged ()

@property (nonatomic, strong) CHBBanner *banner;

@end

@interface AequusChartBoostBannerBridged (Delegate) <CHBBannerDelegate>
@end

@implementation AequusChartBoostBannerBridged

@dynamic delegate;

- (UIView *)bannerView {
	return self.banner;
}

- (BOOL)isCached {
    return self.banner.isCached;
}

- (instancetype)initWithLocation:(NSString *)location delegate:(nullable id<AequusChartBoostAdDelegate>)delegate {
    
    if (self = [[AequusChartBoostBannerBridged alloc] init]) {
        self.delegate = delegate;
        
        //TODO: get banner size
        self.banner = [[CHBBanner alloc] initWithSize:CHBBannerSizeStandard location:location delegate:self];
    }
	
    return self;
}

- (void)cache {
    [[self banner] cache];
}

- (void)showFromViewController:(nullable UIViewController *)viewController {
    [[self banner] showFromViewController:viewController];
}

#pragma mark delegate

- (void)didClickAd:(CHBClickEvent *)event error:(CHBClickError *)error {
    [self.delegate didClickAd];
}

- (void)didCacheAd:(CHBCacheEvent *)event error:(CHBCacheError *)error {
    NSError *mappedError;
    
    if (error != nil) {
		mappedError = [self mapCHBCacheError:error];
    }
        
    [self.delegate didCacheAd:self error:mappedError];
}

- (void)didShowAd:(CHBShowEvent *)event error:(CHBShowError *)error {
    
	NSError *errorMessage;
    
    if (error != nil) {
        errorMessage = [self mapCHBShowError:error];
    }
    
    [self.delegate didShowAd:self error:errorMessage];
}

@end
