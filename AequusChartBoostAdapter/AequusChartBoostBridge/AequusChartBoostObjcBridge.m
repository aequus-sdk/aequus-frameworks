//
//  AequusChartBoostObjcBridge.m
//  AequusChartBoostAdapter
//
//  Created by Oleh Veheria on 24.03.2021.
//

#import "AequusChartBoostObjcBridge.h"
#import "ChartboostSDK/ChartboostSDK.h"

@implementation AequusChartBoostObjcBridge

+ (instancetype)shared {
    static AequusChartBoostObjcBridge *sharedInstance = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedInstance = [[AequusChartBoostObjcBridge alloc] init];
            // Do any other initialisation stuff here
        });
        return sharedInstance;
}

- (void)initSDK:(NSString *)appId appSignature:(NSString *)appSignature completion:(void (^)(BOOL))completion {
    [Chartboost startWithAppID:appId appSignature:appSignature completion:^(CHBStartError * _Nullable error) {
        if (error != NULL) {
            completion(true);
        } else {
            completion(false);
        }
    }];
}

+ (NSString *)getSDKVersion {
	return [Chartboost getSDKVersion];
}

+ (void)setGDPRConsent:(BOOL)consent {
	CHBGDPRDataUseConsent *gdprConsent;
	
	if (consent) {
		gdprConsent = [CHBGDPRDataUseConsent gdprConsent:CHBGDPRConsentBehavioral];
	} else {
		gdprConsent = [CHBGDPRDataUseConsent gdprConsent:CHBGDPRConsentNonBehavioral];
	}
	[Chartboost addDataUseConsent:gdprConsent];
}

+ (void)setCCPAConsent:(BOOL)consent {
	CHBCCPADataUseConsent *ccpaConsent;
	
	if (consent) {
		ccpaConsent = [CHBCCPADataUseConsent ccpaConsent:CHBCCPAConsentOptInSale];
	} else {
		ccpaConsent = [CHBCCPADataUseConsent ccpaConsent:CHBCCPAConsentOptOutSale];
	}
	[Chartboost addDataUseConsent:ccpaConsent];
}

@end
