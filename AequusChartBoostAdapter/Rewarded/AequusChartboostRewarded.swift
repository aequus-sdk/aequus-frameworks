//
//  AequusChartboostRewarded.swift
//  AequusChartboostAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

final class AequusChartboostRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool {
		rewarded?.isCached ?? false
    }
    
    var sdkVersion: String {
		AequusChartBoostObjcBridge.getSDKVersion()
    }
    
    private let unitId: String
	private var rewarded: AequusChartBoostRewardedBridged?
    
    init(unitId: String, delegate: RewardedInterstitialDelegate?) {
 
        self.delegate = delegate
        self.unitId = unitId
            
        super.init()
        
		rewarded = AequusChartBoostRewardedBridged(location: unitId, delegate: self)
    }
    
    func load() {
		rewarded?.cache()
    }
    
    func show(from viewController: UIViewController) {
		rewarded?.show(from: viewController)
    }
    
    
    func destroy() {
		rewarded?.delegate = nil
    }
}

extension AequusChartboostRewarded: AequusChartBoostAdDelegate {
    
	@objc func didCacheAd(_ ad: AequusChartBoostBaseAd, error: Error?) {
		if let error = error {
			self.delegate?.didFailToLoad(rewarded: self, error: error)
		} else {
			self.delegate?.didLoad(rewarded: self)
		}
	}
	
	@objc func didShow(_ ad: AequusChartBoostBaseAd, error: Error?) {
		if let error = error {
			self.delegate?.didFailToShow(rewarded: self, error: error)
		} else {
			self.delegate?.didShow(rewarded: self)
			self.delegate?.impression(rewarded: self)
		}
	}
	
	@objc func didDismissAd() {
		delegate?.didClose(rewarded: self)
	}
    
	
	@objc func didEarnReward(_ ad: AequusChartBoostBaseAd) {
		delegate?.eligibleForReward(rewarded: self)
	}
	
	@objc func didClickAd() {
		delegate?.click(rewarded: self)
	}
}
