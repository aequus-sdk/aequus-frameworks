//
//  AequusChartboostRewardedFactory.swift
//  AequusChartboostAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusChartboostRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusChartboostRewarded( unitId: id, delegate: delegate)
    }
    
}

extension AequusChartboostRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusChartboostRewardedFactory {
        return AequusChartboostRewardedFactory()
    }
}
