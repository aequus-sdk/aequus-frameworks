//
//  AequusChartboostInterstitialFactory.swift
//  AequusChartboostAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusChartboostInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusChartboostInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusChartboostInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusChartboostInterstitialFactory {
        return AequusChartboostInterstitialFactory()
    }
}
