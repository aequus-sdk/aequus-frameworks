//
//  AequusChartboostInterstitial.swift
//  AequusChartboostAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

final class AequusChartboostInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
	
    var isReady: Bool {
		interstitial?.isCached ?? false
    }
    
    var sdkVersion: String {
		AequusChartBoostObjcBridge.getSDKVersion()
    }
	
    private let unitId: String
	private var interstitial: AequusChartBoostInterstitialBridged?
    
    init(unitId: String, delegate: InterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
            
        super.init()
		
		interstitial = AequusChartBoostInterstitialBridged.init(location: unitId, delegate: self)
    }
    
    func load() {
		interstitial?.cache()
    }
    
    func show(from viewController: UIViewController) {
		interstitial?.show(from: viewController)
    }
    
    func destroy() {
		interstitial?.delegate = nil
    }
    
}

extension AequusChartboostInterstitial: AequusChartBoostAdDelegate {
	@objc func didCacheAd(_ ad: AequusChartBoostBaseAd, error: Error?) {
		if let error = error {
			self.delegate?.didFailToLoad(interstitial: self, error: error)
		} else {
			self.delegate?.didLoad(interstitial: self)
		}
	}
	
	@objc func didShow(_ ad: AequusChartBoostBaseAd, error: Error?) {
		if let error = error {
			self.delegate?.didFailToShow(interstitial: self, error: error)
		} else {
			self.delegate?.didShow(interstitial: self)
			self.delegate?.impression(interstitial: self)
		}
	}
	
	@objc func didDismissAd() {
		delegate?.didClose(interstitial: self)
	}
	
	@objc func didClickAd() {
		delegate?.click(interstitial: self)
	}
}
