//
//  AequusChartboostInitializer.swift
//  AequusChartboostAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine

public enum AequusChartboostInitializingError : Error {
    case some
}

public final class AequusChartboostInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        if !AequusChartboostInitializer.isInitialized {
            
            cancelable = privacyPublisher.sink { privacy in
				AequusChartBoostObjcBridge.setGDPRConsent(privacy.GDPRConsent)
				AequusChartBoostObjcBridge.setCCPAConsent(privacy.CCPADoNotSell)
            }
            
            if let config = config,
                let appId = config.appID,
                let appSignature = config.appSignature {
                
				AequusChartBoostObjcBridge.shared().initSDK(appId, appSignature: appSignature) { ready in
					AequusChartboostInitializer.isInitialized = ready
					completion(ready, nil)
				}
                
            } else {
                completion(false, AequusChartboostInitializingError.some)
            }
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusChartboostInitializer: Initializable {
    public static func createInstance() -> AequusChartboostInitializer {
        return AequusChartboostInitializer()
    }
}
