//
//  AequusBidMachineBannerFactory.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr Poliakov on 28.02.2022.
//

import AequusCore

public final class AequusBidmachineExternalBidBannerFactory: ExternalBidBannerFactory {
	
    public func create(viewController: UIViewController, type: AequusBannerType, delegate: BannerDelegate?) -> Banner? {
        if type == .w320h50 {
            guard AequusBidmachineInitializer.bannerUnitId != nil else { return nil }
            
            return AequusBidmachineBanner(viewController: viewController, type: type, delegate: delegate)
        } else {
            guard AequusBidmachineInitializer.mrecUnitId != nil else { return nil }
            
            return AequusBidmachineBanner(viewController: viewController, type: type, delegate: delegate)
        }
    }

    public static func createInstance() -> AequusBidmachineExternalBidBannerFactory {
        return AequusBidmachineExternalBidBannerFactory()
    }
}
