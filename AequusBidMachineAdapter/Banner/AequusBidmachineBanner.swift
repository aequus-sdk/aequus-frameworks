//
//  AequusBidMachineBanner.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr Poliakov on 28.02.2022.
//

import AequusCore
import BidMachine

final class AequusBidmachineBanner: NSObject, Banner, ExternalBidAdUnit, Impressionable {
    
    var adUnit: String = ""
    weak var delegate: BannerDelegate?
    
    var completion: ((Bool) -> ())?
	
    var sdkVersion: String {
        kBDMVersion
    }
    var isReady: Bool = false
	
    var bannerView: UIView? {
        return banner
    }
	
	var price: Int64 = -1
	
    private let viewController: UIViewController
    private var banner: BDMBannerView?
    private var request: BDMBannerRequest?
	private var type: AequusBannerType
	private var bidCompletionCalled: Bool = false
	var timeout: Bool = false
    
	init(viewController: UIViewController, type: AequusBannerType, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.delegate = delegate
		self.type = type
        self.adUnit = type == .w320h50 ? AequusBidmachineInitializer.bannerUnitId ?? "" : AequusBidmachineInitializer.mrecUnitId ?? ""
        
        super.init()
    }
    
    func load() {
		guard let request = request else {
			delegate?.failToLoad(banner: self, error: AequusBidmachineError.cantGetBid)
			return
		}
        
		let size: BDMBannerAdSize = configureSize(type: type)
		let banner = BDMBannerView(frame: CGRect(origin: .zero, size: CGSizeFromBDMSize(size)))
		banner.delegate = self
		banner.producerDelegate = self
		self.banner = banner
		banner.populate(with: request)
    }
    
	func requestBid(completion: @escaping (Bool) -> ()) {
        self.completion = completion
		bidCompletionCalled = false
        request = BDMBannerRequest()
        request?.placementId = adUnit
        request?.adSize = configureSize(type: type)
        request?.perform(with: self)
    }
    
    func destroy() {
		DispatchQueue.main.async {
			self.banner?.removeFromSuperview()
			self.banner?.delegate = nil
			self.banner = nil
			self.delegate = nil
			self.completion = nil
		}
    }
    
    private func configureSize(type: AequusBannerType) -> BDMBannerAdSize {
        var size: BDMBannerAdSize = BDMBannerAdSize.sizeUnknown
        
        switch type {
        case .mrec:
            size = .size300x250
        case .w320h50:
            size = type.size.width == 320 ? .size320x50 : .size728x90
        }
        
        return size
    }
}

// MARK: - BDMRequestDelegate

extension AequusBidmachineBanner: BDMRequestDelegate {
    func requestDidExpire(_ request: BDMRequest) {
        // expire
    }
    
    func request(_ request: BDMRequest, failedWithError error: Error) {
		if !bidCompletionCalled {
			completion?(false)
			bidCompletionCalled.toggle()
		}
    }
    
    func request(_ request: BDMRequest, completeWith info: BDMAuctionInfo) {
		if !bidCompletionCalled {
			self.price = DollarToMicrodollarConverter.convert(dollar: Double(truncating: info.price ?? 0))
			completion?(true)
			bidCompletionCalled.toggle()
		}
    }
}

extension AequusBidmachineBanner: BDMAdEventProducerDelegate {
	func didProduceUserAction(_ producer: BDMAdEventProducer) {}
	
	func didProduceImpression(_ producer: BDMAdEventProducer) {
		delegate?.didShow(banner: self)
		delegate?.impression(banner: self)
	}
}

// MARK: - BDMBannerDelegate

extension AequusBidmachineBanner: BDMBannerDelegate {
    func bannerViewReady(toPresent bannerView: BDMBannerView) {
		delegate?.didLoad(banner: self)
    }
    
    func bannerViewRecieveUserInteraction(_ bannerView: BDMBannerView) {
        delegate?.click(banner: self)
    }
    
    func bannerView(_ bannerView: BDMBannerView, failedWithError error: Error) {
        delegate?.failToLoad(banner: self, error: error)
    }
}
