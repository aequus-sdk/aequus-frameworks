//
//  AequusBidMachineInitializer.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr Poliakov on 28.02.2022.
//

import Foundation
import AequusCore
import Combine
import BidMachine

enum AequusBidmachineError: LocalizedError {
	case cantGetBid
	case adNotLoaded
}

public final class AequusBidmachineInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    public static var bannerUnitId, interstitialUnitId, rewardedUnitId, mrecUnitId: String?
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        cancelable = privacyPublisher.sink { privacy in
            let restrictions = BDMSdk.shared().restrictions
            restrictions.coppa = privacy.underAgeOfConsent
            restrictions.subjectToGDPR = privacy.GDPRConsent
        }
        
        AequusBidmachineInitializer.bannerUnitId = config?.bannerUnitId
        AequusBidmachineInitializer.interstitialUnitId = config?.interstitialUnitId
        AequusBidmachineInitializer.rewardedUnitId = config?.rewardedUnitId
        AequusBidmachineInitializer.mrecUnitId = config?.mrecUnitId
        
        if let selledId = config?.sellerId,
		   let storeURL = config?.storeUrl,
		   let storeID = config?.storeId {
			
			let configuration = BDMSdkConfiguration()
			let targeting = BDMTargeting()
			targeting.storeId = storeID
			targeting.storeURL = URL(string: storeURL)
			configuration.targeting = targeting
			
            BDMSdk.shared().startSession(withSellerID: selledId, configuration: configuration) {
				AequusBidmachineInitializer.isInitialized = true
                completion(true, nil)
            }
        } else {
            completion(false, nil)
        }
    }
}

extension AequusBidmachineInitializer: Initializable {
    public static func createInstance() -> AequusBidmachineInitializer {
        return AequusBidmachineInitializer()
    }
}
