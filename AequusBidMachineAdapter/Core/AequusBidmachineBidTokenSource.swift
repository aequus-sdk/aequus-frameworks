//
//  AequusBidMachineBidTokenSource.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr on 01.03.2022.
//

import AequusCore
import BidMachine

final class AequusBidmachineBidTokenSource: BidTokenSource {
    func invoke(completion: @escaping (String?) -> Void) {
        guard
            let bidToken = BDMSdk.shared().biddingToken else {
            completion(nil)
            return
        }
        
        completion(bidToken)
    }
}

extension AequusBidmachineBidTokenSource: Initializable {
    static func createInstance() -> AequusBidmachineBidTokenSource {
        return AequusBidmachineBidTokenSource()
    }
}
