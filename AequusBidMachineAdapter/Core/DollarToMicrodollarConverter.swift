//
//  DollarToMicrodollarConverter.swift
//  AequusBidmachineAdapter
//
//  Created by Bohdan Korda on 29.04.2022.
//

import Foundation

struct DollarToMicrodollarConverter {
	static func convert(dollar: Double) -> Int64 {
		return Int64(dollar * 1_000_000)
	}
}
