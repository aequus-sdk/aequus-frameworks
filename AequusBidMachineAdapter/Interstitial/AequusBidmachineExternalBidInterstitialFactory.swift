//
//  AequusBidMachineInterstitialFactory.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr Poliakov on 28.02.2022.
//

import Foundation
import AequusCore

public final class AequusBidmachineExternalBidInterstitialFactory: ExternalBidInterstitialFactory {
    public func create(viewController: UIViewController, delegate: InterstitialDelegate?) -> Interstitial? {
        guard AequusBidmachineInitializer.interstitialUnitId != nil else { return nil  }
        
        return AequusBidmachineInterstitial(delegate: delegate)
    }
}

extension AequusBidmachineExternalBidInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusBidmachineExternalBidInterstitialFactory {
        return AequusBidmachineExternalBidInterstitialFactory()
    }
}
