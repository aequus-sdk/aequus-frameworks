//
//  AequusBidMachineInterstitial.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr Poliakov on 28.02.2022.
//

import BidMachine
import AequusCore

final class AequusBidmachineInterstitial: NSObject, Interstitial, ExternalBidAdUnit, Impressionable {
    
    var adUnit: String = AequusBidmachineInitializer.interstitialUnitId ?? ""
	weak var delegate: InterstitialDelegate?
    
	var completion: ((Bool) -> ())?
	
	var isReady: Bool {
        return interstitial != nil && interstitial?.canShow == true
	}
	
	var sdkVersion: String {
        kBDMVersion
	}
	
	var price: Int64 = -1
	
	private var interstitial: BDMInterstitial?
    private var request: BDMInterstitialRequest?
	private var bidCompletionCalled: Bool = false
	
	init(delegate: InterstitialDelegate?) {
		self.delegate = delegate
		
		super.init()
	}
	
	func load() {
		guard let request = request else {
			delegate?.didFailToLoad(interstitial: self, error: AequusBidmachineError.cantGetBid)
			return
		}
		
		DispatchQueue.main.async {
			self.interstitial = BDMInterstitial()
			self.interstitial?.delegate = self
			self.interstitial?.producerDelegate = self
			self.interstitial?.populate(with: request)
		}
    }
	
	func show(from viewController: UIViewController) {
        guard let interstitial = self.interstitial, interstitial.canShow else {
            delegate?.didFailToShow(interstitial: self, error: AequusBidmachineError.adNotLoaded)
            return
        }

        DispatchQueue.main.async {
            interstitial.present(fromRootViewController: viewController)
        }
	}
    
    func requestBid(completion: @escaping (Bool) -> ()) {
        self.completion = completion
		self.request = BDMInterstitialRequest()
        self.request?.placementId = adUnit
		self.request?.perform(with: self)
    }
	
	func destroy() {
        DispatchQueue.main.async {
			self.interstitial?.delegate = nil
            self.interstitial = nil
			self.delegate = nil
			self.completion = nil
        }
	}
	
}

extension AequusBidmachineInterstitial: BDMAdEventProducerDelegate {
	func didProduceUserAction(_ producer: BDMAdEventProducer) {}

	func didProduceImpression(_ producer: BDMAdEventProducer) {
		delegate?.impression(interstitial: self)
	}
}

// MARK: - BDMRequestDelegate

extension AequusBidmachineInterstitial: BDMRequestDelegate {
    func requestDidExpire(_ request: BDMRequest) {
        
    }
    
    func request(_ request: BDMRequest, failedWithError error: Error) {
		if !bidCompletionCalled {
			completion?(false)
			bidCompletionCalled.toggle()
		}
    }
    
    func request(_ request: BDMRequest, completeWith info: BDMAuctionInfo) {
		if !bidCompletionCalled {
			self.price = DollarToMicrodollarConverter.convert(dollar: Double(truncating: info.price ?? 0))
			completion?(true)
			bidCompletionCalled.toggle()
		}
    }
}

// MARK: - BDMInterstitialDelegate

extension AequusBidmachineInterstitial: BDMInterstitialDelegate {
    func interstitial(_ interstitial: BDMInterstitial, failedWithError error: Error) {
        delegate?.didFailToLoad(interstitial: self, error: error)
    }
    
    func interstitial(_ interstitial: BDMInterstitial, failedToPresentWithError error: Error) {
        delegate?.didFailToShow(interstitial: self, error: error)
    }
    
    func interstitialReady(toPresent interstitial: BDMInterstitial) {
        delegate?.didLoad(interstitial: self)
    }
    
    func interstitialWillPresent(_ interstitial: BDMInterstitial) {
        delegate?.didShow(interstitial: self)
    }
    
    func interstitialDidDismiss(_ interstitial: BDMInterstitial) {
        delegate?.didClose(interstitial: self)
    }
    
    func interstitialRecieveUserInteraction(_ interstitial: BDMInterstitial) {
        delegate?.click(interstitial: self)
    }
}
