//
//  AequusBidMachineRewardedFactory.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr Poliakov on 28.02.2022.
//

import Foundation
import AequusCore

public final class AequusBidmachineExternalBidRewardedFactory: ExternalBidRewardedInterstitialFactory {
    public func create(viewController: UIViewController, delegate: RewardedInterstitialDelegate?) -> RewardedInterstitial? {
        guard AequusBidmachineInitializer.rewardedUnitId != nil else { return nil  }

        return AequusBidmachineRewarded(delegate: delegate)
    }
}

extension AequusBidmachineExternalBidRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusBidmachineExternalBidRewardedFactory {
        return AequusBidmachineExternalBidRewardedFactory()
    }
}
