//
//  AequusBidMachineRewarded.swift
//  AequusBidMachineAdapter
//
//  Created by Aleksandr Poliakov on 28.02.2022.
//
import BidMachine
import AequusCore

final class AequusBidmachineRewarded: NSObject, RewardedInterstitial, ExternalBidAdUnit, Impressionable {
    
    var adUnit: String = AequusBidmachineInitializer.rewardedUnitId ?? ""
    weak var delegate: RewardedInterstitialDelegate?
    
	var completion: ((Bool) -> ())?
    
    var isReady: Bool {
        return rewarded != nil && rewarded?.canShow == true
    }
    
    var sdkVersion: String {
        kBDMVersion
    }
    
	var price: Int64 = -1
	
    private var rewarded: BDMRewarded?
    private var request: BDMRewardedRequest?
	private var bidCompletionCalled: Bool = false
    
    init(delegate: RewardedInterstitialDelegate?) {
        self.delegate = delegate
            
        super.init()
    }
    
    func load() {
		guard let request = request else {
			delegate?.didFailToLoad(rewarded: self, error: AequusBidmachineError.cantGetBid)
			return
		}
		
		DispatchQueue.main.async {
			self.rewarded = BDMRewarded()
			self.rewarded?.delegate = self
			self.rewarded?.producerDelegate = self
			self.rewarded?.populate(with: request)
		}
    }
    
	func requestBid(completion: @escaping (Bool) -> ()) {
        self.completion = completion
		self.request = BDMRewardedRequest()
        self.request?.placementId = adUnit
		self.request?.perform(with: self)
    }
    
    func show(from viewController: UIViewController) {
        guard let rewarded = self.rewarded, rewarded.canShow else {
            delegate?.didFailToShow(rewarded: self, error: AequusBidmachineError.adNotLoaded)
            return
        }
        
        DispatchQueue.main.async {
            rewarded.present(fromRootViewController: viewController)
        }
    }
    
    
    func destroy() {
        DispatchQueue.main.async {
			self.rewarded?.delegate = nil
            self.rewarded = nil
			self.delegate = nil
			self.completion = nil
        }
    }
}

extension AequusBidmachineRewarded: BDMAdEventProducerDelegate {
	func didProduceUserAction(_ producer: BDMAdEventProducer) {}

	func didProduceImpression(_ producer: BDMAdEventProducer) {
		delegate?.impression(rewarded: self)
	}
}

// MARK: - BDMRequestDelegate

extension AequusBidmachineRewarded: BDMRequestDelegate {
    func requestDidExpire(_ request: BDMRequest) {
        
    }
    
    func request(_ request: BDMRequest, failedWithError error: Error) {
		if !bidCompletionCalled {
			completion?(false)
			bidCompletionCalled.toggle()
		}
    }
    
    func request(_ request: BDMRequest, completeWith info: BDMAuctionInfo) {
		if !bidCompletionCalled {
			self.price = DollarToMicrodollarConverter.convert(dollar: Double(truncating: info.price ?? 0))
			completion?(true)
			bidCompletionCalled.toggle()
		}
    }
}

// MARK: - BDMRewardedDelegate

extension AequusBidmachineRewarded: BDMRewardedDelegate {
    func rewardedReady(toPresent rewarded: BDMRewarded) {
        delegate?.didLoad(rewarded: self)
    }
    
    func rewardedWillPresent(_ rewarded: BDMRewarded) {
        delegate?.didShow(rewarded: self)
    }
    
    func rewardedDidDismiss(_ rewarded: BDMRewarded) {
        delegate?.didClose(rewarded: self)
    }
    
    func rewardedRecieveUserInteraction(_ rewarded: BDMRewarded) {
        delegate?.click(rewarded: self)
    }
    
    func rewardedFinishRewardAction(_ rewarded: BDMRewarded) {
        delegate?.eligibleForReward(rewarded: self)
    }
    
    func rewarded(_ rewarded: BDMRewarded, failedToPresentWithError error: Error) {
        delegate?.didFailToShow(rewarded: self, error: error)
    }
    
    func rewarded(_ rewarded: BDMRewarded, failedWithError error: Error) {
        delegate?.didFailToLoad(rewarded: self, error: error)
    }

}
