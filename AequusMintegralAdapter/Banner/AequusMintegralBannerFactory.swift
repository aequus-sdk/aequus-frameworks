//
//  AequusMintegralBannerFactory.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore
import MTGSDK

public final class AequusMintegralBannerFactory: BannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusMintegralBanner(placementId: adUnitConfig.placementId ?? "", unitId: id, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusMintegralBannerFactory {
        return AequusMintegralBannerFactory()
    }
}
