//
//  AequusMintegralBanner.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore
import MTGSDK
import MTGSDKBanner

class AequusMintegralBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
        return MTGSDK.sdkVersion()
    }
    var isReady: Bool = false

    var bannerView: UIView? {
        return banner
    }
    
    private var insideView: UIView?
    private let viewController: UIViewController
    private var banner: MTGBannerAdView?
    private let type: AequusBannerType
    private let placementId: String
    private let unitId: String
	var timeout: Bool = false
    
    init(placementId: String, unitId: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.placementId = placementId
        self.type = type
        self.unitId = unitId
        self.delegate = delegate
    }
    
    func load() {
        banner = MTGBannerAdView.init(
            bannerAdViewWith: configureSize(type: type),
            placementId: placementId,
            unitId: unitId,
            rootViewController: viewController)
        
        //??????
        let size = type.size
        
        self.banner?.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
		self.banner?.autoRefreshTime = 0
        self.banner?.showCloseButton = .no
        self.banner?.delegate = self
        self.banner?.loadBannerAd()
    }
    
    func destroy() {
        self.banner?.destroy()
        self.banner?.removeFromSuperview()
        self.banner = nil
    }
    
    private func configureSize(type: AequusBannerType) -> MTGBannerSizeType {
        var size = MTGBannerSizeType.smartBannerType
        
        switch type {
        case .mrec:
            size = .mediumRectangularBanner300x250
        case .w320h50:
            size = .smartBannerType
        }
        
        return size
    }
}

extension AequusMintegralBanner: MTGBannerAdViewDelegate {
    
    func adViewLoadSuccess(_ adView: MTGBannerAdView!) {
        delegate?.didLoad(banner: self)
    }
    
    func adViewLoadFailedWithError(_ error: Error!, adView: MTGBannerAdView!) {
        delegate?.failToLoad(banner: self, error: error)
    }
    
    func adViewWillLogImpression(_ adView: MTGBannerAdView!) {
        delegate?.didShow(banner: self)
        delegate?.impression(banner: self)
    }
    
    func adViewDidClicked(_ adView: MTGBannerAdView!) {
        delegate?.click(banner: self)
    }
    
    func adViewWillLeaveApplication(_ adView: MTGBannerAdView!) {
    }
    
    func adViewWillOpenFullScreen(_ adView: MTGBannerAdView!) {
    }
    
    func adViewCloseFullScreen(_ adView: MTGBannerAdView!) {
    }
    
    func adViewClosed(_ adView: MTGBannerAdView!) {
    }
    
}
