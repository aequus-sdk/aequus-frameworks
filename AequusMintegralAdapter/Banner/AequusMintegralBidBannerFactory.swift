//
//  AequusMintegralBidBannerFactory.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore
import MTGSDK

public final class AequusMintegralBidBannerFactory: BidBannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: BannerDelegate) -> Banner? {
        return AequusMintegralBidBanner(placementId: params?["placementId"] ?? "", unitId: adId, type: type, viewController: viewController, token: adm, delegate: delegate)
    }
    
    public static func createInstance() -> AequusMintegralBidBannerFactory {
        return AequusMintegralBidBannerFactory()
    }
}
