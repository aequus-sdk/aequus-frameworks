//
//  AequusMintegralBidBanner.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 12.05.2021.
//

import AequusCore
import MTGSDK
import MTGSDKBanner
import MTGSDKBidding

class AequusMintegralBidBanner: NSObject, Banner, StatusCheck, Impressionable {
    
    weak var delegate: BannerDelegate?
    
    var isReady: Bool = false

    var bannerView: UIView? {
        return banner
    }
    
    var sdkVersion: String {
        return MTGSDK.sdkVersion()
    }
    
    private var insideView: UIView?
    private let viewController: UIViewController
    private var banner: MTGBannerAdView?
    private let placementId: String
    private let unitId: String
    private let token: String
    private let type: AequusBannerType
	var timeout: Bool = false
    
    init(placementId: String, unitId: String, type: AequusBannerType, viewController: UIViewController, token: String, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.placementId = placementId
        self.unitId = unitId
        self.type = type
        self.token = token
        self.delegate = delegate
    }
    
    func load() {
        
        self.banner = MTGBannerAdView.init(
            bannerAdViewWith: type == .w320h50 ? .standardBannerType320x50 : .mediumRectangularBanner300x250,
                            placementId: placementId,
                            unitId: unitId,
                            rootViewController: viewController)
        
        //??????
        let size = type.size
        
        self.banner?.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        self.banner?.showCloseButton = .no
        self.banner?.delegate = self
		self.banner?.autoRefreshTime = 0
        
        self.banner?.loadBannerAd(withBidToken: self.token)
    }
    
    func destroy() {
        self.banner?.destroy()
        self.banner?.removeFromSuperview()
        self.banner = nil
    }
}

extension AequusMintegralBidBanner: MTGBannerAdViewDelegate {
    
    func adViewLoadSuccess(_ adView: MTGBannerAdView!) {
        delegate?.didLoad(banner: self)
    }
    
    func adViewLoadFailedWithError(_ error: Error!, adView: MTGBannerAdView!) {
        delegate?.failToLoad(banner: self, error: error)
    }
    
    func adViewWillLogImpression(_ adView: MTGBannerAdView!) {
        delegate?.didShow(banner: self)
        delegate?.impression(banner: self)
    }
    
    func adViewDidClicked(_ adView: MTGBannerAdView!) {
        delegate?.click(banner: self)
    }
    
    func adViewWillLeaveApplication(_ adView: MTGBannerAdView!) {
    }
    
    func adViewWillOpenFullScreen(_ adView: MTGBannerAdView!) {
    }
    
    func adViewCloseFullScreen(_ adView: MTGBannerAdView!) {
    }
    
    func adViewClosed(_ adView: MTGBannerAdView!) {
    }
    
}
