//
//  AequusMintegralBidRewardedFactory.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDK

public final class AequusMintegralBidRewardedFactory: BidRewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusMintegralBidRewarded(placementId: params?["placementId"] ?? "", unitId: adId, token: adm)
    }
}

extension AequusMintegralBidRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusMintegralBidRewardedFactory {
        return AequusMintegralBidRewardedFactory()
    }
}
