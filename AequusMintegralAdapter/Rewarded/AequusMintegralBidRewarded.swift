//
//  AequusMintegralBidRewarded.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDKReward
import MTGSDKBidding

final class AequusMintegralBidRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
	
    var isReady: Bool {
        return bidAdManager?
            .isVideoReadyToPlay(
                withPlacementId: placementId,
                unitId: unitId) ?? false
    }
    
    private let placementId: String
    private let unitId: String
    private let token: String
    private var bidAdManager: MTGBidRewardAdManager?
    
    var sdkVersion: String {
        return MTGSDK.sdkVersion()
    }
    
    init(placementId: String, unitId: String, token: String) {
        self.unitId = unitId
        self.placementId = placementId
        self.token = token
            
        super.init()
        
        bidAdManager = MTGBidRewardAdManager()
    }
    
    func load() {
        bidAdManager?.loadVideo(withBidToken: token, placementId: placementId, unitId: unitId, delegate: self)
    }
    
    func show(from viewController: UIViewController) {
        bidAdManager?.showVideo(withPlacementId: placementId, unitId: unitId, withRewardId: "", userId: "", delegate: self, viewController: viewController)
    }
    
    func destroy() {
        bidAdManager?.cleanAllVideoFileCache()
    }
}

extension AequusMintegralBidRewarded: MTGRewardAdLoadDelegate {
    
    func onAdLoadSuccess(_ placementId: String?, unitId: String?) {
        delegate?.didLoad(rewarded: self)
    }
    
    func onVideoAdLoadFailed(_ placementId: String?, unitId: String?, error: Error) {
        delegate?.didFailToLoad(rewarded: self, error: error)
    }
    
}

extension AequusMintegralBidRewarded: MTGRewardAdShowDelegate {
    
    func onVideoPlayCompleted(_ placementId: String?, unitId: String?) {
        delegate?.rewardedVideoDidComplete(rewarded: self)
        delegate?.eligibleForReward(rewarded: self)
    }
    
    func onVideoAdClicked(_ placementId: String?, unitId: String?) {
        delegate?.click(rewarded: self)
    }
    
    func onVideoAdShowSuccess(_ placementId: String?, unitId: String?, bidToken: String?) {
        delegate?.didShow(rewarded: self)
        delegate?.impression(rewarded: self)
    }
    
    func onVideoAdDidClosed(_ placementId: String?, unitId: String?) {
        delegate?.didClose(rewarded: self)
    }
    
    func onVideoAdShowFailed(_ placementId: String?, unitId: String?, withError error: Error) {
        delegate?.didFailToShow(rewarded: self, error: error)
    }
}
