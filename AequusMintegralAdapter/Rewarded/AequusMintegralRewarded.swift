//
//  AequusMintegralRewarded.swift
//  AequusAdcolonyAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDK
import MTGSDKReward

final class AequusMintegralRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool {
        waterfallAdManager?.isVideoReadyToPlay(withPlacementId: placementId, unitId: unitId) ?? false
    }
    
    var sdkVersion: String {
        return MTGSDK.sdkVersion()
    }
	
	var isAdLoadOperationAvailable: Bool {
		!(waterfallAdManager?
			.isVideoReadyToPlay(
				withPlacementId: placementId,
				unitId: unitId) ?? false)
	}
    
    private var waterfallAdManager: MTGRewardAdManager?
    
    private let placementId: String
    private let unitId: String
    
    init(placementId: String, unitId: String, delegate: RewardedInterstitialDelegate?) {
 
        self.delegate = delegate
        self.unitId = unitId
        self.placementId = placementId
            
        super.init()
        
        waterfallAdManager = MTGRewardAdManager()
    }
    
    func load() {
        waterfallAdManager?.loadVideo(withPlacementId: self.placementId, unitId: self.unitId, delegate: self)
    }
    
    func show(from viewController: UIViewController) {
        
        waterfallAdManager?.showVideo(withPlacementId: self.placementId, unitId: self.unitId, withRewardId: "", userId: "", delegate: self, viewController: viewController)
    }
    
    
    func destroy() {
        waterfallAdManager?.cleanAllVideoFileCache()
    }
}

extension AequusMintegralRewarded: MTGRewardAdLoadDelegate {
    
    func onAdLoadSuccess(_ placementId: String?, unitId: String?) {
        delegate?.didLoad(rewarded: self)
    }
    
    func onVideoAdLoadFailed(_ placementId: String?, unitId: String?, error: Error) {
        delegate?.didFailToLoad(rewarded: self, error: error)
    }
}

extension AequusMintegralRewarded: MTGRewardAdShowDelegate {
    
    func onVideoPlayCompleted(_ placementId: String?, unitId: String?) {
        delegate?.rewardedVideoDidComplete(rewarded: self)
        delegate?.eligibleForReward(rewarded: self)
    }
    
    func onVideoAdClicked(_ placementId: String?, unitId: String?) {
        delegate?.click(rewarded: self)
    }
    
	func onVideoAdShowSuccess(_ placementId: String?, unitId: String?) {
        delegate?.didShow(rewarded: self)
        delegate?.impression(rewarded: self)
    }
    
    func onVideoAdDidClosed(_ placementId: String?, unitId: String?) {
        delegate?.didClose(rewarded: self)
    }
    
    func onVideoAdShowFailed(_ placementId: String?, unitId: String?, withError error: Error) {
        delegate?.didFailToShow(rewarded: self, error: error)
    }
}


