//
//  AequusMintegralRewardedFactory.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore
import MTGSDK

public final class AequusMintegralRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusMintegralRewarded(placementId: adUnitConfig.placementId!, unitId: id, delegate: delegate)
    }
    
}

extension AequusMintegralRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusMintegralRewardedFactory {
        return AequusMintegralRewardedFactory()
    }
}
