//
//  AequusMintegralInterstitialFactory.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDK

public final class AequusMintegralInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusMintegralInterstitial(placementId: adUnitConfig.placementId!, unitId: id, delegate: delegate)
    }
    
}

extension AequusMintegralInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusMintegralInterstitialFactory {
        return AequusMintegralInterstitialFactory()
    }
}
