//
//  AequusMintegralInterstitial.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDKInterstitialVideo

final class AequusMintegralInterstitial: NSObject, Interstitial {
    
    weak var delegate: InterstitialDelegate?
	
    var isReady: Bool {
        waterfallAdManager?
            .isVideoReadyToPlay(
                withPlacementId: placementId,
                unitId: unitId) ?? false
    }
    
    var sdkVersion: String {
        return MTGSDK.sdkVersion()
    }
	
	var isAdLoadOperationAvailable: Bool {
		!(waterfallAdManager?
			.isVideoReadyToPlay(
				withPlacementId: placementId,
				unitId: unitId) ?? false)
	}
    
    private var waterfallAdManager: MTGInterstitialVideoAdManager?
    
    private let placementId: String
    private let unitId: String
    
    init(placementId: String, unitId: String, delegate: InterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
        self.placementId = placementId
            
        super.init()
        
        waterfallAdManager = MTGInterstitialVideoAdManager(placementId: placementId, unitId: unitId, delegate: self)
    }
    
    func load() {
        waterfallAdManager?.loadAd()
    }
    
    func show(from viewController: UIViewController) {
        waterfallAdManager?.show(from: viewController)
    }
    
    func destroy() {
        waterfallAdManager?.cleanAllVideoFileCache()
    }
    
}

extension AequusMintegralInterstitial: MTGInterstitialVideoDelegate {
        
    func onInterstitialAdLoadSuccess(_ adManager: MTGInterstitialVideoAdManager) {
        delegate?.didLoad(interstitial: self)
    }
    
    func onInterstitialVideoLoadFail(_ error: Error, adManager: MTGInterstitialVideoAdManager) {
        delegate?.didFailToLoad(interstitial: self, error: error)
    }
    
    func onInterstitialVideoAdClick(_ adManager: MTGInterstitialVideoAdManager) {
        delegate?.click(interstitial: self)
    }
    
    func onInterstitialVideoShowSuccess(_ adManager: MTGInterstitialVideoAdManager) {
        delegate?.didShow(interstitial: self)
        delegate?.impression(interstitial: self)
    }
    
    func onInterstitialVideoShowFail(_ error: Error, adManager: MTGInterstitialVideoAdManager) {
        delegate?.didFailToShow(interstitial: self, error: error)
    }
    
    func onInterstitialVideoAdDidClosed(_ adManager: MTGInterstitialVideoAdManager) {
        delegate?.didClose(interstitial: self)
    }
    
    
}
