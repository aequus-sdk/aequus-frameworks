//
//  AequusMintegralBidInterstitialFactory.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDK

public final class AequusMintegralBidInterstitialFactory: BidInterstitialFactory {
    
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusMintegralBidInterstitial(placementId: params?["placementId"] ?? "", unitId: adId, token: adm)
    }
}

extension AequusMintegralBidInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusMintegralBidInterstitialFactory {
        return AequusMintegralBidInterstitialFactory()
    }
}
