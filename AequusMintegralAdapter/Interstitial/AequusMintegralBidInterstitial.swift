//
//  AequusMintegralBidInterstitial.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDKInterstitialVideo

final class AequusMintegralBidInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
    
    var isReady: Bool {
        return bidAdManager?
            .isVideoReadyToPlay(
                withPlacementId: placementId,
                unitId: unitId) ?? false
    }
    
    var sdkVersion: String {
        return MTGSDK.sdkVersion()
    }
    
    private let placementId: String
    private let unitId: String
    private let token: String
    private var bidAdManager: MTGBidInterstitialVideoAdManager?
    
    init(placementId: String, unitId: String, token: String) {
        self.unitId = unitId
        self.placementId = placementId
        self.token = token
            
        super.init()
        
        bidAdManager = MTGBidInterstitialVideoAdManager(placementId: placementId, unitId: unitId, delegate: self)
    }
    
    func load() {
        bidAdManager?.loadAd(withBidToken: self.token)
    }
    
    func show(from viewController: UIViewController) {
        bidAdManager?.show(from: viewController)
    }
    
    func destroy() {
        bidAdManager?.cleanAllVideoFileCache()
    }
}

extension AequusMintegralBidInterstitial: MTGBidInterstitialVideoDelegate {
    
    func onInterstitialAdLoadSuccess(_ adManager: MTGBidInterstitialVideoAdManager) {
        delegate?.didLoad(interstitial: self)
    }
    
    func onInterstitialVideoLoadFail(_ error: Error, adManager: MTGBidInterstitialVideoAdManager) {
        delegate?.didFailToLoad(interstitial: self, error: error)
    }
    
    func onInterstitialVideoAdClick(_ adManager: MTGBidInterstitialVideoAdManager) {
        delegate?.click(interstitial: self)
    }
    
    func onInterstitialVideoShowSuccess(_ adManager: MTGBidInterstitialVideoAdManager, bidToken: String?) {
        delegate?.didShow(interstitial: self)
        delegate?.impression(interstitial: self)
    }
    
    func onInterstitialVideoShowFail(_ error: Error, adManager: MTGBidInterstitialVideoAdManager) {
        delegate?.didFailToShow(interstitial: self, error: error)
    }
    
    func onInterstitialVideoAdDidClosed(_ adManager: MTGBidInterstitialVideoAdManager) {
        delegate?.didClose(interstitial: self)
    }
    
}
