//
//  AequusMintegralInitializer.swift
//  AequusMintegralAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDK
import Combine

public enum AequusMintegralInitializingError : Error {
    case some
}

public final class AequusMintegralInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        if !AequusMintegralInitializer.isInitialized {
            
            cancelable = privacyPublisher.sink { privacy in
                MTGSDK.sharedInstance().consentStatus = privacy.GDPRConsent
                MTGSDK.sharedInstance().doNotTrackStatus = privacy.CCPADoNotSell
            }
            
            if let config = config,
                let appId = config.appID,
                let appKey = config.appKey {
                
                MTGSDK.sharedInstance().setAppID(appId, apiKey: appKey)
                AequusMintegralInitializer.isInitialized = true
                completion(true, nil)
                
            } else {
                completion(false, AequusMintegralInitializingError.some)
            }
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusMintegralInitializer: Initializable {
    public static func createInstance() -> AequusMintegralInitializer {
        return AequusMintegralInitializer()
    }
}
