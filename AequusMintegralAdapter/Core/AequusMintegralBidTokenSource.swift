//
//  AequusMintegralBidTokenSource.swift
//  AequusCore
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MTGSDK
import MTGSDKBidding

final class AequusMintegralBidTokenSource: BidTokenSource  {
    
    func invoke(completion: @escaping (String?) -> Void) {
        
        completion(MTGBiddingSDK.buyerUID())
    }
    
}

extension AequusMintegralBidTokenSource: Initializable {
    static func createInstance() -> AequusMintegralBidTokenSource {
        AequusMintegralBidTokenSource()
    }
}
