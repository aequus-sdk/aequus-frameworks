## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

aequus-specs is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AequusCore'
```

## Integration

Integrate Network SDK’s you want you use in your project, using CocoaPods.
You need to add every necessary Ad Network Adapter for each Ad Network you want use inside your app.
After using ‘pod install’ for updated lists of Pods inside your app, implement usage of necessary ad types.

## Author

Aequus Inc.

## License

aequus-specs is available under the MIT license. See the LICENSE file for more info.
