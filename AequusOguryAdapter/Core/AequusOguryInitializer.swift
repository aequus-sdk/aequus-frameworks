//
//  AequusOguryInitializer.swift
//  AequusOguryAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore
import Combine
import OgurySdk

public final class AequusOguryInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        if !AequusOguryInitializer.isInitialized {
            cancelable = privacyPublisher.sink { privacy in
				//TODO:
            }
			
			if let assetKey = config?.assetKey {
				let configuration = OguryConfigurationBuilder(assetKey: assetKey).build()

				Ogury.start(with: configuration)
				completion(true, nil)
			} else {
				completion(false, nil)
			}
           
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusOguryInitializer: Initializable {
    public static func createInstance() -> AequusOguryInitializer {
        return AequusOguryInitializer()
    }
}
