//
//  AequusOguryRewardedFactory.swift
//  AequusOguryAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore

public final class AequusOguryRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusOguryRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusOguryRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusOguryRewardedFactory {
        return AequusOguryRewardedFactory()
    }
}
