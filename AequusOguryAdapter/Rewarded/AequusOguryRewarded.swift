//
//  AequusOguryRewarded.swift
//  AequusOguryAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//
import Foundation
import AequusCore
import OguryAds
import OgurySdk

final class AequusOguryRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool {
		return rewarded?.isLoaded() ?? false
    }
    
    var sdkVersion: String {
		Ogury.getSdkVersion()
    }
    
	private var rewarded: OguryOptinVideoAd?
    private let unitId: String
    
    init(unitId: String, delegate: RewardedInterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
            
        super.init()
		
		self.rewarded = OguryOptinVideoAd(adUnitId: unitId)
		self.rewarded?.delegate = self
    }
    
    func load() {
		self.rewarded?.load()
    }
    
    func show(from viewController: UIViewController) {
		self.rewarded?.show(in: viewController)
    }
    
    
    func destroy() {
		self.rewarded?.delegate = nil
    }
}

extension AequusOguryRewarded: OguryOptinVideoAdDelegate {
	
	func didLoad(_ optinVideo: OguryOptinVideoAd) {
		delegate?.didLoad(rewarded: self)
	}
	
	func didFailOguryOptinVideoAdWithError(_ error: OguryError, for optinVideo: OguryOptinVideoAd) {
		//	https://ogury-ltd.gitbook.io/ios/ad-formats/opt-in-video-ad#error-codes
		switch error.code {
		case 0,
			2001,
			2002,
			2004,
			2006,
			2008:
			
			delegate?.didFailToLoad(rewarded: self, error: error)
			
		case 2003,
			2005,
			2010:
			delegate?.didFailToShow(rewarded: self, error: error)
			
		default:
			delegate?.didFailToLoad(rewarded: self, error: error)
			break
		}
	}
	
	func didClick(_ optinVideo: OguryOptinVideoAd) {
		delegate?.click(rewarded: self)
	}
	
	func didDisplay(_ optinVideo: OguryOptinVideoAd) {
		delegate?.didShow(rewarded: self)
	}
	
	func didTriggerImpressionOguryOptinVideoAd(_ optinVideo: OguryOptinVideoAd) {
		delegate?.impression(rewarded: self)
	}
	
	func didClose(_ optinVideo: OguryOptinVideoAd) {
		delegate?.didClose(rewarded: self)
	}
	
	func didRewardOguryOptinVideoAd(with item: OGARewardItem, for optinVideo: OguryOptinVideoAd) {
		delegate?.eligibleForReward(rewarded: self)
	}
	
}
