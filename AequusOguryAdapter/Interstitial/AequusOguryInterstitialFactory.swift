//
//  AequusOguryInterstitialFactory.swift
//  AequusOguryAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore

public final class AequusOguryInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusOguryInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusOguryInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusOguryInterstitialFactory {
        return AequusOguryInterstitialFactory()
    }
}
