//
//  AequusOguryInterstitial.swift
//  AequusOguryAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore
import OguryAds
import OgurySdk

enum AequusOguryError: Int, Error {
	case loadFailed = 0
	case noInternetConnection
	case adDisable
	case profigNotSynced
	case adExpired
	case sdkInitNotCalled
	case anotherAdAlreadyDisplayed
	case cantShowAdsInPresentingViewController
	case unknown
}

final class AequusOguryInterstitial: NSObject, Interstitial, StatusCheck {
	
	weak var delegate: InterstitialDelegate?
	
	var isReady: Bool {
		interstitial?.isLoaded() ?? false
	}
	
	var sdkVersion: String {
		Ogury.getSdkVersion()
	}
	
	private var interstitial: OguryInterstitialAd?
	private let unitId: String
	
	init(unitId: String, delegate: InterstitialDelegate?) {
		self.delegate = delegate
		self.unitId = unitId
		
		super.init()
		
		self.interstitial = OguryInterstitialAd(adUnitId: unitId)
		self.interstitial?.delegate = self
	}
	
	func load() {
		interstitial?.load()
	}
	
	func show(from viewController: UIViewController) {
		interstitial?.show(in: viewController)
	}
	
	func destroy() {
		self.interstitial?.delegate = nil
	}
	
}

extension AequusOguryInterstitial: OguryInterstitialAdDelegate {
	
	func didLoad(_ interstitial: OguryInterstitialAd) {
		delegate?.didLoad(interstitial: self)
	}
	
	func didFailOguryInterstitialAdWithError(_ error: OguryError, for interstitial: OguryInterstitialAd) {
		//	https://ogury-ltd.gitbook.io/ios/ad-formats/opt-in-video-ad#error-codes
		switch error.code {
		case 0,
			2001,
			2002,
			2004,
			2006,
			2008:
			
			delegate?.didFailToLoad(interstitial: self, error: error)
			
		case 2003,
			2005,
			2010:
			delegate?.didFailToShow(interstitial: self, error: error)
			
		default:
			delegate?.didFailToLoad(interstitial: self, error: error)
			break
		}
	}
	
	func didDisplay(_ interstitial: OguryInterstitialAd) {
		delegate?.didShow(interstitial: self)
	}
	
	func didClick(_ interstitial: OguryInterstitialAd) {
		delegate?.click(interstitial: self)
	}
	
	func didTriggerImpressionOguryInterstitialAd(_ interstitial: OguryInterstitialAd) {
		delegate?.impression(interstitial: self)
	}
	
	func didClose(_ interstitial: OguryInterstitialAd) {
		delegate?.didClose(interstitial: self)
	}
	
}
