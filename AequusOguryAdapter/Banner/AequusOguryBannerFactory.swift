//
//  AequusOguryBannerFactory.swift
//  AequusOguryAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore

public final class AequusOguryBannerFactory: BannerFactory {
	public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
		return AequusOguryBanner(unitId: id, type: type, viewController: viewController, delegate: delegate)
	}

    public static func createInstance() -> AequusOguryBannerFactory {
        return AequusOguryBannerFactory()
    }
}
