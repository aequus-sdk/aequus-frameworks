//
//  AequusOguryBanner.swift
//  AequusOguryAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import AequusCore
import OgurySdk
import OguryAds

final class AequusOguryBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
		Ogury.getSdkVersion()
    }
    var isReady: Bool = false
	
    var bannerView: UIView? {
        return banner
    }
	
    private let viewController: UIViewController
    private var banner: OguryBannerAd?
    private let unitId: String
	private let type: AequusBannerType
	var timeout: Bool = false
    
	init(unitId: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
		self.type = type
		self.banner = OguryBannerAd(adUnitId: unitId)
		
		super.init()
		
		self.banner?.delegate = self
    }
    
    func load() {
        banner?.load(with: configureSize(type: type))
    }
    
    func destroy() {
        self.banner?.removeFromSuperview()
		self.banner?.delegate = nil
        self.banner = nil
    }
    
    private func configureSize(type: AequusBannerType) -> OguryAdsBannerSize {
        var size = OguryAdsBannerSize.small_banner_320x50()
        
        switch type {
        case .mrec:
            size = .mpu_300x250()
        case .w320h50:
            size = .small_banner_320x50()
        }
        
        return size
    }
}

extension AequusOguryBanner: OguryBannerAdDelegate {
	
	func didLoad(_ banner: OguryBannerAd) {
		isReady = true
		delegate?.didLoad(banner: self)
	}
    
	func didFailOguryBannerAdWithError(_ error: OguryError, for banner: OguryBannerAd) {
		delegate?.failToLoad(banner: self, error: error)
	}
	
	func didTriggerImpressionOguryBannerAd(_ banner: OguryBannerAd) {
		delegate?.impression(banner: self)
	}

	func didClick(_ banner: OguryBannerAd) {
		delegate?.click(banner: self)
	}
	
	func didDisplay(_ banner: OguryBannerAd) {
		delegate?.didShow(banner: self)
	}
    
}
