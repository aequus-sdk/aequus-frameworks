//
//  AequusAmazonInitializer.swift
//  AequusAmazonAdapter
//
//  Created by Aleksandr on 20.05.2022.
//

import AequusCore
import Combine
import AppLovinSDK
import DTBiOSSDK

public final class AequusAmazonInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        if !AequusAmazonInitializer.isInitialized {
            AequusAmazonInitializer.isInitialized = true
            completion(true, nil)
        } else {
            AequusAmazonInitializer.isInitialized = false
            completion(false, nil)
        }
        
    }
}

extension AequusAmazonInitializer: Initializable {
    public static func createInstance() -> AequusAmazonInitializer {
        return AequusAmazonInitializer()
    }
}
