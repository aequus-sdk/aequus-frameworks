//
//  AequusAmazonExternalParameterHandler.swift
//  AequusAmazonAdapter
//
//  Created by Aleksandr on 16.06.2022.
//

import AequusCore
import DTBiOSSDK

public final class AequusAmazonBidExternalParameterHandler: BidExternalParameterHandler {
    
    private var lastProducedData: (bidId: String, extraParameters: BidExternalParameterProducedData)? = nil

    public func consume(externalParameter: Any?) {
        let lastBid = lastProducedData?.bidId
        
        let apsBid = externalParameter as? DTBAdResponse
        
        if apsBid != nil && apsBid?.bidId == lastBid {
            lastProducedData = nil
            return
        }
        
        popLastBidDataAndCancelTimeoutJob()
        
        guard let apsBid = apsBid else { return }
        
        parseBidAndStartTimeoutJob(apsBid: apsBid)
    }
    
    private var apsBidTimeoutJob: Timer? = nil
    
    private func parseBidAndStartTimeoutJob(apsBid: DTBAdResponse) {
        lastProducedData = (bidId: apsBid.bidId,
                            extraParameters: BidExternalParameterProducedData(hashableParams: apsBid.mediationHints(),
                                                                              bidParameters: ["slotUuid": apsBid.adSize().slotUUID ?? "",
                                                                                              "encodedPrice": apsBid.amznSlots()]))
        apsBidTimeoutJob?.invalidate()
        //10 minutes delay
        let delay = TimeInterval(10 * 60 * 1000)
        apsBidTimeoutJob = Timer(timeInterval: delay, repeats: true) { [weak self] _ in
            self?.popLastBidDataAndCancelTimeoutJob()
        }
    }
    
    private func popLastBidDataAndCancelTimeoutJob()  {
        apsBidTimeoutJob?.invalidate()
        lastProducedData = nil
    }
    
    public func produce() -> BidExternalParameterProducedData? {
        return lastProducedData?.extraParameters ?? nil
    }
    
    public func destroy() {
        apsBidTimeoutJob = nil
        apsBidTimeoutJob?.invalidate()
    }
}

// MARK: - BidExternalParameterHandlerFactory

extension AequusAmazonBidExternalParameterHandler: Initializable {
    public static func createInstance() -> AequusAmazonBidExternalParameterHandler {
        return AequusAmazonBidExternalParameterHandler()
    }
}
