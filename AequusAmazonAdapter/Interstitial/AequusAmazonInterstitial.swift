//
//  AequusAmazonInterstitial.swift
//  AequusAmazonAdapter
//
//  Created by Aleksandr on 20.05.2022.
//

import AequusCore
import AppLovinSDK
import DTBiOSSDK

enum AequusAmazonInterstitialError: Error {
    case didFailToLoad
    case didFailToShow
}
 
final class AequusAmazonInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
    
    var isReady: Bool {
        return interstitial != nil
    }
    
    var sdkVersion: String {
        DTBAds.version()
    }
    
    private var isFirstLoad = true
    private var interstitial: DTBAdInterstitialDispatcher?
    private let adm: String
    private let hashableParams: [AnyHashable: Any]
    
    init(adm: String, hashableParams: [AnyHashable: Any], delegate: InterstitialDelegate?) {
        self.delegate = delegate
        self.adm = adm
        self.hashableParams = hashableParams
        
        super.init()
        
        interstitial = DTBAdInterstitialDispatcher(delegate: self)
    }
    
    func load() {
        DispatchQueue.main.async {
            if !self.hashableParams.isEmpty {
                self.interstitial?.fetchAd(withParameters: self.hashableParams)
            }
        }
    }
    
    func show(from viewController: UIViewController) {
        DispatchQueue.main.async {
            self.interstitial?.show(from: viewController)
            self.delegate?.didShow(interstitial: self)
        }
    }
    
    func destroy() {
        DispatchQueue.main.async {
            self.interstitial = nil
            self.interstitial?.delegate = nil
        }
    }
    
}

extension AequusAmazonInterstitial: DTBAdInterstitialDispatcherDelegate {
    func show(fromRootViewController controller: UIViewController) { }

    func interstitial(_ interstitial: DTBAdInterstitialDispatcher?, didFailToLoadAdWith errorCode: DTBAdErrorCode) {
        delegate?.didFailToLoad(interstitial: self, error: AequusAmazonInterstitialError.didFailToLoad)
    }
    
    func interstitialDidLoad(_ interstitial: DTBAdInterstitialDispatcher?) {
        delegate?.didLoad(interstitial: self)
    }

    
    func interstitialWillDismissScreen(_ interstitial: DTBAdInterstitialDispatcher?) {
        //delegate?.didClose(interstitial: self)
    }
    
    func impressionFired() {
        delegate?.impression(interstitial: self)
    }
    
    func interstitialWillPresentScreen(_ interstitial: DTBAdInterstitialDispatcher?) {
        
    }
    
    func interstitialDidPresentScreen(_ interstitial: DTBAdInterstitialDispatcher?) {
        delegate?.didShow(interstitial: self)
    }
    
    func interstitialDidDismissScreen(_ interstitial: DTBAdInterstitialDispatcher?) {
        delegate?.didClose(interstitial: self)
    }
    
    func interstitialWillLeaveApplication(_ interstitial: DTBAdInterstitialDispatcher?) {
        
    }
}
