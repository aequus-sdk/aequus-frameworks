//
//  AequusAmazonBidInterstitialFactory.swift
//  AequusAmazonAdapter
//
//  Created by Aleksandr on 20.05.2022.
//

import AequusCore

public final class AequusAmazonBidInterstitialFactory: BidInterstitialFactory {
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: InterstitialDelegate) -> Interstitial? {
        return AequusAmazonInterstitial(adm: adm, hashableParams: hashableParams ?? [:], delegate: delegate)
    }
}

extension AequusAmazonBidInterstitialFactory: Initializable {
    public static func createInstance() -> AequusAmazonBidInterstitialFactory {
        return AequusAmazonBidInterstitialFactory()
    }
}
