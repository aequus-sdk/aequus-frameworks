//
//  AequusAmazonBidBannerFactory.swift
//  AequusAmazonAdapter
//
//  Created by Aleksandr on 20.05.2022.
//


import AequusCore

public final class AequusAmazonBidBannerFactory: BidBannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: BannerDelegate) -> Banner? {
        AequusAmazonBanner(hashableParams: hashableParams ?? [:], type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusAmazonBidBannerFactory {
        return AequusAmazonBidBannerFactory()
    }
}
