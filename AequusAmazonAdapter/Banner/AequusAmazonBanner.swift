//
//  AequusAmazonBanner.swift
//  AequusAmazonAdapter
//
//  Created by Aleksandr on 20.05.2022.
//

import AequusCore
import AppLovinSDK
import DTBiOSSDK

enum AequusAmazonBannerError: Error {
    case failToLoad
}

final class AequusAmazonBanner: NSObject, Banner, StatusCheck, Impressionable {
    var timeout: Bool = false
    
    weak var delegate: BannerDelegate?
    
    var sdkVersion: String {
        DTBAds.version()
    }
    var isReady: Bool = false
    
    var bannerView: UIView? {
        return banner?.adView
    }
    
    private let viewController: UIViewController
    private let type: AequusBannerType
    private var banner: DTBAdBannerDispatcher?
    private let hashableParams: [AnyHashable: Any]
    
    init(hashableParams: [AnyHashable: Any], type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.delegate = delegate
        self.type = type
        self.hashableParams = hashableParams
        super.init()
        
        let rect = CGRect(x: .zero, y: .zero, width: type.size.width, height: type.size.height)
        banner = DTBAdBannerDispatcher(adFrame: rect, delegate: self)
    }
    
    func load() {
        if !hashableParams.isEmpty {
            banner?.fetchBannerAd(withParameters: hashableParams)
        }
    }
    
    func destroy() {
        banner?.customCloseDelegate = nil
        banner?.adView = nil
        banner?.adView?.removeFromSuperview()
        banner?.cleanup()
        banner = nil
    }
}

extension AequusAmazonBanner: DTBAdBannerDispatcherDelegate {
    func adDidLoad(_ adView: UIView) {
        delegate?.didLoad(banner: self)
        delegate?.didShow(banner: self)
    }
    
    func adFailed(toLoad banner: UIView?, errorCode: Int) {
        delegate?.failToLoad(banner: self, error: AequusAmazonBannerError.failToLoad)
    }
    
    func bannerWillLeaveApplication(_ adView: UIView) { }
    
    func impressionFired() {
        delegate?.impression(banner: self)
    }
}
