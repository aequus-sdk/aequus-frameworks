//
//  AequusYahooBidBanner.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 08.02.2022.
//

import AequusCore
import VerizonAdsInlinePlacement

final class AequusYahooBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
    
    var sdkVersion: String {
        VASAds.sdkInfo.version
    }
    
    var bannerView: UIView? {
        return banner
    }
    
    var isReady: Bool = false
	var timeout: Bool = false
    
    private var adFactory: VASInlineAdFactory?
	
    private let viewController: UIViewController
    private var banner: VASInlineAdView?
    private let type: AequusBannerType
    
    init(placementId: String, adm: String?, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.delegate = delegate
        self.type = type
        
        super.init()
        
        
        adFactory = VASInlineAdFactory(placementId: placementId,
                                       adSizes: [configureSize(type: type)],
                                       vasAds: VASAds.sharedInstance,
                                       delegate: self)
		
		adFactory?.requestMetadata = requestMetadata(adm: adm)
    }
	
	private func requestMetadata(adm: String?) -> VASRequestMetadata? {
		guard let adm = adm else {
			return nil
		}
		let metadataBuilder = VASRequestMetadataBuilder()
		metadataBuilder.placementData = ["adContent": adm, "overrideWaterfallProvider": "waterfallprovider/sideloading"]
		return metadataBuilder.build()
	}
    
    func load() {
		self.adFactory?.load(self)
    }
    
    func destroy() {
        banner?.removeFromSuperview()
        banner = nil
    }
    
    private func configureSize(type: AequusBannerType) -> VASInlineAdSize {
        var size: VASInlineAdSize = VASInlineAdSize.init(width: 0, height: 0)
        
        switch type {
        case .mrec:
            size = VASInlineAdSize(width: 300, height: 250)
        case .w320h50:
            size = type.size.width == 320 ?
            VASInlineAdSize(width: 320, height: 50) : VASInlineAdSize(width: 728, height: 90)
        }
        
        return size
    }
}

// MARK: - VASInlineAdFactoryDelegate

extension AequusYahooBanner: VASInlineAdFactoryDelegate {
    func inlineAdFactory(_ adFactory: VASInlineAdFactory, didLoadInlineAd inlineAd: VASInlineAdView) {
        QueueDispatcher.main {
            self.isReady = true
            self.banner = inlineAd
            self.bannerView?.frame = CGRect(x: 0, y: 0, width: self.type.size.width, height: self.type.size.height)
            self.delegate?.didLoad(banner: self)
        }
    }
    
    func inlineAdFactory(_ adFactory: VASInlineAdFactory, didFailWithError errorInfo: VASErrorInfo) {
        delegate?.failToLoad(banner: self, error: errorInfo)
    }
}

extension AequusYahooBanner: VASInlineAdViewDelegate {
    func inlineAdDidFail(_ inlineAd: VASInlineAdView, withError errorInfo: VASErrorInfo) {
        delegate?.failToLoad(banner: self, error: errorInfo)
    }
    
    func inlineAdDidExpand(_ inlineAd: VASInlineAdView) {
        delegate?.didShow(banner: self)
    }
    
    func inlineAdDidCollapse(_ inlineAd: VASInlineAdView) {
        inlineAd.destroy()
    }
    
    func inlineAdClicked(_ inlineAd: VASInlineAdView) {
        delegate?.click(banner: self)
    }
    
    func inlineAdDidRefresh(_ inlineAd: VASInlineAdView) {
        delegate?.didLoad(banner: self)
    }
    
    func inlineAdPresentingViewController() -> UIViewController? {
        viewController
    }
    
    func inlineAdDidLeaveApplication(_ inlineAd: VASInlineAdView) {
        //no need
    }
    
    func inlineAdDidResize(_ inlineAd: VASInlineAdView) {
        //no need
    }
    
    func inlineAd(_ inlineAd: VASInlineAdView, event eventId: String, source: String, arguments: [String : Any]) {
        self.delegate?.impression(banner: self)
    }
}
