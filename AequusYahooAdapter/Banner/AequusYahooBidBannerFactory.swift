//
//  AequusYahooBidBannerFactory.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 08.02.2022.
//

import AequusCore

public final class AequusYahooBidBannerFactory: BidBannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: BannerDelegate) -> Banner? {
        return AequusYahooBanner(placementId: adId, adm: adm, type: type, viewController: viewController, delegate: delegate)
    }
    
    public static func createInstance() -> AequusYahooBidBannerFactory {
        return AequusYahooBidBannerFactory()
    }
}
