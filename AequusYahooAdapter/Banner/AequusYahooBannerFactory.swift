//
//  AequusYahooBannerFactory.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 08.02.2022.
//

import AequusCore

public final class AequusYahooBannerFactory: BannerFactory {
	
	public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
		return AequusYahooBanner(placementId: id, adm: nil, type: type, viewController: viewController, delegate: delegate)
	}
    
    public static func createInstance() -> AequusYahooBannerFactory {
        return AequusYahooBannerFactory()
    }
	
}
