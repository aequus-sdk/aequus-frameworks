//
//  AequusYahooRewarded.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 08.02.2022.
//

import AequusCore
import VerizonAdsInterstitialPlacement

final class AequusYahooRewarded: NSObject, RewardedInterstitial, StatusCheck {
	
	weak var delegate: RewardedInterstitialDelegate?
	
	var isReady: Bool {
		return interstitialAd != nil
	}
	
	var sdkVersion: String {
		VASAds.sdkInfo.version
	}
	
	private var interstitialAdFactory: VASInterstitialAdFactory?
	private var interstitialAd: VASInterstitialAd?
	
	// MARK: - Init
	
	init(placementId: String, adm: String?, delegate: RewardedInterstitialDelegate?) {
		self.delegate = delegate
		
		super.init()
		
		interstitialAdFactory = VASInterstitialAdFactory(
				placementId: placementId,
				vasAds: VASAds.sharedInstance,
				delegate: self
			)
		interstitialAdFactory?.requestMetadata = requestMetadata(adm: adm)
	}
	
	private func requestMetadata(adm: String?) -> VASRequestMetadata? {
		guard let adm = adm else {
			return nil
		}
		let metadataBuilder = VASRequestMetadataBuilder()
		metadataBuilder.placementData = ["adContent": adm, "overrideWaterfallProvider": "waterfallprovider/sideloading"]
		return metadataBuilder.build()
	}
	
	func load() {
		self.interstitialAdFactory?.load(self)
	}
	
	func show(from viewController: UIViewController) {
		self.interstitialAd?.show(from: viewController)
	}
	
	func destroy() {
		interstitialAd = nil
	}
}

extension AequusYahooRewarded: VASInterstitialAdFactoryDelegate {
	
	func interstitialAdFactory(_ adFactory: VASInterstitialAdFactory, didLoad interstitialAd: VASInterstitialAd) {
		self.interstitialAd = interstitialAd
		self.delegate?.didLoad(rewarded: self)
	}
	
	func interstitialAdFactory(_ adFactory: VASInterstitialAdFactory, didFailWithError errorInfo: VASErrorInfo) {
		self.interstitialAd = nil
		delegate?.didFailToLoad(rewarded: self, error: errorInfo)
	}
	
}

// MARK: - VASInterstitialAdDelegate, VASInterstitialAdFactoryDelegate

extension AequusYahooRewarded: VASInterstitialAdDelegate {
	func interstitialAdDidFail(_ interstitialAd: VASInterstitialAd, withError errorInfo: VASErrorInfo) {
		delegate?.didFailToLoad(rewarded: self, error: errorInfo)
	}
	
	func interstitialAdDidShow(_ interstitialAd: VASInterstitialAd) {
		delegate?.didShow(rewarded: self)
	}
	
	func interstitialAdDidClose(_ interstitialAd: VASInterstitialAd) {
		delegate?.eligibleForReward(rewarded: self)
		delegate?.didClose(rewarded: self)
	}
	
	func interstitialAdClicked(_ interstitialAd: VASInterstitialAd) {
		delegate?.click(rewarded: self)
	}
	
	func interstitialAdDidLeaveApplication(_ interstitialAd: VASInterstitialAd) {
		// no need
	}
	
	func interstitialAdEvent(_ interstitialAd: VASInterstitialAd, source: String, eventId: String, arguments: [String : Any]?) {
		self.delegate?.impression(rewarded: self)
	}
}
