//
//  AequusBidInterstitialFactory.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 08.02.2022.
//

import AequusCore

public final class AequusYahooBidRewardedInterstitialFactory: BidRewardedInterstitialFactory {
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        return AequusYahooRewarded(placementId: adId,
                                                  adm: adm,
                                                  delegate: delegate)
    }
}

extension AequusYahooBidRewardedInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusYahooBidRewardedInterstitialFactory {
        return AequusYahooBidRewardedInterstitialFactory()
    }
}
