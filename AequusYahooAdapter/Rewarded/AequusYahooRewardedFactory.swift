//
//  AequusYahooRewardedFactory.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 02.03.2022.
//

import AequusCore

public final class AequusYahooRewardedInterstitialFactory: RewardedInterstitialFactory {
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        return AequusYahooRewarded(placementId: id,
                                                  adm: nil,
                                                  delegate: delegate)
    }
}

extension AequusYahooRewardedInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusYahooRewardedInterstitialFactory {
        return AequusYahooRewardedInterstitialFactory()
    }
}
