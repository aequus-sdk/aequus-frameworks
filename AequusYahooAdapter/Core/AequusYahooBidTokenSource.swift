//
//  AequusYahooBidTokenSource.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 07.02.2022.
//

import AequusCore
import VerizonAdsOMSDK

final class AequusYahooBidTokenSource: BidTokenSource {
    func invoke(completion: @escaping (String?) -> Void) {
        completion(VASAds.sharedInstance.biddingToken())
    }
}

// MARK: - Initializable

extension AequusYahooBidTokenSource: Initializable {
    static func createInstance() -> AequusYahooBidTokenSource {
        return AequusYahooBidTokenSource()
    }
}
