//
//  AequusYahooInitializer.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr Poliakov on 07.02.2022.
//

import Combine
import AequusCore
import VerizonAdsCore


public enum AequusYahooInitializerError: Error {
    case some
}

public final class AequusYahooInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        if !AequusYahooInitializer.isInitialized {
            
            cancelable = privacyPublisher.sink { privacy in
                let builder = VASDataPrivacyBuilder()
                builder.gdpr.consent = privacy.GDPRConsent ? "1" : "0"
                builder.ccpa.privacy = privacy.CCPADoNotSell ? "1YYY" : "1YNN"
                builder.coppa.applies = privacy.underAgeOfConsent
                VASAds.sharedInstance.dataPrivacy = builder.build()
            }
         
            if let config = config, let siteId = config.siteId {
                QueueDispatcher.main {
                    VASAds.logLevel = .debug
                    VASAds.initialize(withSiteId: siteId)
                    AequusYahooInitializer.isInitialized = true
                    completion(true , nil)
                }
            } else {
                completion(false, AequusYahooInitializerError.some)
            }
        } else {
            completion(true, nil)
        }
    }
}

extension AequusYahooInitializer: Initializable {
    public static func createInstance() -> AequusYahooInitializer {
        return AequusYahooInitializer()
    }
}
