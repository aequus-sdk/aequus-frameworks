//
//  AequusBidInterstitialFactory.swift
//  AequusYahooAdapter
//
//  Created by Aleksandr on 08.02.2022.
//

import AequusCore

public final class AequusYahooBidInterstitialFactory: BidInterstitialFactory {
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: InterstitialDelegate) -> Interstitial? {
        return AequusYahooInterstitial(placementId: adId,
                                          adm: adm,
                                          delegate: delegate)
    }
}

extension AequusYahooBidInterstitialFactory: Initializable {
    public static func createInstance() -> AequusYahooBidInterstitialFactory {
        return AequusYahooBidInterstitialFactory()
    }
}
