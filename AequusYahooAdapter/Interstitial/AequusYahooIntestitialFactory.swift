//
//  AequusYahooIntestitialFactory.swift
//  Pods
//
//  Created by Aleksandr on 02.03.2022.
//

import AequusCore

public final class AequusYahooInterstitialFactory: InterstitialFactory {
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        return AequusYahooInterstitial(placementId: id,
                                          adm: nil,
                                          delegate: delegate)
    }
}

extension AequusYahooInterstitialFactory: Initializable {
    public static func createInstance() -> AequusYahooInterstitialFactory {
        return AequusYahooInterstitialFactory()
    }
}
