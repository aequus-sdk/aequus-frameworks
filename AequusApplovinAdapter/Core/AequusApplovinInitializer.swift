//
//  AequusApplovinInitializer.swift
//  AequusApplovinAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import Combine
import AppLovinSDK

public final class AequusApplovinInitializer: AdNetworkInitializer {
	
	var cancelable: AnyCancellable?
	static var alSDK: ALSdk?
	
	public static var isInitialized: Bool {
		ALSdk.shared()?.isInitialized ?? false
	}
	
	public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
		
		guard let sdkKey = config?.sdkKey,
			  let sdk = ALSdk.shared(withKey: sdkKey) else {
			completion(false, nil)
			return
		}
		
		AequusApplovinInitializer.alSDK = sdk
		
		cancelable = privacyPublisher.sink { aequusPrivacy in
			ALPrivacySettings.setHasUserConsent(aequusPrivacy.GDPRConsent)
			ALPrivacySettings.setDoNotSell(aequusPrivacy.CCPADoNotSell)
			ALPrivacySettings.setIsAgeRestrictedUser(aequusPrivacy.underAgeOfConsent)
		}
		
		sdk.initializeSdk(completionHandler: { configuration in
			completion(true, nil)
		})
	}
}

extension AequusApplovinInitializer: Initializable {
	public static func createInstance() -> AequusApplovinInitializer {
		return AequusApplovinInitializer()
	}
}

