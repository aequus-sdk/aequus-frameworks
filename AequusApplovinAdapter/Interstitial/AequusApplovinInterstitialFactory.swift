//
//  AequusApplovinInterstitialFactory.swift
//  AequusApplovinAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore

public final class AequusApplovinInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
		return AequusApplovinInterstitial(adUnitId: id, delegate: delegate)
    }
    
}

extension AequusApplovinInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusApplovinInterstitialFactory {
        return AequusApplovinInterstitialFactory()
    }
}
