//
//  AequusApplovinInterstitial.swift
//  AequusApplovinAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import AppLovinSDK

enum AequusApplovinError: Int32, Error {
	case unknown
	case sdkDisabled = -22
	case noFill = 204
	case adRequestNetworkTimeout = -1001
	case notConnectedToInternet = -1009
	case adRequestUnspecifiedError = -1
	case unableToRenderAd = -6
	case invalidZone = -7
	case invalidAdToken = -8
	case unableToPrecacheResources = -200
	case unableToPrecacheImageResources = -201
	case unableToPrecacheVideoResources = -202
	case invalidResponse = -800
	
	//rewarded
	case incentiviziedAdNotPreloaded = -300
	case incentivizedUnknownServerError = -400
	case incentivizedValidationNetworkTimeout = -500
	case incentivizedUserClosedVideo = -600
	case invalidURL = -900
}

final class AequusApplovinInterstitial: NSObject, Interstitial, StatusCheck {
	
	weak var delegate: InterstitialDelegate?
	
	var sdkVersion: String {
		ALSdk.version()
	}
	
	var isReady: Bool = false
	
	private var ad: ALAd?
	private let interstitialAd: ALInterstitialAd
	private let adUnitId: String
	
	init(adUnitId: String, delegate: InterstitialDelegate?) {
		self.delegate = delegate
		self.adUnitId = adUnitId
		self.interstitialAd = ALInterstitialAd(sdk: AequusApplovinInitializer.alSDK!)
		
		super.init()
	}
	
	func load() {
		AequusApplovinInitializer.alSDK?.adService.loadNextAd(forZoneIdentifier: adUnitId, andNotify: self)
	}
	
	func show(from viewController: UIViewController) {
		if let ad = self.ad {
			interstitialAd.adDisplayDelegate = self
			interstitialAd.show(ad)
		}
	}
	
	func destroy() {
		interstitialAd.adDisplayDelegate = nil
	}
	
}

extension AequusApplovinInterstitial: ALAdLoadDelegate {
	
	func adService(_ adService: ALAdService, didLoad ad: ALAd) {
		self.isReady = true
		self.ad = ad
		delegate?.didLoad(interstitial: self)
	}
	
	func adService(_ adService: ALAdService, didFailToLoadAdWithError code: Int32) {
		delegate?.didFailToLoad(interstitial: self, error: AequusApplovinError(rawValue: code) ?? .unknown)
	}
	
}

extension AequusApplovinInterstitial: ALAdDisplayDelegate {
	
	func ad(_ ad: ALAd, wasDisplayedIn view: UIView) {
		delegate?.didShow(interstitial: self)
		delegate?.impression(interstitial: self)
	}
	
	func ad(_ ad: ALAd, wasHiddenIn view: UIView) {
		delegate?.didClose(interstitial: self)
	}
	
	func ad(_ ad: ALAd, wasClickedIn view: UIView) {
		delegate?.click(interstitial: self)
	}
	
}
