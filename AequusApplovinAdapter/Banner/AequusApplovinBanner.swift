//
//  AequusApplovinBanner.swift
//  AequusApplovinAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import AequusCore
import AppLovinSDK
import CoreGraphics

class AequusApplovinBanner: NSObject, Banner, StatusCheck {
	
	weak var delegate: BannerDelegate?
	
	var sdkVersion: String {
		ALSdk.version()
	}
	var isReady: Bool = false
	var timeout: Bool = false
	
	var bannerView: UIView? {
		return alBanner
	}
	
	private let viewController: UIViewController
	private let adUnitId: String
    private var alBanner: ALAdView = ALAdView(frame: .zero)
    private let type: AequusBannerType
    private var rect: CGRect = CGRect()
	
	init(adUnitId: String, viewController: UIViewController, type: AequusBannerType, delegate: BannerDelegate?) {
		self.viewController = viewController
        self.type = type
		self.adUnitId = adUnitId
		self.delegate = delegate
        
		
		super.init()
        
        rect = CGRect(x: 0, y: 0, width: type.size.width, height: type.size.height)
        alBanner = ALAdView(sdk: AequusApplovinInitializer.alSDK!, size: configureSize(type: type), zoneIdentifier: adUnitId)
	}
	
	func load() {
        self.alBanner.frame = rect
		self.alBanner.adLoadDelegate = self
		self.alBanner.adDisplayDelegate = self
		self.alBanner.loadNextAd()
	}
	
	func destroy() {
		self.alBanner.adLoadDelegate = nil
		self.alBanner.adDisplayDelegate = nil
		self.alBanner.removeFromSuperview()
	}
    
    private func configureSize(type: AequusBannerType) -> ALAdSize {
        var size: ALAdSize = ALAdSize.leader
        rect = CGRect(x: 0, y: 0, width: type.size.width, height: type.size.height)
        
        switch type {
        case .w320h50:
            size = .mrec
        case .mrec:
            size = type.size.width == 320 ? .banner : .leader
        }
        
        return size
    }
}

extension AequusApplovinBanner: ALAdLoadDelegate {
	func adService(_ adService: ALAdService, didLoad ad: ALAd) {
		delegate?.didLoad(banner: self)
		isReady = true
	}
	
	func adService(_ adService: ALAdService, didFailToLoadAdWithError code: Int32) {
		delegate?.failToLoad(banner: self, error: AequusApplovinError(rawValue: code) ?? .unknown)
	}
}

extension AequusApplovinBanner: ALAdDisplayDelegate {
	
	func ad(_ ad: ALAd, wasClickedIn view: UIView) {
		delegate?.click(banner: self)
	}
	
	func ad(_ ad: ALAd, wasDisplayedIn view: UIView) {
		delegate?.didShow(banner: self)
		delegate?.impression(banner: self)
	}
	
	func ad(_ ad: ALAd, wasHiddenIn view: UIView) {}
	
}
