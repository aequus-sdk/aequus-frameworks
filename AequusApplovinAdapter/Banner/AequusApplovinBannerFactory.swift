//
//  AequusApplovinBannerFactory.swift
//  AequusApplovinAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusApplovinBannerFactory: BannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
		return AequusApplovinBanner(adUnitId: adUnitConfig.unitId ?? "", viewController: viewController, type: type, delegate: delegate)
    }

    public static func createInstance() -> AequusApplovinBannerFactory {
        return AequusApplovinBannerFactory()
    }
}
