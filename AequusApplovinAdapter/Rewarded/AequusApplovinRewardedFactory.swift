//
//  AequusApplovinRewardedFactory.swift
//  AequusApplovinAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore

public final class AequusApplovinRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
		return AequusApplovinRewarded(adUnitId: id, delegate: delegate)
    }
    
}

extension AequusApplovinRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusApplovinRewardedFactory {
        return AequusApplovinRewardedFactory()
    }
}
