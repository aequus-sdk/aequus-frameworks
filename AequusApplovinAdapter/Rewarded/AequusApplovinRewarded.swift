//
//  AequusApplovinRewarded.swift
//  AequusApplovinAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import AppLovinSDK

final class AequusApplovinRewarded: NSObject, RewardedInterstitial, StatusCheck {
	
	weak var delegate: RewardedInterstitialDelegate?
	
	var sdkVersion: String {
		ALSdk.version()
	}
	
	var isReady: Bool {
		incentivizedInterstitial.isReadyForDisplay
	}
	
	private let adUnitId: String
	private var incentivizedInterstitial: ALIncentivizedInterstitialAd!
	
	init(adUnitId: String, delegate: RewardedInterstitialDelegate?) {
		self.adUnitId = adUnitId
		self.delegate = delegate
		incentivizedInterstitial = ALIncentivizedInterstitialAd(zoneIdentifier: adUnitId, sdk: AequusApplovinInitializer.alSDK!)
		
		super.init()
		
		incentivizedInterstitial.adDisplayDelegate = self
		incentivizedInterstitial.adVideoPlaybackDelegate = self
	}
	
	func load() {
		incentivizedInterstitial.preloadAndNotify(self)
	}
	
	func show(from viewController: UIViewController) {
		incentivizedInterstitial.showAndNotify(self)
	}
	
	func destroy() {
		incentivizedInterstitial.adDisplayDelegate = nil
		incentivizedInterstitial.adVideoPlaybackDelegate = nil
	}
	
}

extension AequusApplovinRewarded: ALAdRewardDelegate {
	func rewardValidationRequest(for ad: ALAd, didSucceedWithResponse response: [AnyHashable : Any]) {
		
	}
	
	func rewardValidationRequest(for ad: ALAd, didExceedQuotaWithResponse response: [AnyHashable : Any]) {
		
	}
	
	func rewardValidationRequest(for ad: ALAd, wasRejectedWithResponse response: [AnyHashable : Any]) {
		
	}
	
	func rewardValidationRequest(for ad: ALAd, didFailWithError responseCode: Int) {
		
	}
}

extension AequusApplovinRewarded: ALAdLoadDelegate {
	func adService(_ adService: ALAdService, didLoad ad: ALAd) {
		delegate?.didLoad(rewarded: self)
	}
	
	func adService(_ adService: ALAdService, didFailToLoadAdWithError code: Int32) {
		delegate?.didFailToLoad(rewarded: self, error: AequusApplovinError(rawValue: code) ?? .unknown)
	}
}

extension AequusApplovinRewarded: ALAdDisplayDelegate {
	func ad(_ ad: ALAd, wasDisplayedIn view: UIView) {
		delegate?.didShow(rewarded: self)
		delegate?.impression(rewarded: self)
	}
	
	func ad(_ ad: ALAd, wasHiddenIn view: UIView) {
		delegate?.didClose(rewarded: self)
	}
	
	func ad(_ ad: ALAd, wasClickedIn view: UIView) {
		delegate?.click(rewarded: self)
	}
}

extension AequusApplovinRewarded: ALAdVideoPlaybackDelegate  {
	func videoPlaybackBegan(in ad: ALAd) {
		delegate?.rewardedVideoDidStart(rewarded: self)
	}
	
	func videoPlaybackEnded(in ad: ALAd, atPlaybackPercent percentPlayed: NSNumber, fullyWatched wasFullyWatched: Bool) {
		delegate?.rewardedVideoDidComplete(rewarded: self)
		if wasFullyWatched {
			delegate?.eligibleForReward(rewarded: self)
		}
	}

}
