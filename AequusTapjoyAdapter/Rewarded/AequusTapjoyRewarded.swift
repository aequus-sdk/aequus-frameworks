//
//  AequusTapjoyInterstitial.swift
//  AequusTapjoyAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Tapjoy

final class AequusTapjoyRewarded: NSObject, RewardedInterstitial, StatusCheck {
	
	weak var delegate: RewardedInterstitialDelegate?
	
	var isReady: Bool {
		placement?.isContentReady ?? false
	}
	
	var sdkVersion: String {
		Tapjoy.getVersion()
	}
	
	private let unitId: String
	private var placement: TJPlacement?
	
	init(unitId: String, delegate: RewardedInterstitialDelegate?) {
		self.delegate = delegate
		self.unitId = unitId
		
		super.init()
		placement = TJPlacement(name: unitId, delegate: self)
		placement?.videoDelegate = self
	}
	
	func load() {
		placement?.requestContent()
	}
	
	func show(from viewController: UIViewController) {
		placement?.showContent(with: viewController)
	}
	
	func destroy() {
		placement?.videoDelegate = nil
		placement?.delegate = nil
	}
	
}

extension AequusTapjoyRewarded: TJPlacementDelegate {
	func requestDidSucceed(_ placement: TJPlacement) {}
	
	func requestDidFail(_ placement: TJPlacement, error: Error?) {
		delegate?.didFailToLoad(rewarded: self, error: error ?? NSError(domain: "com.aequus.ads.sdk.adapter.tapjoy", code: 999))
	}
	
	func contentIsReady(_ placement: TJPlacement) {
		delegate?.didLoad(rewarded: self)
	}
	
	func contentDidAppear(_ placement: TJPlacement) {
		delegate?.didShow(rewarded: self)
	}
	
	func contentDidDisappear(_ placement: TJPlacement) {
		delegate?.didClose(rewarded: self)
	}
	
	func placement(_ placement: TJPlacement, didRequestReward request: TJActionRequest?, itemId: String?, quantity: Int32) {
		delegate?.eligibleForReward(rewarded: self)
	}
	
	func didClick(_ placement: TJPlacement) {
		delegate?.click(rewarded: self)
	}
}

extension AequusTapjoyRewarded: TJPlacementVideoDelegate {
	func videoDidStart(_ placement: TJPlacement) {
		delegate?.rewardedVideoDidStart(rewarded: self)
	}
	
	func videoDidComplete(_ placement: TJPlacement) {
		delegate?.rewardedVideoDidComplete(rewarded: self)
	}
}
