//
//  AequusTapjoyRewardedFactory.swift
//  AequusTapjoyAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusTapjoyRewardedFactory: RewardedInterstitialFactory {
	
	public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
		
		return AequusTapjoyRewarded(unitId: id, delegate: delegate)
	}
	
}

extension AequusTapjoyRewardedFactory: Initializable {
	
	public static func createInstance() -> AequusTapjoyRewardedFactory {
		return AequusTapjoyRewardedFactory()
	}
	
}
