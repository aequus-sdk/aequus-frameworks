//
//  AequusTapjoyInitializer.swift
//  AequusTapjoyAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine
import Tapjoy

public enum AequusTapjoyInitializingError : Error {
    case some
}

public final class AequusTapjoyInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
	private var initBlock: ((Bool, Error?) -> Void)?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        if !AequusTapjoyInitializer.isInitialized {
            cancelable = privacyPublisher.sink { privacy in
				let privacyPolicy = Tapjoy.getPrivacyPolicy()
				privacyPolicy.setSubjectToGDPR(privacy.GDPRConsent)
				privacyPolicy.setBelowConsentAge(privacy.underAgeOfConsent)
            }
			
			if let sdkKey = config?.sdkKey {
				self.initBlock = completion
				NotificationCenter.default.addObserver(self, selector: #selector(tjcConnectSuccess(notif:)), name: NSNotification.Name(rawValue: TJC_CONNECT_SUCCESS), object: nil)
					
				NotificationCenter.default.addObserver(self, selector: #selector(tjcConnectFail(notif:)), name: NSNotification.Name(rawValue: TJC_CONNECT_FAILED), object: nil)
				
				Tapjoy.setDebugEnabled(true)
				Tapjoy.connect(sdkKey)
			}
			
        } else {
            completion(true, nil)
        }
    }
	
	@objc func tjcConnectSuccess(notif: NSNotification) {
		initBlock?(true, nil)
	}

	@objc func tjcConnectFail(notif: NSNotification) {
		initBlock?(false, nil)
	}
}

extension AequusTapjoyInitializer: Initializable {
    public static func createInstance() -> AequusTapjoyInitializer {
        return AequusTapjoyInitializer()
    }
}
