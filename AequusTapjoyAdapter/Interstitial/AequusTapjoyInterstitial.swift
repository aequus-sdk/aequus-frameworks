//
//  AequusTapjoyInterstitial.swift
//  AequusTapjoyAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Tapjoy

final class AequusTapjoyInterstitial: NSObject, Interstitial {
    
    weak var delegate: InterstitialDelegate?
	
	var isReady: Bool {
		placement?.isContentReady ?? false
	}
    
    var sdkVersion: String {
		Tapjoy.getVersion()
    }
    
    private let unitId: String
	private var placement: TJPlacement?
    
	init(unitId: String, delegate: InterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
		
        super.init()
		placement = TJPlacement(name: unitId, delegate: self)
    }
    
    func load() {
		placement?.requestContent()
    }
    
    func show(from viewController: UIViewController) {
		placement?.showContent(with: viewController)
    }
    
    func destroy() {
		TJPlacement.dismissContent()
		placement?.delegate = nil
    }
    
}

extension AequusTapjoyInterstitial: TJPlacementDelegate {
	func requestDidSucceed(_ placement: TJPlacement) {}
	
	func requestDidFail(_ placement: TJPlacement, error: Error?) {
		delegate?.didFailToLoad(interstitial: self, error: error ?? NSError(domain: "com.aequus.ads.sdk.adapter.tapjoy", code: 999))
	}
	
	func contentIsReady(_ placement: TJPlacement) {
		delegate?.didLoad(interstitial: self)
	}
	
	func contentDidAppear(_ placement: TJPlacement) {
		delegate?.didShow(interstitial: self)
		delegate?.impression(interstitial: self)
	}
	
	func contentDidDisappear(_ placement: TJPlacement) {
		delegate?.didClose(interstitial: self)
	}
	
	func didClick(_ placement: TJPlacement) {
		delegate?.click(interstitial: self)
	}
}
