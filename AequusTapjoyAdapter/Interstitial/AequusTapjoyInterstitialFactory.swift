//
//  AequusTapjoyInterstitialFactory.swift
//  AequusTapjoyAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusTapjoyInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusTapjoyInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusTapjoyInterstitialFactory: Initializable {
    public static func createInstance() -> AequusTapjoyInterstitialFactory {
        return AequusTapjoyInterstitialFactory()
    }
}
