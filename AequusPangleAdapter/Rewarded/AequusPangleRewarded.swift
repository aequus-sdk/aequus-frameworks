//
//  AequusPangleRewarded.swift
//  AequusPangleAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import BUAdSDK

final class AequusPangleRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool = false
    
    var sdkVersion: String {
		BUAdSDKManager.sdkVersion
    }
    
	private let rewardedVideoAd: BURewardedVideoAd
    private let unitId: String
    
    init(unitId: String, delegate: RewardedInterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
            
		
		let model = BURewardedVideoModel()
		self.rewardedVideoAd = BURewardedVideoAd(slotID: unitId, rewardedVideoModel: model)
		
        super.init()
        
		self.rewardedVideoAd.delegate = self
    }
    
    func load() {
		rewardedVideoAd.loadData()
    }
    
    func show(from viewController: UIViewController) {
		rewardedVideoAd.show(fromRootViewController: viewController)
    }
    
    func destroy() {
		self.rewardedVideoAd.delegate = nil
    }
}

extension AequusPangleRewarded: BURewardedVideoAdDelegate {
    
	func rewardedVideoAdDidLoad(_ rewardedVideoAd: BURewardedVideoAd) {
		self.isReady = true
		delegate?.didLoad(rewarded: self)
	}
	
	func rewardedVideoAd(_ rewardedVideoAd: BURewardedVideoAd, didFailWithError error: Error?) {
		delegate?.didFailToLoad(rewarded: self, error: error ?? AequusPangleError.unknown)
	}
	
	func rewardedVideoAdDidVisible(_ rewardedVideoAd: BURewardedVideoAd) {
		delegate?.didShow(rewarded: self)
		delegate?.impression(rewarded: self)
	}
	
	func rewardedVideoAdDidClose(_ rewardedVideoAd: BURewardedVideoAd) {
		self.isReady = false
		delegate?.didClose(rewarded: self)
	}
	
	func rewardedVideoAdDidClick(_ rewardedVideoAd: BURewardedVideoAd) {
		delegate?.click(rewarded: self)
	}
	
	func rewardedVideoAdDidPlayFinish(_ rewardedVideoAd: BURewardedVideoAd, didFailWithError error: Error?) {
		delegate?.rewardedVideoDidComplete(rewarded: self)
		delegate?.eligibleForReward(rewarded: self)
	}
}
