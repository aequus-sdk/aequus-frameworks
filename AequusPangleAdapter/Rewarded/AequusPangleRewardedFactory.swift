//
//  AequusPangleRewardedFactory.swift
//  AequusPangleAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusPangleRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusPangleRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusPangleRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusPangleRewardedFactory {
        return AequusPangleRewardedFactory()
    }
}
