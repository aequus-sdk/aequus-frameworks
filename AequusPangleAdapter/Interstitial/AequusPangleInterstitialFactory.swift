//
//  AequusPangleInterstitialFactory.swift
//  AequusPangleAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusPangleInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusPangleInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusPangleInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusPangleInterstitialFactory {
        return AequusPangleInterstitialFactory()
    }
}
