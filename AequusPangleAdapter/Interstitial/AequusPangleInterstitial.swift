//
//  AequusPangleInterstitial.swift
//  AequusPangleAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import BUAdSDK

enum AequusPangleError: Error {
	case unknown
}

final class AequusPangleInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
	
	var isReady = false
    
    var sdkVersion: String {
		BUAdSDKManager.sdkVersion
    }
    
    private let interstitial: BUFullscreenVideoAd
    private let unitId: String
    
    init(unitId: String, delegate: InterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
		self.interstitial = BUFullscreenVideoAd(slotID: unitId)
            
        super.init()
    }
    
    func load() {
		self.interstitial.delegate = self
		interstitial.loadData()
    }
    
    func show(from viewController: UIViewController) {
		interstitial.show(fromRootViewController: viewController)
    }
    
    func destroy() {
		self.interstitial.delegate = nil
    }
    
}

extension AequusPangleInterstitial: BUFullscreenVideoAdDelegate {
        
	func fullscreenVideoAdVideoDataDidLoad(_ fullscreenVideoAd: BUFullscreenVideoAd) {
		self.isReady = true
		delegate?.didLoad(interstitial: self)
	}
	
	func fullscreenVideoAd(_ fullscreenVideoAd: BUFullscreenVideoAd, didFailWithError error: Error?) {
		delegate?.didFailToLoad(interstitial: self, error: error  ?? AequusPangleError.unknown)
	}
	
	func fullscreenVideoAdDidVisible(_ fullscreenVideoAd: BUFullscreenVideoAd) {
		delegate?.didShow(interstitial: self)
		delegate?.impression(interstitial: self)
	}
	
	func fullscreenVideoAdDidClose(_ fullscreenVideoAd: BUFullscreenVideoAd) {
		self.isReady = false
		delegate?.didClose(interstitial: self)
	}
	
	func fullscreenVideoAdDidClick(_ fullscreenVideoAd: BUFullscreenVideoAd) {
		delegate?.click(interstitial: self)
	}
    
}
