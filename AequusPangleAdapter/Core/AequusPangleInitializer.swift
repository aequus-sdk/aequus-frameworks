//
//  AequusPangleInitializer.swift
//  AequusPangleAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine
import BUAdSDK

public enum AequusPangleInitializingError : Error {
    case some
}

public final class AequusPangleInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        if !AequusPangleInitializer.isInitialized {
            
            cancelable = privacyPublisher.sink { privacy in
				BUAdSDKManager.setCoppa(privacy.underAgeOfConsent ? 1 : 0)
            }
            
            if let config = config,
                let appId = config.appID {
                
				BUAdSDKManager.setAppID(appId)
				
                AequusPangleInitializer.isInitialized = true
                completion(true, nil)
                
            } else {
                completion(false, AequusPangleInitializingError.some)
            }
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusPangleInitializer: Initializable {
    public static func createInstance() -> AequusPangleInitializer {
        return AequusPangleInitializer()
    }
}
