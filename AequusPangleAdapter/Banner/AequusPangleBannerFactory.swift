//
//  AequusPangleBannerFactory.swift
//  AequusPangleAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusPangleBannerFactory: BannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusPangleBanner(unitId: id, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusPangleBannerFactory {
        return AequusPangleBannerFactory()
    }
}
