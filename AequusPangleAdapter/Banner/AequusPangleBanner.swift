//
//  AequusPangleBanner.swift
//  AequusPangleAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore
import BUAdSDK

class AequusPangleBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
    
    var sdkVersion: String {
		BUAdSDKManager.sdkVersion
    }
    var isReady: Bool = false

    var bannerView: UIView? {
        return banner
    }
    
    private let viewController: UIViewController
    private var banner: BUNativeExpressBannerView?
    private let unitId: String
    private let type: AequusBannerType
	var timeout: Bool = false
    
    init(unitId: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
        self.type = type
    }
    
    func load() {
        self.banner = BUNativeExpressBannerView(slotID: self.unitId, rootViewController: self.viewController, adSize: type.size)
        
        let size = type.size
		self.banner?.frame = CGRect(origin: .zero, size: size)
        self.banner?.delegate = self
        
        self.banner?.loadAdData()
    }
    
    func destroy() {
        self.banner?.removeFromSuperview()
		self.banner?.delegate = nil
        self.banner = nil
    }
}

extension AequusPangleBanner: BUNativeExpressBannerViewDelegate {
	func nativeExpressBannerAdViewDidLoad(_ bannerAdView: BUNativeExpressBannerView) {
		delegate?.didLoad(banner: self)
		self.isReady = true
	}
	
	func nativeExpressBannerAdView(_ bannerAdView: BUNativeExpressBannerView, didLoadFailWithError error: Error?) {
		delegate?.failToLoad(banner: self, error: error ?? AequusPangleError.unknown)
	}
    
	func nativeExpressBannerAdViewRenderSuccess(_ bannerAdView: BUNativeExpressBannerView) {
		delegate?.impression(banner: self)
	}
	
	func nativeExpressBannerAdViewWillBecomVisible(_ bannerAdView: BUNativeExpressBannerView) {
		delegate?.didShow(banner: self)
	}
	
	func nativeExpressBannerAdViewDidClick(_ bannerAdView: BUNativeExpressBannerView) {
		delegate?.click(banner: self)
	}
}
