//
//  AequusFyberInitializer.swift
//  AequusCore
//
//  Created by Bohdan Korda on 05.06.2021.
//

import Foundation
import AequusCore
import Combine
import IASDKCore

public enum AequusFyberInitializingError : Error {
    case some
}

public final class AequusFyberInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        
        if !AequusFyberInitializer.isInitialized {
            cancelable = privacyPublisher.sink { privacy in
				IASDKCore.sharedInstance().gdprConsent = privacy.GDPRConsent ? .given : .denied
            }
            
            if let config = config,
                let appId = config.appID {
                
				IASDKCore.sharedInstance().initWithAppID(appId, completionBlock: { success, error in
					completion(success, error)
				}, completionQueue: nil)
                
            } else {
                completion(false, AequusFyberInitializingError.some)
            }
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusFyberInitializer: Initializable {
    public static func createInstance() -> AequusFyberInitializer {
        return AequusFyberInitializer()
    }
}
