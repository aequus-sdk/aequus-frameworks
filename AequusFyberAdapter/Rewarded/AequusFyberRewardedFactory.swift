//
//  AequusFyberRewardedFactory.swift
//  AequusFyberAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusFyberRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusFyberRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusFyberRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusFyberRewardedFactory {
        return AequusFyberRewardedFactory()
    }
}
