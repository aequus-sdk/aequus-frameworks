//
//  AequusFyberBanner.swift
//  AequusFyberAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore
import IASDKCore

enum AequusFyberError: Error {
	case noView
	case initialisationError
}

class AequusFyberBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
		IASDKCore.sharedInstance().version()
    }
	
    var isReady: Bool = false

    var bannerView: UIView? {
        return insideView
    }
    
    private var insideView: UIView?
    private let viewController: UIViewController
    private let unitId: String
	
	private var adSpot: IAAdSpot?
	private var unitController: IAViewUnitController?
	private var mraidContentController: IAMRAIDContentController?
    private let type: AequusBannerType
	var timeout: Bool = false
    
    init(unitId: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
        self.type = type
		
		super.init()
		
        self.insideView = UIView(frame: configureSize(type: type))
        
		let adRequest = IAAdRequest.build { builder in
			builder.spotID = unitId
		}
		
		let mraidContentController = IAMRAIDContentController.build { builder in
			builder.mraidContentDelegate = self
		}
		
		guard let adRequest = adRequest, let mraidContentController = mraidContentController else {
			fatalError()
		}
		
		self.mraidContentController = mraidContentController
		
		let unitController = IAViewUnitController.build { builder in
			builder.unitDelegate = self
			builder.addSupportedContentController(mraidContentController)
		}
		
		guard let unitController = unitController else {
			fatalError()
		}
		
		self.unitController = unitController
		
		adSpot = IAAdSpot.build { builder in
			builder.adRequest = adRequest
			builder.addSupportedUnitController(unitController)
		}
    }
    
    func load() {
		guard let adSpot = adSpot, let unitController = unitController else {
			delegate?.failToLoad(banner: self, error: AequusFyberError.initialisationError)
			return
		}
		adSpot.fetchAd { [weak self] adSpot, adModel, error in
			guard let self = self else { return }
			if let error = error {
				self.delegate?.failToLoad(banner: self, error: error)
			} else {
				guard let insideView = self.insideView else {
					self.delegate?.failToLoad(banner: self, error: AequusFyberError.noView)
					return
				}
				self.delegate?.didLoad(banner: self)
				self.isReady = true
				if adSpot?.activeUnitController == self.unitController {
					unitController.showAd(inParentView: insideView)
				}
			}
		}
    }
    
    func destroy() {
		mraidContentController?.mraidContentDelegate = nil
		mraidContentController = nil
		unitController = nil
		adSpot = nil
    }
    
    private func configureSize(type: AequusBannerType) -> CGRect {
        var rect = CGRect()
        
        switch type {
        case .mrec:
            rect = CGRect(x: 0, y: 0, width: 300, height: 250)
        case .w320h50:
            rect = type.size.width == 320 ?
            CGRect(x: 0, y: 0, width: 320, height: 50) :
            CGRect(x: 0, y: 0, width: 728, height: 90)
        }
        
        return rect
    }
}

extension AequusFyberBanner: IAMRAIDContentDelegate {
	func iaAdWillLogImpression(_ unitController: IAUnitController?) {
		delegate?.didShow(banner: self)
		delegate?.impression(banner: self)
	}
}

extension AequusFyberBanner: IAUnitDelegate {
	func iaParentViewController(for unitController: IAUnitController?) -> UIViewController {
		return viewController
	}
	
	func iaAdDidReceiveClick(_ unitController: IAUnitController?) {
		delegate?.click(banner: self)
	}
}
