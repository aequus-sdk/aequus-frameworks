//
//  AequusFyberBannerFactory.swift
//  AequusFyberAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusFyberBannerFactory: BannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusFyberBanner(unitId: id, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusFyberBannerFactory {
        return AequusFyberBannerFactory()
    }
}
