//
//  AequusFyberInterstitialFactory.swift
//  AequusFyberAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusFyberInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusFyberInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusFyberInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusFyberInterstitialFactory {
        return AequusFyberInterstitialFactory()
    }
}
