//
//  AequusFyberInterstitial.swift
//  AequusFyberAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import IASDKCore

final class AequusFyberInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
	
    var isReady: Bool = false
    
    var sdkVersion: String {
		IASDKCore.sharedInstance().version()
    }
    
	private var adSpot: IAAdSpot!
	private var unitController: IAFullscreenUnitController!
	private var videoContentController: IAVideoContentController!
	private var mraidContentController: IAMRAIDContentController!
	private var parentViewController: UIViewController?
    
    private let unitId: String
    
    init(unitId: String, delegate: InterstitialDelegate?) {
        
        self.delegate = delegate
        self.unitId = unitId
            
        super.init()
        
		let adRequest = IAAdRequest.build { builder in
			builder.spotID = unitId
		}
		
		self.videoContentController = IAVideoContentController.build { builder in
		}
		
		self.mraidContentController = IAMRAIDContentController.build { builder in
			builder.mraidContentDelegate = self
		}
		
		self.unitController = IAFullscreenUnitController.build { builder in
			builder.unitDelegate = self
			
			builder.addSupportedContentController(self.videoContentController)
			builder.addSupportedContentController(self.mraidContentController)
		}
		
		adSpot = IAAdSpot.build { builder in
			builder.adRequest = adRequest!
			builder.addSupportedUnitController(self.unitController!)
		}
    }
    
    func load() {
		adSpot.fetchAd { [weak self] adSpot, adModel, error in
			guard let `self` = self else { return }
			if let error = error {
				self.delegate?.didFailToLoad(interstitial: self, error: error)
			} else {
				self.isReady = true
				self.delegate?.didLoad(interstitial: self)
			}
		}
    }
    
    func show(from viewController: UIViewController) {
		self.parentViewController = viewController
		if adSpot?.activeUnitController == self.unitController {
			unitController.showAd(animated: true, completion: nil)
		}
    }
    
    func destroy() {
		unitController.removeAd()
    }
    
}

extension AequusFyberInterstitial: IAMRAIDContentDelegate {
	func iaAdWillLogImpression(_ unitController: IAUnitController?) {
		delegate?.impression(interstitial: self)
	}
}

extension AequusFyberInterstitial: IAUnitDelegate {
	func iaParentViewController(for unitController: IAUnitController?) -> UIViewController {
		guard let parentViewController = parentViewController else { fatalError() }
		return parentViewController
	}
	
	func iaUnitControllerDidPresentFullscreen(_ unitController: IAUnitController?) {
		delegate?.didShow(interstitial: self)
	}
	
	func iaAdDidReceiveClick(_ unitController: IAUnitController?) {
		delegate?.click(interstitial: self)
	}
	
	func iaUnitControllerDidDismissFullscreen(_ unitController: IAUnitController?) {
		delegate?.didClose(interstitial: self)
	}
}
