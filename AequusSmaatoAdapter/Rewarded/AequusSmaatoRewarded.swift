//
//  AequusSmaatoRewarded.swift
//  AequusSmaatoAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import SmaatoSDKRewardedAds

final class AequusSmaatoRewarded: NSObject, RewardedInterstitial, StatusCheck {
	
	weak var delegate: RewardedInterstitialDelegate?
	
	var isReady: Bool = false
	
	var sdkVersion: String {
		SmaatoSDK.sdkVersion
	}
	
	private let unitId: String
	private var rewarded: SMARewardedInterstitial?
	
	init(unitId: String, delegate: RewardedInterstitialDelegate?) {
		
		self.delegate = delegate
		self.unitId = unitId
		
		super.init()
	}
	
	func load() {
		SmaatoSDK.loadRewardedInterstitial(forAdSpaceId: self.unitId, delegate: self)
	}
	
	func show(from viewController: UIViewController) {
		rewarded?.show(from: viewController)
	}
	
	func destroy() {

	}
	
}

extension AequusSmaatoRewarded: SMARewardedInterstitialDelegate {
	func rewardedInterstitialDidLoad(_ rewardedInterstitial: SMARewardedInterstitial) {
		delegate?.didLoad(rewarded: self)
	}
	
	func rewardedInterstitialDidFail(_ rewardedInterstitial: SMARewardedInterstitial?, withError error: Error) {
		delegate?.didFailToLoad(rewarded: self, error: error)
	}
	
	func rewardedInterstitialDidReward(_ rewardedInterstitial: SMARewardedInterstitial) {
		delegate?.eligibleForReward(rewarded: self)
	}
	
	func rewardedInterstitialDidClick(_ rewardedInterstitial: SMARewardedInterstitial) {
		delegate?.click(rewarded: self)
	}
	
	func rewardedInterstitialDidAppear(_ rewardedInterstitial: SMARewardedInterstitial) {
		delegate?.didShow(rewarded: self)
		delegate?.impression(rewarded: self)
	}
	
	func rewardedInterstitialDidDisappear(_ rewardedInterstitial: SMARewardedInterstitial) {
		delegate?.didClose(rewarded: self)
	}
	
	func rewardedInterstitialDidTTLExpire(_ rewardedInterstitial: SMARewardedInterstitial) {}
}
