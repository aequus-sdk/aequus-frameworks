//
//  AequusSmaatoRewardedFactory.swift
//  AequusSmaatoAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusSmaatoRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusSmaatoRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusSmaatoRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusSmaatoRewardedFactory {
        return AequusSmaatoRewardedFactory()
    }
}
