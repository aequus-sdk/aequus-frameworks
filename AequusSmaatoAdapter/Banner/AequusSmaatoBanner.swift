//
//  AequusSmaatoBanner.swift
//  AequusSmaatoAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore
import SmaatoSDKCore
import SmaatoSDKBanner

class AequusSmaatoBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
		SmaatoSDK.sdkVersion
    }
    var isReady: Bool = false

    var bannerView: UIView? {
        return banner
    }
    
    private let viewController: UIViewController
    private var banner: SMABannerView?
    private let unitId: String
    private let type: AequusBannerType
	var timeout: Bool = false
    
    init(unitId: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
		
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
        self.type = type
		
        banner = SMABannerView(frame: CGRect(origin: .zero, size: type.size))
		
		super.init()
		
		banner?.delegate = self
        banner?.autoreloadInterval = .disabled
    }
    
    func load() {
        banner?.load(withAdSpaceId: unitId, adSize: configureSize(type: type))
    }
    
    func destroy() {
		banner?.removeFromSuperview()
    }
    
    private func configureSize(type: AequusBannerType) -> SMABannerAdSize {
        var size: SMABannerAdSize = SMABannerAdSize.xxLarge_320x50
        
        switch type {
        case .mrec:
            size = .mediumRectangle_300x250
        case .w320h50:
            size = type.size.width == 320 ? .xxLarge_320x50 : .leaderboard_728x90
        }
        
        return size
    }
}

extension AequusSmaatoBanner: SMABannerViewDelegate {
	func presentingViewController(for bannerView: SMABannerView) -> UIViewController {
		return self.viewController
	}
	
	func bannerViewDidTTLExpire(_ bannerView: SMABannerView) {
		
	}
	
	func bannerViewDidLoad(_ bannerView: SMABannerView) {
		delegate?.didLoad(banner: self)
        delegate?.didShow(banner: self)
	}
	
	func bannerView(_ bannerView: SMABannerView, didFailWithError error: Error) {
		delegate?.failToLoad(banner: self, error: error)
	}
	
	func bannerViewDidClick(_ bannerView: SMABannerView) {
		delegate?.click(banner: self)
	}
	
	func bannerViewDidImpress(_ bannerView: SMABannerView) {
		delegate?.impression(banner: self)
	}
}
