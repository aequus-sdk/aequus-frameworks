//
//  AequusSmaatoInitializer.swift
//  AequusCore
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine
import SmaatoSDKCore

public enum AequusSmaatoInitializingError : Error {
    case some
}

public final class AequusSmaatoInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        if !AequusSmaatoInitializer.isInitialized {
            cancelable = privacyPublisher.sink { privacy in
                
                
            }
			
			guard let publisherId = config?.publisherID,
				let smaatoConfig = SMAConfiguration(publisherId: publisherId) else {
				fatalError("SDK config is nil!")
			}
			// allow HTTPS traffic only
			smaatoConfig.httpsOnly = true
			// log errors only
			smaatoConfig.logLevel = .error
			// ads content restriction based on age
			smaatoConfig.maxAdContentRating = .undefined
					 
			SmaatoSDK.initSDK(withConfig:smaatoConfig)
//					 allow the Smaato SDK to automatically get the user's location and put it inside the ad request
//					SmaatoSDK.gpsEnabled = true
			
			completion(true, nil)
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusSmaatoInitializer: Initializable {
    public static func createInstance() -> AequusSmaatoInitializer {
        return AequusSmaatoInitializer()
    }
}
