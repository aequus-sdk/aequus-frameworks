//
//  AequusSmaatoInterstitialFactory.swift
//  AequusSmaatoAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusSmaatoInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusSmaatoInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusSmaatoInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusSmaatoInterstitialFactory {
        return AequusSmaatoInterstitialFactory()
    }
}
