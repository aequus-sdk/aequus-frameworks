//
//  AequusSmaatoInterstitial.swift
//  AequusSmaatoAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import SmaatoSDKInterstitial

final class AequusSmaatoInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
	
    var isReady: Bool = false
    
    var sdkVersion: String {
		SmaatoSDK.sdkVersion
    }
    
    private let unitId: String
	private var interstitial: SMAInterstitial?
    
	init(unitId: String, delegate: InterstitialDelegate?) {
		
        self.delegate = delegate
        self.unitId = unitId
		
        super.init()
    }
    
    func load() {
		SmaatoSDK.loadInterstitial(forAdSpaceId: self.unitId, delegate: self)
    }
    
    func show(from viewController: UIViewController) {
		interstitial?.show(from: viewController)
    }
    
    func destroy() {

    }
    
}

extension AequusSmaatoInterstitial: SMAInterstitialDelegate {
	func interstitialDidLoad(_ interstitial: SMAInterstitial) {
		self.interstitial = interstitial
		self.delegate?.didLoad(interstitial: self)
	}
	
	func interstitial(_ interstitial: SMAInterstitial?, didFailWithError error: Error) {
		self.delegate?.didFailToLoad(interstitial: self, error: error)
	}
	
	func interstitialDidClick(_ interstitial: SMAInterstitial) {
		delegate?.click(interstitial: self)
	}
	
	func interstitialDidAppear(_ interstitial: SMAInterstitial) {
		delegate?.didShow(interstitial: self)
		delegate?.impression(interstitial: self)
	}
	
	func interstitialDidDisappear(_ interstitial: SMAInterstitial) {
		delegate?.didClose(interstitial: self)
	}
	
	func interstitialDidTTLExpire(_ interstitial: SMAInterstitial) {}
}
