//
//  AequusMytargetBidRewardedFactory.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusMytargetBidRewardedFactory: BidRewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusMytargetBidRewarded(unitId: adId, bid: adm, delegate: delegate)
    }
}

extension AequusMytargetBidRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusMytargetBidRewardedFactory {
        return AequusMytargetBidRewardedFactory()
    }
}
