//
//  AequusMytargetRewardedFactory.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusMytargetRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusMytargetRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusMytargetRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusMytargetRewardedFactory {
        return AequusMytargetRewardedFactory()
    }
}
