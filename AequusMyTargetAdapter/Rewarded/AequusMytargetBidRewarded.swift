//
//  AequusMytargetBidRewarded.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MyTargetSDK

final class AequusMytargetBidRewarded: NSObject, RewardedInterstitial, StatusCheck {
	
	weak var delegate: RewardedInterstitialDelegate?
	
	var isReady: Bool = false
	
	var sdkVersion: String {
		"5.14.1"
	}
	
	private let unitId: UInt
	private let bid: String
	private let rewarded: MTRGRewardedAd
	
	init(unitId: String, bid: String, delegate: RewardedInterstitialDelegate?) {
 
		guard let unitId = UInt(unitId) else {
			fatalError("error unit id")
		}
		
		self.delegate = delegate
		self.unitId = unitId
		self.bid = bid
		self.rewarded = MTRGRewardedAd(slotId: unitId)
		super.init()
		
		self.rewarded.delegate = self
	}
	
	func load() {
		rewarded.load(fromBid: self.bid)
	}
	
	func show(from viewController: UIViewController) {
		rewarded.show(with: viewController)
	}
	
	func destroy() {
		rewarded.close()
	}
}

extension AequusMytargetBidRewarded: MTRGRewardedAdDelegate {
	func onLoad(with rewardedAd: MTRGRewardedAd) {
		isReady = true
		delegate?.didLoad(rewarded: self)
	}
	
	func onNoAd(withReason reason: String, rewardedAd: MTRGRewardedAd) {
		delegate?.didFailToLoad(rewarded: self, error: AequusMytargetError.noAd)
	}
	
	func onReward(_ reward: MTRGReward, rewardedAd: MTRGRewardedAd) {
		delegate?.eligibleForReward(rewarded: self)
	}
	
	func onClose(with rewardedAd: MTRGRewardedAd) {
		delegate?.didClose(rewarded: self)
	}
	
	func onDisplay(with rewardedAd: MTRGRewardedAd) {
		delegate?.didShow(rewarded: self)
		delegate?.impression(rewarded: self)
	}
}


