//
//  AequusMytargetRewarded.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MyTargetSDK

final class AequusMytargetRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool = false
    
    var sdkVersion: String {
		"5.14.1"
    }
    
    private let unitId: UInt
	private let rewarded: MTRGRewardedAd
    
    init(unitId: String, delegate: RewardedInterstitialDelegate?) {
 
		guard let unitId = UInt(unitId) else {
			fatalError("error unit id")
		}
		
        self.delegate = delegate
        self.unitId = unitId
		self.rewarded = MTRGRewardedAd(slotId: unitId)
        super.init()
		
		self.rewarded.delegate = self
    }
    
    func load() {
		rewarded.load()
    }
    
    func show(from viewController: UIViewController) {
		rewarded.show(with: viewController)
    }
    
    
    func destroy() {
		rewarded.close()
    }
}

extension AequusMytargetRewarded: MTRGRewardedAdDelegate {
	func onLoad(with rewardedAd: MTRGRewardedAd) {
		isReady = true
		delegate?.didLoad(rewarded: self)
	}
	
	func onNoAd(withReason reason: String, rewardedAd: MTRGRewardedAd) {
		delegate?.didFailToLoad(rewarded: self, error: AequusMytargetError.noAd)
	}
	
	func onReward(_ reward: MTRGReward, rewardedAd: MTRGRewardedAd) {
		delegate?.eligibleForReward(rewarded: self)
	}
	
	func onClose(with rewardedAd: MTRGRewardedAd) {
		delegate?.didClose(rewarded: self)
	}
	
	func onDisplay(with rewardedAd: MTRGRewardedAd) {
		delegate?.didShow(rewarded: self)
		delegate?.impression(rewarded: self)
	}
}


