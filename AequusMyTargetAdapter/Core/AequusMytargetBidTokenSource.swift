//
//  AequusMytargetBidTokenSource.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MyTargetSDK

final class AequusMytargetBidTokenSource: BidTokenSource  {
    
    func invoke(completion: @escaping (String?) -> Void) {
		completion(MTRGManager.getBidderToken())
    }
    
}

extension AequusMytargetBidTokenSource: Initializable {
    static func createInstance() -> AequusMytargetBidTokenSource {
        AequusMytargetBidTokenSource()
    }
}
