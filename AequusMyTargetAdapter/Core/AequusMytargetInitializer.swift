//
//  AequusMytargetInitializer.swift
//  AequusCore
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine

public enum AequusMytargetInitializingError : Error {
    case some
}

public final class AequusMytargetInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        if !AequusMytargetInitializer.isInitialized {
            cancelable = privacyPublisher.sink { privacy in
                
                
            }
			completion(true, nil)
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusMytargetInitializer: Initializable {
    public static func createInstance() -> AequusMytargetInitializer {
        return AequusMytargetInitializer()
    }
}
