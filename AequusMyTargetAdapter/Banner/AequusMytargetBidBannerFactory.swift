//
//  AequusMytargetBidBannerFactory.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusMytargetBidBannerFactory: BidBannerFactory {
	public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: BannerDelegate) -> Banner? {
        return AequusMytargetBidBanner(unitId: adId, type: type, bid: adm, viewController: viewController, delegate: delegate)
	}

	public static func createInstance() -> AequusMytargetBidBannerFactory {
		return AequusMytargetBidBannerFactory()
	}
}
