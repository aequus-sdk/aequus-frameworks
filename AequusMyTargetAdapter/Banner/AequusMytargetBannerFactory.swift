//
//  AequusMytargetBannerFactory.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusMytargetBannerFactory: BannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusMytargetBanner(unitId: id, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusMytargetBannerFactory {
        return AequusMytargetBannerFactory()
    }
}
