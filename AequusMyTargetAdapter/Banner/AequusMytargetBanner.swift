//
//  AequusMytargetBanner.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore
import MyTargetSDK

class AequusMytargetBanner: NSObject, Banner, StatusCheck {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
		"5.14.1"
    }
    var isReady: Bool = false
	var timeout: Bool = false

    var bannerView: UIView? {
        return banner
    }
    
    private let viewController: UIViewController
    private var banner: MTRGAdView?
    private let unitId: UInt
    private let type: AequusBannerType
    
    init(unitId: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
		
		guard let unitId = UInt(unitId) else {
			fatalError("error unit id")
		}
		
        self.type = type
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
		
		banner = MTRGAdView(slotId: unitId, shouldRefreshAd: false)
		
		super.init()
		
        banner?.adSize = configureSize(type: type)
		banner?.delegate = self
    }
    
    func load() {
		banner?.load()
    }
    
    func destroy() {
		banner?.removeFromSuperview()
    }
    
    private func configureSize(type: AequusBannerType) -> MTRGAdSize {
        var size = MTRGAdSize.init()
        
        switch type {
        case .mrec:
            size = .adSize300x250()
        case .w320h50:
            size = type.size.width == 320 ? .adSize320x50() : .adSize728x90()
        }
        
        return size
    }
}

extension AequusMytargetBanner: MTRGAdViewDelegate {
	func onLoad(with adView: MTRGAdView) {
		self.isReady = true
		delegate?.didLoad(banner: self)
	}
	
	func onNoAd(withReason reason: String, adView: MTRGAdView) {
		delegate?.failToLoad(banner: self, error: AequusMytargetError.noAd)
	}
	
	func onAdClick(with adView: MTRGAdView) {
		delegate?.click(banner: self)
	}
    
	func onAdShow(with adView: MTRGAdView) {
		delegate?.didShow(banner: self)
		delegate?.impression(banner: self)
	}
}
