//
//  AequusMytargetBidInterstitialFactory.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusMytargetBidInterstitialFactory: BidInterstitialFactory {
    
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: InterstitialDelegate) -> Interstitial? {
        
		return AequusMytargetBidInterstitial(unitId: adId, bid: adm, delegate: delegate)
    }
}

extension AequusMytargetBidInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusMytargetBidInterstitialFactory {
        return AequusMytargetBidInterstitialFactory()
    }
}
