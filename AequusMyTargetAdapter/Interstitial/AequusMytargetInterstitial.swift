//
//  AequusMytargetInterstitial.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import MyTargetSDK

final class AequusMytargetInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
	
    var isReady: Bool = false
    
    var sdkVersion: String {
		"5.14.1"
    }
    
    private let unitId: UInt
	
	private var interstitial: MTRGInterstitialAd
    
	init(unitId: String, delegate: InterstitialDelegate?) {

		guard let unitId = UInt(unitId) else {
			fatalError("unit id error")
		}
		
        self.delegate = delegate
        self.unitId = unitId
		interstitial = MTRGInterstitialAd(slotId: unitId)
		
        super.init()
		
		interstitial.delegate = self
    }
    
    func load() {
		interstitial.load()
    }
    
    func show(from viewController: UIViewController) {
		interstitial.show(with: viewController)
    }
    
    func destroy() {
		interstitial.close()
    }
    
}

extension AequusMytargetInterstitial: MTRGInterstitialAdDelegate {
	func onLoad(with interstitialAd: MTRGInterstitialAd) {
		self.isReady = true
		delegate?.didLoad(interstitial: self)
	}
	
	func onNoAd(withReason reason: String, interstitialAd: MTRGInterstitialAd) {
		delegate?.didFailToLoad(interstitial: self, error: AequusMytargetError.noAd)
	}
	
	func onDisplay(with interstitialAd: MTRGInterstitialAd) {
		delegate?.didShow(interstitial: self)
		delegate?.impression(interstitial: self)
	}
	
	func onClose(with interstitialAd: MTRGInterstitialAd) {
		self.isReady = false
		delegate?.didClose(interstitial: self)
	}
	
	func onClick(with interstitialAd: MTRGInterstitialAd) {
		delegate?.click(interstitial: self)
	}
}
