//
//  AequusMytargetInterstitialFactory.swift
//  AequusMytargetAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusMytargetInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusMytargetInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusMytargetInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusMytargetInterstitialFactory {
        return AequusMytargetInterstitialFactory()
    }
}
