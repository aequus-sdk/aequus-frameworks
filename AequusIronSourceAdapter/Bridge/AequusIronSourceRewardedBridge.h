//
//  AequusIronSourceRewardedBridge.h
//  Pods
//
//  Created by Oleh Veheria on 12.04.2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AequusIronSourceRewardedDelegate

- (void)rewardedVideoDidLoad:(NSString *)instanceId;
- (void)rewardedVideoDidFailToLoadWithError:(NSError *)error instanceId:(NSString *)instanceId;
- (void)rewardedVideoDidOpen:(NSString *)instanceId;
- (void)rewardedVideoDidClose:(NSString *)instanceId;
- (void)rewardedVideoDidFailToShowWithError:(NSError *)error instanceId:(NSString *)instanceId;
- (void)rewardedVideoDidClick:(NSString *)instanceId;
- (void)rewardedVideoAdRewarded:(NSString *)instanceId;

@end

@interface AequusIronSourceRewardedBridge : NSObject

+ (AequusIronSourceRewardedBridge *)shared;
- (BOOL)hasRewarded;
- (void)initRewarded:(id<AequusIronSourceRewardedDelegate>)delegate;
- (void)load:(NSString *)adUnitId;
- (void)showRewardedWith:(UIViewController *)controller adUnitId:(NSString *)adUnitId;

@end

NS_ASSUME_NONNULL_END
