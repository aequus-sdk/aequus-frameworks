//
//  AequusIronSourceObjCBridge.m
//  AequusIronSourceAdapter
//
//  Created by Oleh Veheria on 23.03.2021.
//

#import "AequusIronSourceBridge.h"
#import "AequusIronSourceInterstitialBridge.h"
#import "AequusIronSourceRewardedBridge.h"
#import "AequusIronSourceBannerBridge.h"
#import <IronSource/IronSource.h>

@interface AequusIronSourceBridge (Impression)<ISImpressionDataDelegate>

@end

@implementation AequusIronSourceBridge

AequusIronSourceBridge* _sharedObject;

+ (AequusIronSourceBridge*)shared {
	if (!_sharedObject) {
		_sharedObject = [AequusIronSourceBridge new];
	}
	return  _sharedObject;
}

+ (void)initialize:(NSString *)appKey {
	// Before initializing any of our products (Rewarded video, Offerwall or Interstitial) you must set
	// their delegates. Take a look at these classes and you will see that they each implement a product
	// protocol. This is our way of letting you know what's going on, and if you don't set the delegates
	// we will not be able to communicate with you.
	// We're passing 'self' to our delegates because we want
	// to be able to enable/disable buttons to match ad availability.
	[AequusIronSourceInterstitialBridge shared];
	[AequusIronSourceRewardedBridge shared];
	[AequusIronSourceBannerBridge shared];
	
	[IronSource addImpressionDataDelegate:[AequusIronSourceBridge shared]];
    [IronSource initISDemandOnly:appKey adUnits:@[IS_INTERSTITIAL, IS_REWARDED_VIDEO]];
}

+ (NSString *)sdkVersion {
	return [IronSource sdkVersion];
}


- (void)impressionDataDidSucceed:(ISImpressionData *)impressionData {
	
}

@end
