//
//  AequusIronSourceBridge.h
//  AequusIronSourceAdapter
//
//  Created by Oleh Veheria on 23.03.2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AequusIronSourceBridge : NSObject

+ (void)initialize:(NSString *)appKey;
+ (NSString *)sdkVersion;

@end

NS_ASSUME_NONNULL_END
