//
//  AequusIronSourceInterstitialBridge.h
//  AequusIronSourceAdapter
//
//  Created by Oleh Veheria on 09.04.2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AequusIronSourceInterstitialDelegate

- (void)interstitialDidLoad:(NSString *)instanceId;
- (void)interstitialDidFailToLoadWithError:(NSError *)error instanceId:(NSString *)instanceId;
- (void)interstitialDidOpen:(NSString *)instanceId;
- (void)interstitialDidClose:(NSString *)instanceId;
- (void)interstitialDidFailToShowWithError:(NSError *)error instanceId:(NSString *)instanceId;
- (void)didClickInterstitial:(NSString *)instanceId;

@end

@interface AequusIronSourceInterstitialBridge : NSObject

+ (AequusIronSourceInterstitialBridge *)shared;
- (BOOL)hasInterstitial;
- (void)initInterstitial:(id<AequusIronSourceInterstitialDelegate>)delegate;
- (void)load:(NSString *)adUnitId;
- (void)showInterstitialWith:(UIViewController *)controller adUnitId:(NSString *)adUnitId;

@end

NS_ASSUME_NONNULL_END
