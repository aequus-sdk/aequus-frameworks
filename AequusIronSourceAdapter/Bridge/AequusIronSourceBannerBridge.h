//
//  AequusIronSourceBannerBridge.h
//  AequusIronSourceAdapter
//
//  Created by Oleh Veheria on 09.04.2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class ISBannerView;
@protocol AequusIronSourceBannerDelegate
- (void)bannerDidLoad:(UIView *)bannerView;
- (void)bannerDidFailToLoadWith:(NSError *)error;
- (void)bannerDidDismissScreen;
- (void)bannerDidShow;
- (void)didClickBanner;

@end

@interface AequusIronSourceBannerBridge : NSObject

+ (AequusIronSourceBannerBridge *)shared;
- (void)initBannerWith:(id<AequusIronSourceBannerDelegate>)delegate placementName:(NSString *)placement;
- (void)load:(NSString *)placement controller:(UIViewController *)controller;
- (void)destroyBanner:(ISBannerView *)bannerView;

@end

NS_ASSUME_NONNULL_END
