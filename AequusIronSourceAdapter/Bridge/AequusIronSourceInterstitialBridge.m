//
//  AequusIronSourceInterstitialBridge.m
//  AequusIronSourceAdapter
//
//  Created by Oleh Veheria on 12.04.2021.
//

#import "AequusIronSourceInterstitialBridge.h"
#import <IronSource/IronSource.h>

@interface AequusIronSourceInterstitialBridge () <ISDemandOnlyInterstitialDelegate>

@property (nonatomic, weak) id<AequusIronSourceInterstitialDelegate> delegate;
@property (nonatomic, weak) NSString *adUnitId;

@end

@implementation AequusIronSourceInterstitialBridge

+ (AequusIronSourceInterstitialBridge *)shared {
    static AequusIronSourceInterstitialBridge *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
		[IronSource setISDemandOnlyInterstitialDelegate:shared];
    });
    return shared;
}

- (BOOL)hasInterstitial {
    return [IronSource hasISDemandOnlyInterstitial:_adUnitId];
}

- (void)initInterstitial:(id<AequusIronSourceInterstitialDelegate>)delegate {
	if (self.delegate != nil) {
		return;
	}
	self.delegate = delegate;
    [IronSource setISDemandOnlyInterstitialDelegate:self];
}

- (void)load:(NSString *)adUnitId {
	self.adUnitId = adUnitId;
	[IronSource loadISDemandOnlyInterstitial:adUnitId];
}

- (void)showInterstitialWith:(UIViewController *)controller adUnitId:(NSString *)adUnitId {
    [IronSource showISDemandOnlyInterstitial:controller instanceId:adUnitId];
}


//MARK: - ISInterstitialDelegate
- (void)didClickInterstitial:(NSString *)instanceId {
	[_delegate didClickInterstitial:instanceId];
}

- (void)interstitialDidClose:(NSString *)instanceId {
	[_delegate interstitialDidClose:instanceId];
}

- (void)interstitialDidFailToLoadWithError:(NSError *)error instanceId:(NSString *)instanceId {
	[_delegate interstitialDidFailToLoadWithError:error instanceId:instanceId];
}

- (void)interstitialDidFailToShowWithError:(NSError *)error instanceId:(NSString *)instanceId {
	[_delegate interstitialDidFailToShowWithError:error instanceId:instanceId];
}

- (void)interstitialDidLoad:(NSString *)instanceId {
	[_delegate interstitialDidLoad:instanceId];
}

- (void)interstitialDidOpen:(NSString *)instanceId {
	[_delegate interstitialDidOpen:instanceId];
}

@end
