//
//  AequusIronSourceRewardedBridge.m
//  AequusIronSourceAdapter
//
//  Created by Oleh Veheria on 09.04.2021.
//

#import "AequusIronSourceRewardedBridge.h"
#import <IronSource/IronSource.h>

@interface AequusIronSourceRewardedBridge () <ISDemandOnlyRewardedVideoDelegate>

@property (nonatomic, weak) id<AequusIronSourceRewardedDelegate> delegate;
@property (nonatomic, weak) NSString *adUnitId;

@end

@implementation AequusIronSourceRewardedBridge

+ (AequusIronSourceRewardedBridge *)shared {
    static AequusIronSourceRewardedBridge *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
		[IronSource setISDemandOnlyRewardedVideoDelegate:shared];
    });
    return shared;
}

- (BOOL)hasRewarded {
    return [IronSource hasISDemandOnlyInterstitial:_adUnitId];
}

- (void)initRewarded:(id<AequusIronSourceRewardedDelegate>)delegate {
	if (self.delegate != nil) {
		return;
	}
    _delegate = delegate;
}

- (void)load:(NSString *)adUnitId {
	self.adUnitId = adUnitId;
	[IronSource loadISDemandOnlyRewardedVideo:adUnitId];
}

- (void)showRewardedWith:(UIViewController *)controller adUnitId:(NSString *)adUnitId {
    [IronSource showISDemandOnlyRewardedVideo:controller instanceId:adUnitId];
}


//MARK: - ISRewardedVideoDelegate
- (void)rewardedVideoAdRewarded:(NSString *)instanceId {
	[_delegate rewardedVideoAdRewarded:instanceId];
}

- (void)rewardedVideoDidClick:(NSString *)instanceId {
	[_delegate rewardedVideoDidClick:instanceId];
}

- (void)rewardedVideoDidClose:(NSString *)instanceId {
	[_delegate rewardedVideoDidClose:instanceId];
}

- (void)rewardedVideoDidFailToLoadWithError:(NSError *)error instanceId:(NSString *)instanceId {
	[_delegate rewardedVideoDidFailToLoadWithError:error instanceId:instanceId];
}

- (void)rewardedVideoDidFailToShowWithError:(NSError *)error instanceId:(NSString *)instanceId {
	[_delegate rewardedVideoDidFailToShowWithError:error instanceId:instanceId];
}

- (void)rewardedVideoDidLoad:(NSString *)instanceId {
	[_delegate rewardedVideoDidLoad:instanceId];
}

- (void)rewardedVideoDidOpen:(NSString *)instanceId {
	[_delegate rewardedVideoDidOpen:instanceId];
}

@end
