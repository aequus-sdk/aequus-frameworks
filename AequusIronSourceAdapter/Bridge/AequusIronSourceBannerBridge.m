//
//  AequusIronSourceBannerBridge.m
//  AequusIronSourceAdapter
//
//  Created by Oleh Veheria on 09.04.2021.
//

#import "AequusIronSourceBannerBridge.h"
#import <IronSource/IronSource.h>

@interface AequusIronSourceBannerBridge() <ISBannerDelegate>

@property (nonatomic, weak) id<AequusIronSourceBannerDelegate> delegate;

@end


@implementation AequusIronSourceBannerBridge

+ (AequusIronSourceBannerBridge *)shared {
    static AequusIronSourceBannerBridge *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
		[IronSource setBannerDelegate: shared];
    });
    return shared;
}

- (void)initBannerWith:(id<AequusIronSourceBannerDelegate>)delegate placementName:(NSString *)placement {
    _delegate = delegate;
}

- (void)load:(NSString *)placement controller:(UIViewController *)controller  {
	ISBannerSize *size = [[ISBannerSize alloc] initWithWidth:320 andHeight:50];
	[IronSource loadBannerWithViewController:controller size:size placement: placement];
}

- (void)destroyBanner:(ISBannerView *)bannerView {
	[IronSource destroyBanner:bannerView];
}


//MARK: - <ISBannerDelegate>

- (void)bannerDidFailToLoadWithError:(NSError *)error {
    [_delegate bannerDidFailToLoadWith:error];
}

- (void)bannerDidLoad:(ISBannerView *)bannerView {
    [_delegate bannerDidLoad:bannerView];
}

- (void)bannerDidDismissScreen {
	[_delegate bannerDidDismissScreen];
}

- (void)bannerDidShow {
	[_delegate bannerDidShow];
}

- (void)didClickBanner {
	[_delegate didClickBanner];
}

- (void)bannerWillLeaveApplication {}
- (void)bannerWillPresentScreen {}

@end
