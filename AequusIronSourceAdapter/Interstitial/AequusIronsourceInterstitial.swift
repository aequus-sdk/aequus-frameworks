//
//  AequusIronsourceInterstitial.swift
//  AequusIronsourceAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine

final class AequusIronsourceInterstitial: NSObject, Interstitial, StatusCheck {

    weak var delegate: InterstitialDelegate?
	private var cancellable: AnyCancellable?
	
    var isReady: Bool {
		AequusIronSourceInterstitialBridge.shared().hasInterstitial()
    }

    var sdkVersion: String {
		AequusIronSourceBridge.sdkVersion()
	}
	
	var multiInstanceSupport: Bool {
		false
	}

	private var onScreen: Bool = false
    private let unitId: String

    init(unitId: String, delegate: InterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
        super.init()
		
		AequusIronSourceInterstitialBridge.shared().initInterstitial(InterstitialManager.shared)
		
		cancellable = InterstitialManager.shared.adEventPublisher.sink { [weak self] event in
			guard let self = self, event.adUnitID == self.unitId else { return }
			switch event.type {
			case .didLoad:
				self.delegate?.didLoad(interstitial: self)
			case .failToLoad(let error):
				self.delegate?.didFailToLoad(interstitial: self, error: error)
			case .didShow:
				self.onScreen = true
				self.delegate?.didShow(interstitial: self)
			case .hide:
				self.onScreen = false
				self.delegate?.didClose(interstitial: self)
			case .failToShow(let error):
				self.onScreen = false
				self.delegate?.didFailToShow(interstitial: self, error: error)
			case .reward:
				break
			case .click:
				self.delegate?.click(interstitial: self)
			case .expire:
				break
			case .impression:
				self.delegate?.impression(interstitial: self)
			case .videoStart:
				break
			case .videoFinished:
				break
			}
		}
    }

    func load() {
		do {
			try IronSourceSDKProxy.shared.loadInterstitialPlacement(withID: self.unitId)
		} catch {
			delegate?.didFailToLoad(interstitial: self, error: error)
		}
    }

    func show(from viewController: UIViewController) {
		AequusIronSourceInterstitialBridge.shared().showInterstitial(with: viewController, adUnitId: self.unitId)
    }

    func destroy() {
		if onScreen {
			return
		}
		cancellable = nil
    }

}
