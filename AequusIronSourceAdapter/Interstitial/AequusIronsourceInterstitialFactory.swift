//
//  AequusIronsourceInterstitialFactory.swift
//  AequusIronsourceAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusIronsourceInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        return AequusIronsourceInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusIronsourceInterstitialFactory: Initializable {
    public static func createInstance() -> AequusIronsourceInterstitialFactory {
        return AequusIronsourceInterstitialFactory()
    }
}
