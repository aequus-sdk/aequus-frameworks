//
//  InterstitialManager.swift
//  AequusIronsourceAdapter
//
//  Created by Bohdan Korda on 03.12.2021.
//

import Foundation
import AequusCore
import Combine

class InterstitialManager: AdapterEventManager, AequusIronSourceInterstitialDelegate {
	
	static var shared: InterstitialManager = InterstitialManager()
	
	private override init() {
		super.init()
	}
	
	func interstitialDidLoad(_ instanceId: String) {
		adDidLoad(instanceId)
	}
	
	func interstitialDidFailToLoadWithError(_ error: Error, instanceId: String) {
		IronSourceSDKProxy.shared.placementFinishShowingInterstitial(placementID: instanceId)
		adDidFailToLoadWithError(error, id: instanceId)
	}
	
	func interstitialDidOpen(_ instanceId: String) {
		adDidShow(instanceId)
		adImpression(instanceId)
	}
	
	func interstitialDidClose(_ instanceId: String) {
		IronSourceSDKProxy.shared.placementFinishShowingInterstitial(placementID: instanceId)
		adDidClose(instanceId)
	}
	
	func interstitialDidFailToShowWithError(_ error: Error, instanceId: String) {
		adDidFailToShowWithError(error, id: instanceId)
	}
	
	func didClickInterstitial(_ instanceId: String) {
		adDidClick(instanceId)
	}
	
}
