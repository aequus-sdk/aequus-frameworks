//
//  AequusIronsourceInitializer.swift
//  AequusCore
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine

public enum AequusIronSourceInitializingError : Error {
    case some
}

public final class AequusIronsourceInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        if !AequusIronsourceInitializer.isInitialized {
            
            cancelable = privacyPublisher.sink { privacy in
                
                
            }
            
            if let config = config,
                let appKey = config.appKey {
				AequusIronSourceBridge.initialize(appKey)
				AequusIronsourceInitializer.isInitialized = true
            } else {
                completion(false, AequusIronSourceInitializingError.some)
            }
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusIronsourceInitializer: Initializable {
    public static func createInstance() -> AequusIronsourceInitializer {
        return AequusIronsourceInitializer()
    }
}
