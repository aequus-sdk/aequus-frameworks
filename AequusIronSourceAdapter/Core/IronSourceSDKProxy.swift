//
//  IronSourceSDKProxy.swift
//  AequusIronsourceAdapter
//
//  Created by Bohdan Korda on 15.12.2021.
//

import Foundation

enum IronSourceSDKProxyError: Error {
	case alreadyInProgress
	case sdkNotInitialised
}

class IronSourceSDKProxy: NSObject {
	
	static let shared = IronSourceSDKProxy()
	
	private var placementsInLoadingInterstitial: [String] = []
	private var placementsInLoadingRewarded: [String] = []
	private override init(){}
	
	func loadInterstitialPlacement(withID placementID: String) throws {
		guard AequusIronsourceInitializer.isInitialized else {
			throw IronSourceSDKProxyError.sdkNotInitialised
		}
		guard !placementsInLoadingInterstitial.contains(placementID) else {
			throw IronSourceSDKProxyError.alreadyInProgress
		}
		
		self.placementsInLoadingInterstitial.append(placementID)
		AequusIronSourceInterstitialBridge.shared().load(placementID)
	}
	
	func loadRewardedPlacement(withID placementID: String) throws {
		guard AequusIronsourceInitializer.isInitialized else {
			throw IronSourceSDKProxyError.sdkNotInitialised
		}
		guard !placementsInLoadingRewarded.contains(placementID) else {
			throw IronSourceSDKProxyError.alreadyInProgress
		}
		
		self.placementsInLoadingRewarded.append(placementID)
		AequusIronSourceRewardedBridge.shared().load(placementID)
	}
	
	func placementFinishShowingInterstitial(placementID: String) {
		if let index = placementsInLoadingInterstitial.firstIndex(of: placementID) {
			placementsInLoadingInterstitial.remove(at: index)
		}
	}
	
	func placementFinishShowingRewarded(placementID: String) {
		if let index = placementsInLoadingRewarded.firstIndex(of: placementID) {
			placementsInLoadingRewarded.remove(at: index)
		}
	}
	
}
