//
//  RewardedManager.swift
//  AequusCore
//
//  Created by Bohdan Korda on 03.12.2021.
//

import Foundation
import AequusCore
import Combine

class RewardedManager: AdapterEventManager, AequusIronSourceRewardedDelegate {
	
	static var shared: RewardedManager = RewardedManager()
	
	private override init() {
		super.init()
	}
	
	func rewardedVideoDidLoad(_ instanceId: String) {
		adDidLoad(instanceId)
	}
	
	func rewardedVideoDidFailToLoadWithError(_ error: Error, instanceId: String) {
		IronSourceSDKProxy.shared.placementFinishShowingRewarded(placementID: instanceId)
		adDidFailToLoadWithError(error, id: instanceId)
	}
	
	func rewardedVideoDidOpen(_ instanceId: String) {
		adDidShow(instanceId)
		adImpression(instanceId)
	}
	
	func rewardedVideoDidClose(_ instanceId: String) {
		IronSourceSDKProxy.shared.placementFinishShowingRewarded(placementID: instanceId)
		adDidClose(instanceId)
	}
	
	func rewardedVideoDidFailToShowWithError(_ error: Error, instanceId: String) {
		adDidFailToShowWithError(error, id: instanceId)
	}
	
	func rewardedVideoDidClick(_ instanceId: String) {
		adDidClick(instanceId)
	}
	
	func rewardedVideoAdRewarded(_ instanceId: String) {
		adDidReward(instanceId)
	}
	
}
