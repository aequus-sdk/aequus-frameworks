//
//  AequusIronsourceRewarded.swift
//  AequusIronsourceAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine

final class AequusIronsourceRewarded: NSObject, RewardedInterstitial {
    
    weak var delegate: RewardedInterstitialDelegate?
	private var cancellable: AnyCancellable?
	
    var isReady: Bool {
		AequusIronSourceRewardedBridge.shared().hasRewarded()
    }
    
    var sdkVersion: String {
		AequusIronSourceBridge.sdkVersion()
    }
	
	var multiInstanceSupport: Bool {
		false
	}
    
	private var onScreen: Bool = false
    private let unitId: String
    
    init(unitId: String, delegate: RewardedInterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
        super.init()
        
		AequusIronSourceRewardedBridge.shared().initRewarded(RewardedManager.shared)
		
		cancellable = RewardedManager.shared.adEventPublisher.sink { [weak self] event in
			guard let self = self, event.adUnitID == self.unitId else { return }
			switch event.type {
			case .didLoad:
				self.delegate?.didLoad(rewarded: self)
			case .failToLoad(let error):
				self.delegate?.didFailToLoad(rewarded: self, error: error)
			case .didShow:
				self.onScreen = true
				self.delegate?.didShow(rewarded: self)
			case .hide:
				self.onScreen = false
				self.delegate?.didClose(rewarded: self)
			case .failToShow(let error):
				self.onScreen = false
				self.delegate?.didFailToShow(rewarded: self, error: error)
			case .reward:
				self.delegate?.eligibleForReward(rewarded: self)
			case .click:
				self.delegate?.click(rewarded: self)
			case .expire:
				break
			case .impression:
				self.delegate?.impression(rewarded: self)
			case .videoStart:
				break
			case .videoFinished:
				break
			}
		}
    }
    
    func load() {
		do {
			try IronSourceSDKProxy.shared.loadRewardedPlacement(withID: self.unitId)
		} catch {
			delegate?.didFailToLoad(rewarded: self, error: error)
		}
    }
    
    func show(from viewController: UIViewController) {
		AequusIronSourceRewardedBridge.shared().showRewarded(with: viewController, adUnitId: self.unitId)
    }
    
    func destroy() {
		if onScreen {
			return
		}
		cancellable = nil
    }
}
