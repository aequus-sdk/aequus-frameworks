//
//  AequusIronsourceRewardedFactory.swift
//  AequusIronsourceAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusIronsourceRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusIronsourceRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusIronsourceRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusIronsourceRewardedFactory {
        return AequusIronsourceRewardedFactory()
    }
}
