//
//  AequusInmobiRewardedFactory.swift
//  AequusInmobiAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusInmobiRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusInmobiRewarded(placementId: id, delegate: delegate)
    }
    
}

extension AequusInmobiRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusInmobiRewardedFactory {
        return AequusInmobiRewardedFactory()
    }
}
