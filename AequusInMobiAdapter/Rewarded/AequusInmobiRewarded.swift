//
//  AequusInmobiRewarded.swift
//  AequusInmobiAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import InMobiSDK

final class AequusInmobiRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool {
		interstitial?.isReady() ?? false
    }
    
    var sdkVersion: String {
		IMSdk.getVersion()
	}
    
    private let placementId: Int64
	private var interstitial: IMInterstitial?
    
    init(placementId: String, delegate: RewardedInterstitialDelegate?) {
		guard let placementId = Int64(placementId) else {
			fatalError("null placementId")
		}
		
        self.delegate = delegate
        self.placementId = placementId
            
        super.init()
		
		interstitial = IMInterstitial.init(placementId: placementId)
		interstitial?.delegate = self
    }
    
    func load() {
		interstitial?.load()
    }
    
    func show(from viewController: UIViewController) {
		interstitial?.show(from: viewController)
    }
    
    func destroy() {
		interstitial?.cancel()
    }
}

extension AequusInmobiRewarded: IMInterstitialDelegate {
	
	func interstitialDidFinishLoading(_ interstitial: IMInterstitial!) {
		delegate?.didLoad(rewarded: self)
	}
	
	func interstitial(_ interstitial: IMInterstitial!, didFailToLoadWithError error: IMRequestStatus!) {
		delegate?.didFailToLoad(rewarded: self, error: error)
	}
	
	func interstitialDidPresent(_ interstitial: IMInterstitial!) {
		delegate?.didShow(rewarded: self)
		delegate?.impression(rewarded: self)
	}
	
	func interstitial(_ interstitial: IMInterstitial!, didFailToPresentWithError error: IMRequestStatus!) {
		delegate?.didFailToShow(rewarded: self, error: error)
	}
	
	func interstitialDidDismiss(_ interstitial: IMInterstitial!) {
		delegate?.didClose(rewarded: self)
	}
	
	func interstitial(_ interstitial: IMInterstitial!, didInteractWithParams params: [AnyHashable : Any]!) {
		delegate?.click(rewarded: self)
	}
	
	func interstitial(_ interstitial: IMInterstitial!, rewardActionCompletedWithRewards rewards: [AnyHashable : Any]!) {
		delegate?.eligibleForReward(rewarded: self)
	}
}
