//
//  AequusInmobiBidRewardedFactory.swift
//  AequusInmobiAdapter
//
//  Created by Aleksandr on 13.05.2022.
//

import AequusCore

public final class AequusInmobiBidRewardedFactory: BidRewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        return AequssInmobiBidRewarded(placementId: adId, response: adm, delegate: delegate)
    }
}

extension AequusInmobiBidRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusInmobiBidRewardedFactory {
        return AequusInmobiBidRewardedFactory()
    }
}
