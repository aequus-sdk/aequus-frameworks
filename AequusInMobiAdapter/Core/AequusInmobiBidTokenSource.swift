//
//  AequusInmobiBidTokenSource.swift
//  AequusInmobiAdapter
//
//  Created by Aleksandr on 13.05.2022.
//

import AequusCore
import InMobiSDK

final class AequusInmobiBidTokenSource: BidTokenSource {
    func invoke(completion: @escaping (String?) -> Void) {
        if AequusInmobiInitializer.isInitialized {
            completion(IMSdk.getToken())
        } else {
            completion(nil)
        }
    }
}

extension AequusInmobiBidTokenSource: Initializable {
    static func createInstance() -> AequusInmobiBidTokenSource {
        return AequusInmobiBidTokenSource()
    }
}
