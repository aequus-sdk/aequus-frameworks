//
//  AequusInmobiInitializer.swift
//  AequusInmobiAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine
import InMobiSDK

public enum AequusInmobiInitializingError : Error {
    case some
}

public final class AequusInmobiInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
		
        if !AequusInmobiInitializer.isInitialized {
            
			IMUnifiedIdService.enableDebugMode(true)
			
            cancelable = privacyPublisher.sink { privacy in
				IMSdk.updateGDPRConsent([IM_GDPR_CONSENT_AVAILABLE:privacy.GDPRConsent])
            }
            
            if let config = config,
                let accountID = config.accountID {
                
				IMSdk.setLogLevel(.error)
				IMSdk.initWithAccountID(accountID) { error in
					if let error = error {
						print("error = \(error)")
						completion(false, error)
                    } else {
                        AequusInmobiInitializer.isInitialized = true
                        completion(true, nil)
                    }
				}
                
            } else {
                completion(false, AequusInmobiInitializingError.some)
            }
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusInmobiInitializer: Initializable {
    public static func createInstance() -> AequusInmobiInitializer {
        return AequusInmobiInitializer()
    }
}
