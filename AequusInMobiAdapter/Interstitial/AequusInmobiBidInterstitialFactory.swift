//
//  AequusInmobiBidIntestitialFactory.swift
//  AequusInmobiAdapter
//
//  Created by Aleksandr on 13.05.2022.
//

import AequusCore

public final class AequusInmobiBidInterstitialFactory: BidInterstitialFactory {
    public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: InterstitialDelegate) -> Interstitial? {
        return AequusInmobiBidInterstitial(placementId: adId, response: adm, delegate: delegate)
    }
}

extension AequusInmobiBidInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusInmobiBidInterstitialFactory {
        return AequusInmobiBidInterstitialFactory()
    }
}
