//
//  AequusInmobiBidIntestitial.swift
//  AequusInmobiAdapter
//
//  Created by Aleksandr on 13.05.2022.
//

import AequusCore
import InMobiSDK

final class AequusInmobiBidInterstitial: NSObject, Interstitial {
    
    weak var delegate: InterstitialDelegate?
    
    var isReady: Bool {
        interstitial?.isReady() ?? false
    }
    
    var sdkVersion: String {
        IMSdk.getVersion()
    }
    
    private let bidResponse: String
    private let placementId: Int64
    private var interstitial: IMInterstitial?
    
    init(placementId: String, response: String, delegate: InterstitialDelegate?) {
        guard let placementId = Int64(placementId) else {
            fatalError("null placementId")
        }
        
        self.bidResponse = response
        self.delegate = delegate
        self.placementId = placementId
            
        super.init()
        
        interstitial = IMInterstitial.init(placementId: placementId)
        interstitial?.delegate = self
    }
    
    func load() {
        interstitial?.load(bidResponse.data(using: .utf8))
    }
    
    func show(from viewController: UIViewController) {
        interstitial?.show(from: viewController)
    }
    
    func destroy() {
        interstitial?.cancel()
    }
    
}

extension AequusInmobiBidInterstitial: IMInterstitialDelegate {
    
    func interstitialDidFinishLoading(_ interstitial: IMInterstitial!) {
        delegate?.didLoad(interstitial: self)
    }
    
    func interstitial(_ interstitial: IMInterstitial!, didFailToLoadWithError error: IMRequestStatus!) {
        delegate?.didFailToLoad(interstitial: self, error: error)
    }
    
    func interstitialDidPresent(_ interstitial: IMInterstitial!) {
        delegate?.didShow(interstitial: self)
        delegate?.impression(interstitial: self)
    }
    
    func interstitial(_ interstitial: IMInterstitial!, didFailToPresentWithError error: IMRequestStatus!) {
        delegate?.didFailToShow(interstitial: self, error: error)
    }
    
    func interstitialDidDismiss(_ interstitial: IMInterstitial!) {
        delegate?.didClose(interstitial: self)
    }
    
    func interstitial(_ interstitial: IMInterstitial!, didInteractWithParams params: [AnyHashable : Any]!) {
        delegate?.click(interstitial: self)
    }
}
