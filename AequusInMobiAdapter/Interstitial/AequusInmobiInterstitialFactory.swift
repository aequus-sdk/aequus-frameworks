//
//  AequusInmobiInterstitialFactory.swift
//  AequusInmobiAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusInmobiInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
		return AequusInmobiInterstitial(placementId: id, delegate: delegate)
    }
    
}

extension AequusInmobiInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusInmobiInterstitialFactory {
        return AequusInmobiInterstitialFactory()
    }
}
