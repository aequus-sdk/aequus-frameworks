//
//  AequusInmobiBidBannerFactory.swift
//  AequusInmobiAdapter
//
//  Created by Aleksandr on 13.05.2022.
//

import AequusCore
import InMobiSDK

public final class AequusInmobiBidBannerFactory: BidBannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: BannerDelegate) -> Banner? {
        return AequusInmobiBidBanner(placementId: adId, response: adm, type: type, viewController: viewController, delegate: delegate)
    }
}

extension AequusInmobiBidBannerFactory: Initializable {
    public static func createInstance() -> AequusInmobiBidBannerFactory {
        return AequusInmobiBidBannerFactory()
    }
}
