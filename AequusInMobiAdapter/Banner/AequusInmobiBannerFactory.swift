//
//  AequusInmobiBannerFactory.swift
//  AequusInmobiAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusInmobiBannerFactory: BannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusInmobiBanner(placementId: id, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusInmobiBannerFactory {
        return AequusInmobiBannerFactory()
    }
}
