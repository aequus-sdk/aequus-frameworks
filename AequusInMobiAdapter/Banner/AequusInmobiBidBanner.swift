//
//  AequusInmobiBidBanner.swift
//  AequusInmobiAdapter
//
//  Created by Aleksandr on 13.05.2022.
//

import AequusCore
import InMobiSDK

class AequusInmobiBidBanner: NSObject, Banner {
    
    weak var delegate: BannerDelegate?
    
    var sdkVersion: String {
        IMSdk.getVersion()
    }
    
    var isReady: Bool = false
    var timeout: Bool = false
    
    var bannerView: UIView? {
        return banner
    }
    
    private let viewController: UIViewController
    private var banner: IMBanner?
    private let placementId: Int64
    private let responseBid: String
    private let type: AequusBannerType
    
    init(placementId: String, response: String, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate?) {
        
        guard let placementId = Int64(placementId) else {
            fatalError("null placementId")
        }
        
        var rect = CGRect()
        
        switch type {
        case .mrec:
            rect = CGRect(x: 0, y: 0, width: 300, height: 250)
        case .w320h50:
            rect = type.size.width == 320 ?
            CGRect(x: 0, y: 0, width: 320, height: 50) :
            CGRect(x: 0, y: 0, width: 728, height: 90)
        }
        
        banner = IMBanner.init(frame: rect,
                               placementId: placementId)
        self.responseBid = response
        self.type = type
        self.viewController = viewController
        self.placementId = placementId
        self.delegate = delegate
    }
    
    func load() {
        banner?.shouldAutoRefresh(false)
        banner?.delegate = self
        banner?.load(responseBid.data(using: .utf8))
    }
    
    func destroy() {
        self.banner?.cancel()
        self.banner?.removeFromSuperview()
        self.banner = nil
    }
}

extension AequusInmobiBidBanner: IMBannerDelegate {
    func bannerDidFinishLoading(_ banner: IMBanner!) {
        isReady = true
        delegate?.didLoad(banner: self)
        delegate?.didShow(banner: self)
        delegate?.impression(banner: self)
    }
    
    func banner(_ banner: IMBanner!, didFailToLoadWithError error: IMRequestStatus!) {
        delegate?.failToLoad(banner: self, error: error)
    }
    
    func banner(_ banner: IMBanner!, didInteractWithParams params: [AnyHashable : Any]!) {
        delegate?.click(banner: self)
    }
}
