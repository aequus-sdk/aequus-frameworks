//
//  AequusFacebookInitializer.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import FBAudienceNetwork
import Combine

/*
 
 https://developers.facebook.com/docs/audience-network/guides/SKAdNetwork
 
 <array>
 <dict>
 <key>SKAdNetworkIdentifier</key>
 <string>v9wttpbfk9.skadnetwork</string>
 </dict>
 <dict>
 <key>SKAdNetworkIdentifier</key>
 <string>n38lu8286q.skadnetwork</string>
 </dict>
 </array>
 
 https://developers.facebook.com/docs/ios/implement-sdk
 
 <key>CFBundleURLTypes</key>
 <array>
 <dict>
 <key>CFBundleURLSchemes</key>
 <array>
 <string>fb842110613204811</string>
 </array>
 </dict>
 </array>
 <key>FacebookAppID</key>
 <string>842110613204811</string>
 <key>FacebookDisplayName</key>
 <string>Aequus App</string>
 
 <key>LSApplicationQueriesSchemes</key>
 <array>
 <string>fbapi</string>
 <string>fbapi20130214</string>
 <string>fbapi20130410</string>
 <string>fbapi20130702</string>
 <string>fbapi20131010</string>
 <string>fbapi20131219</string>
 <string>fbapi20140410</string>
 <string>fbapi20140116</string>
 <string>fbapi20150313</string>
 <string>fbapi20150629</string>
 <string>fbapi20160328</string>
 <string>fbauth</string>
 <string>fb-messenger-share-api</string>
 <string>fbauth2</string>
 <string>fbshareextension</string>
 </array>
 */


public final class AequusFacebookInitializer: AdNetworkInitializer {
    public static var isInitialized: Bool = false
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        if !AequusFacebookInitializer.isInitialized {
            
            
            FBAudienceNetworkAds.initialize(with: nil) { result in
                completion(result.isSuccess, nil)
                AequusFacebookInitializer.isInitialized = result.isSuccess
            }
            
            //    FBAdSettings.setAdvertiserTrackingEnabled(true)
        }
    }
    
}

extension AequusFacebookInitializer: Initializable {
    public static func createInstance() -> AequusFacebookInitializer {
        return AequusFacebookInitializer()
    }
}
