//
//  AequusFacebookBidTokenSource.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import FBAudienceNetwork

final class AequusFacebookBidTokenSource: BidTokenSource  {
    func invoke(completion: @escaping (String?) -> Void) {
        if !AequusFacebookInitializer.isInitialized {
            completion(nil)
            return
        }

        completion(FBAdSettings.bidderToken)
    }
}

extension AequusFacebookBidTokenSource: Initializable {
    static func createInstance() -> AequusFacebookBidTokenSource {
        return AequusFacebookBidTokenSource()
    }
}
