//
//  AequusFacebookBanner.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import AequusCore
import FBAudienceNetwork

final class AequusFacebookBanner: NSObject, Banner, Impressionable {
    
	weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
        return "\(FBAudienceNetworkAds.version())"
    }
    
    var bannerView: UIView? {
        return banner
    }
    
    var isReady: Bool {
        return banner?.isAdValid ?? false
    }
    private let placementId: String
    private let bidToken: String?
    private let type: AequusBannerType
    private var viewController: UIViewController
    private var banner: FBAdView?
	var timeout: Bool = false
    
    init(placementId: String, token: String?, type: AequusBannerType, viewController: UIViewController, delegate: BannerDelegate) {
        self.type = type
        self.delegate = delegate
        self.viewController = viewController
        self.placementId = placementId
        self.bidToken = token
    }
    
    func destroy() {
        banner?.removeFromSuperview()
        banner = nil
    }
    
    func load() {
        DispatchQueue.main.async {
            self.banner = FBAdView(placementID: self.placementId, adSize: self.configureSize(type: self.type), rootViewController: self.viewController)
            self.banner?.frame = CGRect(origin: .zero, size: self.type.size)
            self.banner?.delegate = self
            
            if let token = self.bidToken {
                self.banner?.loadAd(withBidPayload: token)
            } else {
                self.banner?.loadAd()
            }
        }
    }
    
    private func configureSize(type: AequusBannerType) -> FBAdSize {
        var size: FBAdSize = FBAdSize.init()
        
        switch type {
        case .mrec:
            size = kFBAdSizeHeight250Rectangle
        case .w320h50:
            size = type.size.width == 320 ? kFBAdSizeHeight50Banner : kFBAdSizeHeight90Banner
        }
        
        return size
    }
}

extension AequusFacebookBanner: FBAdViewDelegate {
    
    func adViewDidLoad(_ adView: FBAdView) {
        delegate?.didLoad(banner: self)
    }
    
    func adView(_ adView: FBAdView, didFailWithError error: Error) {
        delegate?.failToLoad(banner: self, error: error)
    }
    
    func adViewDidClick(_ adView: FBAdView) {
        delegate?.click(banner: self)
    }
    
    func adViewWillLogImpression(_ adView: FBAdView) {
        delegate?.didShow(banner: self)
        delegate?.impression(banner: self)
    }
    
}
