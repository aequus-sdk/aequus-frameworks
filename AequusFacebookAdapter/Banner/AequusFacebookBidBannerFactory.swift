//
//  AequusFacebookBidBannerFactory.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore

public final class AequusFacebookBidBannerFactory: BidBannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable : Any]?, delegate: BannerDelegate) -> Banner? {
        return AequusFacebookBanner(placementId: adId, token: adm, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusFacebookBidBannerFactory {
        return AequusFacebookBidBannerFactory()
    }
}

