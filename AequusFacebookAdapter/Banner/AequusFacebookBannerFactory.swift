//
//  AequusFacebookBannerFactory.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore

public final class AequusFacebookBannerFactory: BannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusFacebookBanner(placementId: id, token: nil, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusFacebookBannerFactory {
        return AequusFacebookBannerFactory()
    }
}
