//
//  AequusFacebookBidInterstitialFactory.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import FBAudienceNetwork

final class AequusFacebookBidInterstitialFactory: BidInterstitialFactory {
    
    func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable : Any]?, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusFacebookInterstitial(placementId: adId, bidToken: adm, delegate: delegate)
    }
    
}

extension AequusFacebookBidInterstitialFactory: Initializable {
    static func createInstance() -> AequusFacebookBidInterstitialFactory {
        return AequusFacebookBidInterstitialFactory()
    }
}
