//
//  AequusFacebookInterstitial.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import FBAudienceNetwork

class AequusFacebookInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
    
    var isReady: Bool {
        return interstitialAd?.isAdValid ?? false
    }
    
    var sdkVersion: String {
        return "\(FBAudienceNetworkAds.version())"
    }
    
    private var interstitialAd: FBInterstitialAd?
    private var placementId: String
    private var bidToken: String?
    
    init(placementId: String, bidToken: String?, delegate: InterstitialDelegate) {
        self.placementId = placementId
        self.bidToken = bidToken
        self.delegate = delegate
        
        super.init()
        
        interstitialAd = FBInterstitialAd(placementID: placementId)
        interstitialAd?.delegate = self
    }
    
    func load() {
        DispatchQueue.main.async {
            if let token = self.bidToken {
                self.interstitialAd?.load(withBidPayload: token)
            } else {
                self.interstitialAd?.load()
            }
        }
    }
    
    func show(from viewController: UIViewController) {
        self.interstitialAd?.show(fromRootViewController: viewController)
        self.delegate?.didShow(interstitial: self)
    }
    
    func destroy() {
        
    }
}

extension AequusFacebookInterstitial:  FBInterstitialAdDelegate {
    
    func interstitialAdDidClick(_ interstitialAd: FBInterstitialAd) {
        delegate?.click(interstitial: self)
    }
 
    func interstitialAdDidClose(_ interstitialAd: FBInterstitialAd) {
        delegate?.didClose(interstitial: self)
    }
    
    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
        delegate?.didLoad(interstitial: self)
    }

    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
        delegate?.didFailToLoad(interstitial: self, error: error)
    }
    
    func interstitialAdWillLogImpression(_ interstitialAd: FBInterstitialAd) {
        delegate?.impression(interstitial: self)
    }
    
}
