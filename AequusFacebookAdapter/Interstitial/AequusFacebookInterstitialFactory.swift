//
//  AequusFacebookInterstitialFactory.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import FBAudienceNetwork

public final class AequusFacebookInterstitialFactory: InterstitialFactory {

    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusFacebookInterstitial(placementId: id, bidToken: nil, delegate: delegate)
    }
    
    public static func createInstance() -> AequusFacebookInterstitialFactory {
        return AequusFacebookInterstitialFactory()
    }
}

