//
//  AequusFacebookBidRewardedFactory.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

final class AequusFacebookBidRewardedFactory: BidRewardedInterstitialFactory {
    
    func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable : Any]?, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusFacebookRewarded(placementId: adId, bidToken: adm, delegate: delegate)
    }
    
}

extension AequusFacebookBidRewardedFactory: Initializable {
    static func createInstance() -> AequusFacebookBidRewardedFactory {
        return AequusFacebookBidRewardedFactory()
    }
}
