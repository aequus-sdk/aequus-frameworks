//
//  AequusFacebookRewarded.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 14.05.2021.
//

import Foundation
import AequusCore
import FBAudienceNetwork

class AequusFacebookRewarded: NSObject, RewardedInterstitial, StatusCheck {
    
    weak var delegate: RewardedInterstitialDelegate?
    
    var isReady: Bool {
        return rewardedAd?.isAdValid ?? false
    }
    
    var sdkVersion: String {
        return "\(FBAudienceNetworkAds.version())"
    }
    
    private var rewardedAd: FBRewardedVideoAd?
    private var placementId: String
    private var bidToken: String?
    
    init(placementId: String, bidToken: String?, delegate: RewardedInterstitialDelegate) {
        self.placementId = placementId
        self.bidToken = bidToken
        self.delegate = delegate
        
        super.init()
        
        rewardedAd = FBRewardedVideoAd(placementID: placementId)
        rewardedAd?.delegate = self
    }
    
    func load() {
        DispatchQueue.main.async {
            if let token = self.bidToken {
                self.rewardedAd?.load(withBidPayload: token)
            } else {
                self.rewardedAd?.load()
            }
        }
    }
    
    func show(from viewController: UIViewController) {
        
        self.rewardedAd?.show(fromRootViewController: viewController)
        
        self.delegate?.didShow(rewarded: self)
    }
    
    func destroy() {
        
    }
}

extension AequusFacebookRewarded:  FBRewardedVideoAdDelegate {
    
    func rewardedVideoAdDidLoad(_ rewardedVideoAd: FBRewardedVideoAd) {
        delegate?.didLoad(rewarded: self)
    }
    
    func rewardedVideoAd(_ rewardedVideoAd: FBRewardedVideoAd, didFailWithError error: Error) {
        delegate?.didFailToLoad(rewarded: self, error: error)
    }
    
    func rewardedVideoAdDidClick(_ rewardedVideoAd: FBRewardedVideoAd) {
        delegate?.click(rewarded: self)
    }
    
    func rewardedVideoAdWillLogImpression(_ rewardedVideoAd: FBRewardedVideoAd) {
        delegate?.impression(rewarded: self)
    }
    
    func rewardedVideoAdDidClose(_ rewardedVideoAd: FBRewardedVideoAd) {
        delegate?.didClose(rewarded: self)
    }
    
    func rewardedVideoAdServerRewardDidSucceed(_ rewardedVideoAd: FBRewardedVideoAd) {
        delegate?.eligibleForReward(rewarded: self)
    }
}
