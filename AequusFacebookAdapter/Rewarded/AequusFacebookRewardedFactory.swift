//
//  AequusFacebookRewardedFactory.swift
//  AequusFacebookAdapter
//
//  Created by Bohdan Korda on 25.04.2021.
//

import Foundation
import AequusCore

public final class AequusFacebookRewardedFactory: RewardedInterstitialFactory {
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusFacebookRewarded(placementId: id, bidToken: nil, delegate: delegate)
    }
    
    public static func createInstance() -> AequusFacebookRewardedFactory {
        return AequusFacebookRewardedFactory()
    }
    
}

