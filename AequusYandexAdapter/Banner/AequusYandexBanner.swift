//
//  AequusYandexBanner.swift
//  AequusYandexAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import AequusCore
import YandexMobileAds

class AequusYandexBanner: NSObject, Banner, Impressionable {
    
    weak var delegate: BannerDelegate?
	
    var sdkVersion: String {
		YMAMobileAds.sdkVersion()
    }
    var isReady: Bool = false

    var bannerView: UIView? {
        return banner
    }
	var timeout: Bool = false
    
    private let viewController: UIViewController
    private var banner: YMAAdView?
    private let unitId: String
    
	init(unitId: String, viewController: UIViewController, size: CGSize, delegate: BannerDelegate?) {
		
        self.viewController = viewController
        self.unitId = unitId
        self.delegate = delegate
		
		let size = YMAAdSize.fixedSize(with: size)
		banner = YMAAdView(adUnitID: self.unitId, adSize: size)
		
		super.init()
		
		banner?.delegate = self
    }
    
    func load() {
		banner?.loadAd()
    }
    
    func destroy() {
		banner?.removeFromSuperview()
    }
}

extension AequusYandexBanner: YMAAdViewDelegate {
	func viewControllerForPresentingModalView() -> UIViewController? {
		return viewController
	}
	
	func adViewDidLoad(_ adView: YMAAdView) {
		delegate?.didLoad(banner: self)
	}
	
	func adViewDidFailLoading(_ adView: YMAAdView, error: Error) {
		delegate?.failToLoad(banner: self, error: error)
	}
	
	func adView(_ adView: YMAAdView, didTrackImpressionWith impressionData: YMAImpressionData?) {
		delegate?.didShow(banner: self)
		delegate?.impression(banner: self)
	}
}
