//
//  AequusYandexBannerFactory.swift
//  AequusYandexAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusYandexBannerFactory: BannerFactory {
    
	public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner {
		return AequusYandexBanner(unitId: id, viewController: viewController, size: type.size, delegate: delegate)
    }

    public static func createInstance() -> AequusYandexBannerFactory {
        return AequusYandexBannerFactory()
    }
}
