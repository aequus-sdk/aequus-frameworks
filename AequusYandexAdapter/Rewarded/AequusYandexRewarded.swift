//
//  AequusYandexRewarded.swift
//  AequusYandexAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import YandexMobileAds

final class AequusYandexRewarded: NSObject, RewardedInterstitial, StatusCheck {
	
	weak var delegate: RewardedInterstitialDelegate?
	
	var isReady: Bool = false
	
	var sdkVersion: String {
		YMAMobileAds.sdkVersion()
	}
	
	private let unitId: String
	private var rewarded: YMARewardedAd
	
	init(unitId: String, delegate: RewardedInterstitialDelegate?) {
		self.delegate = delegate
		self.unitId = unitId
		self.rewarded = YMARewardedAd(adUnitID: unitId)
		
		super.init()
		
		rewarded.delegate = self
	}
	
	func load() {
		rewarded.load()
	}
	
	func show(from viewController: UIViewController) {
		rewarded.present(from: viewController)
	}
	
	func destroy() {

	}
	
}

extension AequusYandexRewarded:  YMARewardedAdDelegate  {
	func rewardedAdDidLoad(_ rewardedAd: YMARewardedAd) {
		delegate?.didLoad(rewarded: self)
	}
	
	func rewardedAdDidFail(toLoad rewardedAd: YMARewardedAd, error: Error) {
		delegate?.didFailToLoad(rewarded: self, error: error)
	}
	
	func rewardedAd(_ rewardedAd: YMARewardedAd, didTrackImpressionWith impressionData: YMAImpressionData?) {
		delegate?.impression(rewarded: self)
	}
	
	func rewardedAdDidAppear(_ rewardedAd: YMARewardedAd) {
		delegate?.didShow(rewarded: self)
	}
	
	func rewardedAdDidDisappear(_ rewardedAd: YMARewardedAd) {
		delegate?.didClose(rewarded: self)
	}
	
	func rewardedAdDidFail(toPresent rewardedAd: YMARewardedAd, error: Error) {
		delegate?.didFailToShow(rewarded: self, error: error)
	}
	
	func rewardedAd(_ rewardedAd: YMARewardedAd, didReward reward: YMAReward) {
		delegate?.eligibleForReward(rewarded: self)
	}
}
