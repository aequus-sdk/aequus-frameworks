//
//  AequusYandexRewardedFactory.swift
//  AequusYandexAdapter
//
//  Created by Bohdan Korda on 29.04.2021.
//

import Foundation
import AequusCore

public final class AequusYandexRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusYandexRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusYandexRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusYandexRewardedFactory {
        return AequusYandexRewardedFactory()
    }
}
