//
//  AequusYandexInitializer.swift
//  AequusYandexAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import Combine
import YandexMobileAds

public enum AequusYandexInitializingError : Error {
    case some
}

public final class AequusYandexInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        if !AequusYandexInitializer.isInitialized {
            cancelable = privacyPublisher.sink { privacy in
                
            }
		}
		AequusYandexInitializer.isInitialized = true
		completion(true, nil)
        
    }
}

extension AequusYandexInitializer: Initializable {
    public static func createInstance() -> AequusYandexInitializer {
        return AequusYandexInitializer()
    }
}
