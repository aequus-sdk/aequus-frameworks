//
//  AequusYandexInterstitial.swift
//  AequusYandexAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore
import YandexMobileAds

final class AequusYandexInterstitial: NSObject, Interstitial, StatusCheck {
    
    weak var delegate: InterstitialDelegate?
	
    var isReady: Bool = false
    
    var sdkVersion: String {
		YMAMobileAds.sdkVersion()
    }
    
    private let unitId: String
	private var interstitial: YMAInterstitialAd
    
	init(unitId: String, delegate: InterstitialDelegate?) {
        self.delegate = delegate
        self.unitId = unitId
		self.interstitial = YMAInterstitialAd(adUnitID: unitId)
		
        super.init()
		
		interstitial.delegate = self
    }
    
    func load() {
		interstitial.load()
    }
    
    func show(from viewController: UIViewController) {
		interstitial.present(from: viewController)
    }
    
    func destroy() {

    }
    
}

extension AequusYandexInterstitial: YMAInterstitialAdDelegate {
	func interstitialAdDidLoad(_ interstitialAd: YMAInterstitialAd) {
		delegate?.didLoad(interstitial: self)
	}
	
	func interstitialAdDidFail(toLoad interstitialAd: YMAInterstitialAd, error: Error) {
		delegate?.didFailToLoad(interstitial: self, error: error)
	}
	
	func interstitialAd(_ interstitialAd: YMAInterstitialAd, didTrackImpressionWith impressionData: YMAImpressionData?) {
		delegate?.impression(interstitial: self)
	}
	
	func interstitialAdDidAppear(_ interstitialAd: YMAInterstitialAd) {
		delegate?.didShow(interstitial: self)
	}
	
	func interstitialAdDidDisappear(_ interstitialAd: YMAInterstitialAd) {
		delegate?.didClose(interstitial: self)
	}
	
	func interstitialAdDidFail(toPresent interstitialAd: YMAInterstitialAd, error: Error) {
		delegate?.didFailToShow(interstitial: self, error: error)
	}
}
