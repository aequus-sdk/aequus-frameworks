//
//  AequusYandexInterstitialFactory.swift
//  AequusYandexAdapter
//
//  Created by Bohdan Korda on 28.04.2021.
//

import Foundation
import AequusCore

public final class AequusYandexInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusYandexInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusYandexInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusYandexInterstitialFactory {
        return AequusYandexInterstitialFactory()
    }
}
