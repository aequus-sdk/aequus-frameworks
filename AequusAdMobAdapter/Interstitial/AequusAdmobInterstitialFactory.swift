//
//  AequusAdmobInterstitialFactory.swift
//  AequusAdmobAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore

public final class AequusAdmobInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
        
        return AequusAdmobInterstitial(unitId: id, delegate: delegate)
    }
    
}

extension AequusAdmobInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusAdmobInterstitialFactory {
        return AequusAdmobInterstitialFactory()
    }
}
