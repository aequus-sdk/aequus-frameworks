//
//  AequusAdmobBannerFactory.swift
//  AequusAdmobAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore

public final class AequusAdmobBannerFactory: BannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        AequusAdmobBanner(unitId: id, type: type, viewController: viewController, delegate: delegate)
    }

    public static func createInstance() -> AequusAdmobBannerFactory {
        return AequusAdmobBannerFactory()
    }
}
