//
//  AequusAdmobRewardedFactory.swift
//  AequusAdmobAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore

public final class AequusAdmobRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
        return AequusAdmobRewarded(unitId: id, delegate: delegate)
    }
    
}

extension AequusAdmobRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusAdmobRewardedFactory {
        return AequusAdmobRewardedFactory()
    }
}
