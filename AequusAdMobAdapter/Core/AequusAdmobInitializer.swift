//
//  AequusAdmobInitializer.swift
//  AequusAdmobAdapter
//
//  Created by Bohdan Korda on 01.07.2021.
//

import Foundation
import AequusCore
import Combine
import GoogleMobileAds

public final class AequusAdmobInitializer: AdNetworkInitializer {
    
    public static var isInitialized: Bool = false
    private var cancelable: AnyCancellable?
    
    public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
        
        if !AequusAdmobInitializer.isInitialized {
            cancelable = privacyPublisher.sink { privacy in
				//TODO
            }
			
			GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ GADSimulatorID ]
            
			GADMobileAds.sharedInstance().start { status in
				if status.adapterStatusesByClassName["GADMobileAds"]?.state == .ready {
					AequusAdmobInitializer.isInitialized = true
					completion(true, nil)
				} else {
					completion(false, nil)
				}
			}
           
        } else {
            completion(true, nil)
        }
        
    }
}

extension AequusAdmobInitializer: Initializable {
    public static func createInstance() -> AequusAdmobInitializer {
        return AequusAdmobInitializer()
    }
}
