//
//  AdEventManager.swift
//  AequusCore
//
//  Created by Bohdan Korda on 03.12.2021.
//

import Foundation
import AequusCore
import Combine

class AdEventManager: AdapterEventManager, AequusVungleDelegateProtocol {
	
	static var shared: AdEventManager = AdEventManager()
	
	private override init() {
		super.init()
	}
	
	func didLoad(placementId: String) {
		adDidLoad(placementId)
	}
	
	func didShow(placementId: String) {
		adDidShow(placementId)
	}
	
	func impression(placementId: String) {
		adImpression(placementId)
	}
	
	func didClose(placementId: String) {
		adDidClose(placementId)
	}
	
	func didFailToLoad(placementId: String, error: Error) {
		adDidFailToLoadWithError(error, id: placementId)
	}
	
	func didFailToShow(placementId: String, error: Error) {
		adDidFailToShowWithError(error, id: placementId)
	}
	
	func click(placementId: String) {
		adDidClick(placementId)
	}
	
	func eligibleForReward(placementId: String) {
		adDidReward(placementId)
	}
	
	func rewardedVideoDidStart(placementId: String) {
		adVideoStart(placementId)
	}
	
	func rewardedVideoDidComplete(placementId: String) {
		adVideoFinished(placementId)
	}
}
