//
//  AequusVungleDelegateProtocol.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 30.06.2021.
//

import Foundation
import AequusCore

protocol AequusVungleDelegateProtocol: AnyObject {
	func didLoad(placementId: String)
	func didShow(placementId: String)
	func impression(placementId: String)
	func didClose(placementId: String)
	func didFailToLoad(placementId: String, error: Error)
	func didFailToShow(placementId: String, error: Error)
	func click(placementId: String)//clickType: ClickType = ClickType.Main
	func eligibleForReward(placementId: String)
	// If the adapter supports it, else ignore
	func rewardedVideoDidStart(placementId: String)
	// If the adapter supports it, else ignore
	func rewardedVideoDidComplete(placementId: String)
}
