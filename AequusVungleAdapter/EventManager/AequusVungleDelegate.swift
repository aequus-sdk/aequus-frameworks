//
//  AequusVungleDelegate.swift
//  AequusCore
//
//  Created by Bohdan Korda on 30.06.2021.
//

import Foundation
import AequusCore
import VungleSDK

enum MockError: Error {
	case unknown
}

final class AequusVungleDelegate: NSObject, VungleSDKDelegate {
	static let shared = AequusVungleDelegate()
	
	weak var adEventManager: AequusVungleDelegateProtocol? = AdEventManager.shared
	weak var initDelegate: AequusVungleInitializationDelegate?
	
	func vungleSDKDidInitialize() {
		initDelegate?.vungleSDKDidInitialize()
	}
	
	func vungleSDKFailedToInitializeWithError(_ error: Error) {
		initDelegate?.vungleSDKFailedToInitializeWithError(error)
	}
	
	func vungleAdPlayabilityUpdate(_ isAdPlayable: Bool, placementID: String?, error: Error?) {
		guard let placementID = placementID else { return }
		if let error = error {
			adEventManager?.didFailToLoad(placementId: placementID, error: error)
		} else if isAdPlayable {
			adEventManager?.didLoad(placementId: placementID)
		}
	}
	
	func vungleDidShowAd(forPlacementID placementID: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.didShow(placementId: placementID)
	}
	
	func vungleDidCloseAd(forPlacementID placementID: String) {
		adEventManager?.didClose(placementId: placementID)
	}
	
	func vungleRewardUser(forPlacementID placementID: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.eligibleForReward(placementId: placementID)
	}
	
	func vungleTrackClick(forPlacementID placementID: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.click(placementId: placementID)
	}
	
	func vungleAdViewed(forPlacement placementID: String) {
		adEventManager?.impression(placementId: placementID)
	}
}

extension AequusVungleDelegate: VungleSDKHBDelegate {
	
    //Looks like this method could be responsible for rewardedVideoDidComplete but let's see if that won't break something
    func vungleWillCloseAd(forPlacementID placementID: String) {
        adEventManager?.rewardedVideoDidComplete(placementId: placementID)
    }
    
	func vungleAdViewed(forPlacementID placementID: String?, adMarkup: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.impression(placementId: placementID)
	}
	
	func vungleRewardUser(forPlacementID placementID: String?, adMarkup: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.eligibleForReward(placementId: placementID)
	}
	
	func vungleDidCloseAd(forPlacementID placementID: String?, adMarkup: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.didClose(placementId: placementID)
	}
	
	func vungleTrackClick(forPlacementID placementID: String?, adMarkup: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.click(placementId: placementID)
	}
	
	func vungleDidShowAd(forPlacementID placementID: String?, adMarkup: String?) {
		guard let placementID = placementID else { return }
		adEventManager?.didShow(placementId: placementID)
	}
	
	func vungleAdPlayabilityUpdate(_ isAdPlayable: Bool, placementID: String?, adMarkup: String?, error: Error?) {
		guard let placementID = placementID else { return }
		if let error = error {
			adEventManager?.didFailToLoad(placementId: placementID, error: error)
		} else if isAdPlayable {
			adEventManager?.didLoad(placementId: placementID)
		}
	}
}
