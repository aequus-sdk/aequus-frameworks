//
//  AequusVungleBidTokenSource.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import VungleSDK

final class AequusVungleBidTokenSource: BidTokenSource  {
	
	func invoke(completion: @escaping (String?) -> Void) {
		completion(VungleSDK.shared().currentSuperToken())
	}
	
}

extension AequusVungleBidTokenSource: Initializable {
	static func createInstance() -> AequusVungleBidTokenSource {
		AequusVungleBidTokenSource()
	}
}
