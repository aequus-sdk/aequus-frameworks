//
//  AequusVungleInitializer.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import Combine
import VungleSDK

public final class AequusVungleInitializer: AdNetworkInitializer {
	public static var isInitialized: Bool = false
	private static var inProgress: Bool = false
	private var cancellable: AnyCancellable?
	
	private var initCallback: ((Bool, Error?) -> Void)?
	
	public func initialize(config: SDKConfig.AdapterNetworkConfig?, privacyPublisher: AnyPublisher<AequusPrivacy, Never>, completion: @escaping (Bool, Error?) -> Void) {
		
		cancellable = privacyPublisher.sink { privacy in
			VungleSDK.shared().update(privacy.GDPRConsent ? .accepted : .denied, consentMessageVersion: privacy.GDPRConsent ? "Accepted" : "Denied")
			VungleSDK.shared().update(privacy.CCPADoNotSell ? .accepted : .denied)
		}
		
		if let config = config,
		   let appId = config.appID,
		   !AequusVungleInitializer.isInitialized,
		   !AequusVungleInitializer.inProgress {
			
			initCallback = completion
			AequusVungleDelegate.shared.initDelegate = self
			VungleSDK.shared().delegate = AequusVungleDelegate.shared
			VungleSDK.shared().sdkHBDelegate = AequusVungleDelegate.shared
			
			VungleSDK.shared().perform(Selector(("setPluginName:version:")), with: "vunglehbs", with: "vungle 5.0.0")
			VungleSDK.enableBackgroundDownload(false)
			do {
				try VungleSDK.shared().start(withAppId: appId)
				AequusVungleInitializer.inProgress = true
			} catch {
				AequusVungleInitializer.inProgress = false
				print("Error while starting VungleSDK : \(error)")
				completion(false, error)
			}
		}

	}
}

extension AequusVungleInitializer: Initializable {
	public static func createInstance() -> AequusVungleInitializer {
		return AequusVungleInitializer()
	}
}

extension AequusVungleInitializer: AequusVungleInitializationDelegate {
	func vungleSDKDidInitialize() {
		AequusVungleInitializer.isInitialized = true
		AequusVungleInitializer.inProgress = false
		initCallback?(true, nil)
	}
	
	func vungleSDKFailedToInitializeWithError(_ error: Error) {
		AequusVungleInitializer.inProgress = false
		initCallback?(false, error)
	}
}
