//
//  VungleSDKProxy.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 15.12.2021.
//

import Foundation
import VungleSDK

enum VungleSDKProxyError: Error {
	case alreadyInProgress
}

class VungleSDKProxy {
	
	static let shared = VungleSDKProxy()
	private var owners: [String : String] = [:]
	
	private init(){}
	
	func tryToCreateInstance(for placementID: String, owner: String) -> Bool {
		tryPassOwner(placementID: placementID, owner: owner)
	}
	
	private func tryPassOwner(placementID: String, owner: String) -> Bool {
		if owners[placementID] == owner {
			return true
		}

		if owners[placementID] == nil {
			owners[placementID] = owner
			return true
		}

		return false
	}

	func tryReleaseOwner(placementID: String, owner: String) {
		if owners[placementID] == owner {
			owners.removeValue(forKey: placementID)
		}
	}
}
