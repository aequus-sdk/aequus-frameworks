//
//  AequusVungleInitializationDelegate.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 30.06.2021.
//

import Foundation

protocol AequusVungleInitializationDelegate: AnyObject {
	func vungleSDKDidInitialize()
	func vungleSDKFailedToInitializeWithError(_ error: Error)
}
