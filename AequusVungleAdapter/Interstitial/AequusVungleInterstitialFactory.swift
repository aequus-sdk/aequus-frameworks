//
//  AequusVungleInterstitialFactory.swift
//  AequusVungleInterstitialFactory
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import VungleSDK

public final class AequusVungleInterstitialFactory: InterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: InterstitialDelegate) -> Interstitial? {
		return AequusVungleInterstitial(placementId: id, delegate: delegate)
    }
    
}

extension AequusVungleInterstitialFactory: Initializable {
    
    public static func createInstance() -> AequusVungleInterstitialFactory {
        return AequusVungleInterstitialFactory()
    }
}
