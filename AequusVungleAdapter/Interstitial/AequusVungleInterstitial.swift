//
//  AequusVungleInterstitial.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import VungleSDK
import Combine
import UIKit

final class AequusVungleInterstitial: NSObject, Interstitial, StatusCheck {
	
	weak var delegate: InterstitialDelegate?
	
	var sdkVersion: String {
		"6.10.6"
	}

	var isReady: Bool {
		VungleSDK.shared().isAdCached(forPlacementID: placementId)
	}
	
	private var didLoadSent: Bool = false
	private var callLoad: Bool = false
	
	private let placementId: String
	private var cancellable: AnyCancellable?
	private let objectId: UUID
	
	init?(placementId: String, delegate: InterstitialDelegate?) {
		let id = UUID()
		guard VungleSDKProxy.shared.tryToCreateInstance(for: placementId, owner: id.uuidString) else {
			return nil
		}
		
		self.objectId = id
		self.placementId = placementId
		self.delegate = delegate
		super.init()
		subscribeEvents()
	}
	
	func load() {
		DispatchQueue.main.async {
			self.callLoad = true
			let sdk = VungleSDK.shared()
			if self.isReady {
				AdEventManager.shared.didLoad(placementId: self.placementId)
				return
			}
			
			do {
				try sdk.loadPlacement(withID: self.placementId)
			} catch {
				self.delegate?.didFailToLoad(interstitial: self, error: error)
			}
		}
	}
	
	func show(from viewController: UIViewController) {
		DispatchQueue.main.async {
			let sdk:VungleSDK = VungleSDK.shared()
			if self.isReady {
				do {
					try sdk.playAd(viewController, options: nil, placementID: self.placementId)
				}
				catch {
					self.delegate?.didFailToShow(interstitial: self, error: error)
				}
			} else {
				//-1984 code for show failing
				self.delegate?.didFailToShow(interstitial: self, error: NSError(domain: "com.aequus.sdk", code: -1984, userInfo: [:]))
			}
		}
	}
	
	func destroy() {
		VungleSDKProxy.shared.tryReleaseOwner(placementID: self.placementId, owner: self.objectId.uuidString)
		cancellable?.cancel()
		cancellable = nil
		callLoad = false
		didLoadSent = false
	}
	
	private func subscribeEvents() {
		cancellable = AdEventManager.shared.adEventPublisher.sink { [weak self] event in
			guard let self = self,
				  event.adUnitID == self.placementId,
				  self.callLoad else { return }
			switch event.type {
			case .didLoad:
				if !self.didLoadSent {
					self.didLoadSent = true
					self.delegate?.didLoad(interstitial: self)
				}
			case .failToLoad(let error):
				if !self.didLoadSent {
					self.delegate?.didFailToLoad(interstitial: self, error: error)
				}
			case .didShow:
				self.delegate?.didShow(interstitial: self)
			case .hide:
				self.delegate?.didClose(interstitial: self)
			case .failToShow(let error):
				self.delegate?.didFailToShow(interstitial: self, error: error)
			case .reward:
				break
			case .click:
				self.delegate?.click(interstitial: self)
			case .expire:
				break
			case .impression:
				self.delegate?.impression(interstitial: self)
			case .videoStart:
				break
			case .videoFinished:
				break
			}
		}
	}
}
