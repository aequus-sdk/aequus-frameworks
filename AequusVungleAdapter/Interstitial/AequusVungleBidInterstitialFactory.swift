//
//  AequusVungleBidInterstitialFactory.swift
//  AequusVungleInterstitialFactory
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import VungleSDK

public final class AequusVungleBidInterstitialFactory: BidInterstitialFactory {
	public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: InterstitialDelegate) -> Interstitial? {
		return AequusVungleBidInterstitial(placementId: adId, bidResponse: adm, delegate: delegate)
	}
}

extension AequusVungleBidInterstitialFactory: Initializable {
    public static func createInstance() -> AequusVungleBidInterstitialFactory {
        return AequusVungleBidInterstitialFactory()
    }
}
