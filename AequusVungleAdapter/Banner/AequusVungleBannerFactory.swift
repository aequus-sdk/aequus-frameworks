//
//  AequusVungleBannerFactory.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusVungleBannerFactory: BannerFactory {
    
    public func create(viewController: UIViewController, type: AequusBannerType, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: BannerDelegate) -> Banner? {
        return AequusVungleBanner(placementId: id, viewController: viewController, type: type, delegate: delegate)
    }

    public static func createInstance() -> AequusVungleBannerFactory {
        return AequusVungleBannerFactory()
    }
}
