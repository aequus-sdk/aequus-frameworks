//
//  AequusVungleBidBannerFactory.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 11.05.2021.
//

import Foundation
import AequusCore

public final class AequusVungleBidBannerFactory: BidBannerFactory {
    public func create(viewController: UIViewController, type: AequusBannerType, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: BannerDelegate) -> Banner? {
        return AequusVungleBidBanner(placementId: adId, bidResponse: adm, viewController: viewController, type: type, delegate: delegate)
    }
    
    public static func createInstance() -> AequusVungleBidBannerFactory {
        return AequusVungleBidBannerFactory()
    }
}
