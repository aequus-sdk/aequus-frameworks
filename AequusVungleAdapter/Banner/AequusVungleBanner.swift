//
//  AequusVungleBanner.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import AequusCore
import VungleSDK
import Combine

class AequusVungleBanner: BannerBase {
	override var isReady: Bool {
		VungleSDK.shared().isAdCached(forPlacementID: placementId)
	}
	
	override func tryToLoadBannerPlacement(type: AequusBannerType) {
		do {
			if type == .w320h50 {
                try vungleSdk.loadPlacement(withID: placementId, with: type.size.width == 320 ? .banner : .bannerLeaderboard)
			} else { //if mrec
				try vungleSdk.loadPlacement(withID: placementId)
			}
		} catch let error {
			sendEvent(.failToLoad(error: error))
		}
	}
}
