//
//  BannerBase.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 04.05.2022.
//

import Foundation
import AequusCore
import VungleSDK
import Combine

class BannerBase: NSObject, Banner, Impressionable {
	weak var delegate: BannerDelegate?
	
	var sdkVersion: String {
		"6.10.6"
	}
	var isReady: Bool {
		fatalError("overide in child class")
	}
	
	var bannerView: UIView? {
		return insideView
	}
	
	var timeout: Bool = false
	
	private let viewController: UIViewController
	private var cancellable: AnyCancellable?
	private let type: AequusBannerType
	private var sentEvents: [AdEventType] = []
	
	let insideView: UIView
	let vungleSdk: VungleSDK = VungleSDK.shared()
	let placementId: String
	var bidResponse: String?
	
	init(placementId: String, bidResponse: String? = nil, viewController: UIViewController, type: AequusBannerType, delegate: BannerDelegate?) {
		self.insideView = UIView(frame: CGRect(origin: .zero, size: type.size))
		self.type = type
		self.viewController = viewController
		self.placementId = placementId
		self.delegate = delegate
		self.bidResponse = bidResponse
		
		super.init()
		
		cancellable = AdEventManager.shared.adEventPublisher.sink { [weak self] event in
			guard let self = self,
				  event.adUnitID == self.placementId,
				  self.sendEvent(event.type)
			else { return }
			
			if event.type == .didLoad {
				self.tryToShowBanner()
			}
		}
	}
	
	func load() {
		if isReady {
			sendEvent(.didLoad)
			tryToShowBanner()
			return
		}
		
		tryToLoadBannerPlacement(type: type)
	}
	
	func destroy() {
		insideView.removeFromSuperview()
		cancellable = nil
	}
	
	func tryToLoadBannerPlacement(type: AequusBannerType) {
		fatalError("override in child class")
	}
	
	private func tryToShowBanner() {
		if isReady {
			do {
				if let repsponse = bidResponse {
					try vungleSdk.addAdView(to: insideView, withOptions: [:], placementID: placementId, adMarkup: repsponse)
				} else {
					try vungleSdk.addAdView(to: insideView, withOptions: [:], placementID: placementId)
				}
			} catch {
				sendEvent(.failToLoad(error: error))
			}
		} else {
			sendEvent(.failToLoad(error: NSError(domain: "", code: 0)))  //?????
		}
	}
	
	private func cutEventIfNeeded(_ event: AdEventType) -> Bool {
		if event == .click {
			return false
		}
		
		if sentEvents.contains(where: { event == $0 }) {
			return true
		}
		
		sentEvents.append(event)
		return false
	}
	
	@discardableResult
	func sendEvent(_ event: AdEventType) -> Bool {
		guard !self.cutEventIfNeeded(event) else {
			return false
		}
		
		switch event {
		case .didLoad:
			delegate?.didLoad(banner: self)
		case .failToLoad(let error):
			delegate?.failToLoad(banner: self, error: error)
		case .click:
			delegate?.click(banner: self)
		case .didShow:
			delegate?.didShow(banner: self)
		case .impression:
			delegate?.impression(banner: self)
		default:
			return false
		}
		return true
	}
}
