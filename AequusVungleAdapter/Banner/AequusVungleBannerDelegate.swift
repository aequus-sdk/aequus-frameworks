//
//  AequusVungleBannerDelegate.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 30.06.2021.
//

import Foundation

protocol AequusVungleBannerDelegate: AnyObject {
	func didLoad(placementId: String)
	func failToLoad(placementId: String, error: Error?)
	func didShow(placementId: String)
	func impression(placementId: String)
	func click(placementId: String)
}
