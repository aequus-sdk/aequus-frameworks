//
//  AequusVungleBidRewardedFactory.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import VungleSDK

public final class AequusVungleBidRewardedFactory: BidRewardedInterstitialFactory {
	
	public func create(viewController: UIViewController, adId: String, bidId: String, adm: String, params: [String : String]?, hashableParams: [AnyHashable: Any]?, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
		return AequusVungleBidRewarded(placementId: adId, bidResponse: adm, delegate: delegate)
	}
	
}

extension AequusVungleBidRewardedFactory: Initializable {
    public static func createInstance() -> AequusVungleBidRewardedFactory {
        return AequusVungleBidRewardedFactory()
    }
}
