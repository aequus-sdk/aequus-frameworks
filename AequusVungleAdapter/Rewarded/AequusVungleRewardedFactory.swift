//
//  AequusVungleRewardedFactory.swift
//  AequusVungleInterstitialFactory
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import VungleSDK

public final class AequusVungleRewardedFactory: RewardedInterstitialFactory {
    
    public func create(viewController: UIViewController, id: String, adUnitConfig: SDKConfig.AdUnitConfig, delegate: RewardedInterstitialDelegate) -> RewardedInterstitial? {
        
		return AequusVungleRewarded(placementId: id, delegate: delegate)
    }
    
}

extension AequusVungleRewardedFactory: Initializable {
    
    public static func createInstance() -> AequusVungleRewardedFactory {
        return AequusVungleRewardedFactory()
    }
}
