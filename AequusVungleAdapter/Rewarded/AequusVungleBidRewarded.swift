//
//  AequusVungleRewarded.swift
//  AequusVungleAdapter
//
//  Created by Bohdan Korda on 29.06.2021.
//

import Foundation
import AequusCore
import VungleSDK
import Combine

final class AequusVungleBidRewarded: NSObject, RewardedInterstitial, StatusCheck {
	
	weak var delegate: RewardedInterstitialDelegate?
	
	var sdkVersion: String {
		"6.10.6"
	}
	
	var isReady: Bool {
		VungleSDK.shared().isAdCached(forPlacementID: placementId, adMarkup: bidResponse)
	}
	
	private var didLoadSent: Bool = false
	private var callLoad: Bool = false
	
	private let placementId: String
	private var cancellable: AnyCancellable?
	private let objectId: UUID
	private let bidResponse: String
	
	init?(placementId: String, bidResponse: String, delegate: RewardedInterstitialDelegate?) {
		let id = UUID()
		guard VungleSDKProxy.shared.tryToCreateInstance(for: placementId, owner: id.uuidString) else {
			return nil
		}
		
		self.objectId = id
		self.placementId = placementId
		self.delegate = delegate
		self.bidResponse = bidResponse
		super.init()
		subscribeEvents()
	}
	
	func load() {
		self.callLoad = true
		let sdk = VungleSDK.shared()
		if isReady {
			AdEventManager.shared.didLoad(placementId: self.placementId)
			return
		}
		
		do {
			try sdk.loadPlacement(withID: self.placementId, adMarkup: bidResponse)
		} catch {
			self.delegate?.didFailToLoad(rewarded: self, error: error)
		}
	}
	
	func show(from viewController: UIViewController) {
		DispatchQueue.main.async {
			let sdk:VungleSDK = VungleSDK.shared()
			if self.isReady {
				do {
					try sdk.playAd(viewController, options: nil, placementID: self.placementId, adMarkup: self.bidResponse)
				}
				catch let error  {
					self.delegate?.didFailToShow(rewarded: self, error: error)
				}
			} else {
				//-1984 code fo show failing
				self.delegate?.didFailToShow(rewarded: self, error: NSError(domain: "com.aequus.sdk", code: -1984, userInfo: [:]))
			}
		}
	}
	
	func destroy() {
		VungleSDKProxy.shared.tryReleaseOwner(placementID: self.placementId, owner: self.objectId.uuidString)
		cancellable?.cancel()
		cancellable = nil
		callLoad = false
		didLoadSent = false
	}
	
	private func subscribeEvents() {
		cancellable = AdEventManager.shared.adEventPublisher.sink { [weak self] event in
			guard let self = self,
				  event.adUnitID == self.placementId,
				  self.callLoad else { return }
			switch event.type {
			case .didLoad:
				if !self.didLoadSent {
					self.didLoadSent = true
					self.delegate?.didLoad(rewarded: self)
				}
			case .failToLoad(let error):
				if !self.didLoadSent {
					self.delegate?.didFailToLoad(rewarded: self, error: error)
				}
			case .didShow:
				self.delegate?.didShow(rewarded: self)
			case .hide:
				self.delegate?.didClose(rewarded: self)
			case .failToShow(let error):
				self.delegate?.didFailToShow(rewarded: self, error: error)
			case .reward:
				self.delegate?.eligibleForReward(rewarded: self)
			case .click:
				self.delegate?.click(rewarded: self)
			case .expire:
				break
			case .impression:
				self.delegate?.impression(rewarded: self)
			case .videoStart:
				self.delegate?.rewardedVideoDidStart(rewarded: self)
			case .videoFinished:
				self.delegate?.rewardedVideoDidComplete(rewarded: self)
			}
		}
	}
}
