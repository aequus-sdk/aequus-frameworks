#
#  Be sure to run `pod spec lint moduleName.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

moduleName = 'AequusCore'
moduleNameBundleIdentifier = 'com.aequus.ads.sdk.core'

Pod::Spec.new do |s|
  s.name             = moduleName
  s.version          = '1.4.7'
  s.summary          = moduleName
  s.description      = 'Aequus allows app Developers to monetize their mobile in-app advertising inventory through a unique blend of bidding and waterfall management system. Developers can work with unique demand partners, to maximize yield and efficiency, through a unified auction between mediated networks and bidders.'

  s.homepage         = 'https://aequus1.gitlab.io/shared/aequus-documentation/'
  s.license          = { :type => 'New BSD', :file => 'LICENSE' }
  s.author           = { 'Bohdan Korda' => 'bohdan-korda@aequus.mobi' }
  s.source           = { :git => 'https://gitlab.com/aequus-sdk/aequus-frameworks.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'
  s.requires_arc = true
  s.swift_versions = '5.0'
  
  s.module_name = moduleName

  s.frameworks = 'Foundation', 'UIKit', 'WebKit', 'AdSupport', 'CoreData'
  s.libraries = 'c++'
  s.vendored_frameworks = "AequusCore.xcframework"

end
